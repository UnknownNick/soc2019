package unknownnick.engine.shape;

import static java.lang.Math.PI;
import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;
import unknownnick.engine.util.math.Prints;

public class Cylinder implements Shape {
    int precision;
    private float height;
    private float[] vertices;
    private float normals[];
    private float[] textures;
    private int indices[];
    
    public Cylinder(){
        precision = 30;
        this.height = 1;
        populate();
    }

    public Cylinder(int precision, float height) {
        this.precision = precision;
        this.height = height;
        populate();
    }
    
    public void move(Vector3f pos){
        for(int i = 0; i < vertices.length/3;i++){
            vertices[3*i] += pos.x;
            vertices[3*i+1] += pos.y;
            vertices[3*i+2] += pos.z;
        }
    }    

    @Override
    public ModelData getModel() {
        return new ModelData(vertices, textures, normals, indices, 1.0f);
    }
    
    private void populate(){
        List<Vector3f> circle = new ArrayList<>();
        for(float alpha = 0; alpha < 2*PI; alpha += 2*PI/precision){
            circle.add(new Vector3f((float)sin(alpha), height/2, (float)cos(alpha)));
        }
        List<Vector3f> circle2 = new ArrayList<>();
        for(Vector3f vec : circle){
            circle2.add(new Vector3f(vec));
        }
        for(Vector3f vec : circle2){
            vec.setY(-height/2);
        }
        vertices = new float[circle.size()*3*2];
        normals = new float[circle.size()*3*2];
        textures = new float[circle.size()*2*2];
        for(int i = 0; i < circle.size() ; i++){
            vertices[6*i] = circle.get(i).x;
            vertices[6*i+1] = circle.get(i).y;
            vertices[6*i+2] = circle.get(i).z;
            normals[6*i] = circle.get(i).x;
            normals[6*i+1] = 0;
            normals[6*i+2] = circle.get(i).z;
            textures[4*i] = (float) (this.getLenghtOnUnitCircle(circle.get(i).x, circle.get(i).getZ())/(2*PI));
            textures[4*i+1] = 0;
            
            vertices[6*i+3] = circle2.get(i).x;
            vertices[6*i+4] = circle2.get(i).y;
            vertices[6*i+5] = circle2.get(i).z;
            normals[6*i+3] = circle2.get(i).x;
            normals[6*i+4] = 0;
            normals[6*i+5] = circle2.get(i).z;
            textures[4*i+2] = (float) (this.getLenghtOnUnitCircle(circle2.get(i).x, circle.get(i).getZ())/(2*PI));
            textures[4*i+3] = 1;            
        }
        indices = new int[circle.size()*3*2];
        int t = 0;
        for(; t < circle.size()-1 ;t++){
             indices[6*t] = 2*t;
             indices[6*t+1] = 2*t+1;
             indices[6*t+2] = 2*t+3;
             indices[6*t+3] = 2*t+2;
             indices[6*t+4] = 2*t;             
             indices[6*t+5] = 2*t+3;
        }
             indices[6*t] = 2*t;
             indices[6*t+1] = 2*t+1;
             indices[6*t+2] = 1;
             indices[6*t+3] = 0;
             indices[6*t+4] = 2*t;             
             indices[6*t+5] = 1;        
    }
    
    private float getLenghtOnUnitCircle(float x, float y){
        double acos = acos(y);
        double asin = asin(x);
        double angle = (asin>0)? acos:2*PI-acos;
        return (float) angle;
    }
    
}
