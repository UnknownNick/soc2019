package unknownnick.engine.shape;

import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;

public class Sphere implements Shape {
    
    public enum MeshType{
        ICOSAHEDRON
    }
    
    int precision;
    private MeshType type = MeshType.ICOSAHEDRON;
    private float[] vertices;
    private float normals[];
    private float[] textures;
    private int indices[];
    
    public Sphere(){
        precision = 4;
        populateMesh();
    }
    
    public Sphere(int precision){
        this.precision = precision;
        populateMesh();
    }

    @Override
    public ModelData getModel() {
        return new ModelData(vertices, textures, normals, indices, 1.0f);
    }
    
    public void move(Vector3f pos){
        for(int i = 0; i < vertices.length/3;i++){
            vertices[3*i] += pos.x;
            vertices[3*i+1] += pos.y;
            vertices[3*i+2] += pos.z;
        }
    }
    
    public void populateMesh(){
        if(type==MeshType.ICOSAHEDRON){
            List<Vector3f> initialVertices = new ArrayList<>();
            float s = (float) ((1+sqrt(5))/2);
            initialVertices.add(new Vector3f(-1,  s, 0));
            initialVertices.add(new Vector3f( 1,  s, 0));
            initialVertices.add(new Vector3f(-1, -s, 0));
            initialVertices.add(new Vector3f( 1, -s, 0));
            initialVertices.add(new Vector3f( 0, -1,  s));
            initialVertices.add(new Vector3f( 0,  1,  s));
            initialVertices.add(new Vector3f( 0, -1, -s));
            initialVertices.add(new Vector3f( 0,  1, -s));
            initialVertices.add(new Vector3f( s, 0, -1));
            initialVertices.add(new Vector3f( s, 0,  1));
            initialVertices.add(new Vector3f(-s, 0, -1));
            initialVertices.add(new Vector3f(-s, 0,  1));
            for(Vector3f vec : initialVertices){
                vec.normalise();
            }
            
            
            List<TriangleIndices> faces = new ArrayList<>();
            faces.add(new TriangleIndices(0, 11, 5));
            faces.add(new TriangleIndices(0, 5, 1));
            faces.add(new TriangleIndices(0, 1, 7));
            faces.add(new TriangleIndices(0, 7, 10));
            faces.add(new TriangleIndices(0, 10, 11));
            
            faces.add(new TriangleIndices(1, 5, 9));
            faces.add(new TriangleIndices(5, 11, 4));
            faces.add(new TriangleIndices(11, 10, 2));
            faces.add(new TriangleIndices(10, 7, 6));
            faces.add(new TriangleIndices(7, 1, 8));

            faces.add(new TriangleIndices(3, 9, 4));
            faces.add(new TriangleIndices(3, 4, 2));
            faces.add(new TriangleIndices(3, 2, 6));
            faces.add(new TriangleIndices(3, 6, 8));
            faces.add(new TriangleIndices(3, 8, 9));

            faces.add(new TriangleIndices(4, 9, 5));
            faces.add(new TriangleIndices(2, 4, 11));
            faces.add(new TriangleIndices(6, 2, 10));
            faces.add(new TriangleIndices(8, 6, 7));
            faces.add(new TriangleIndices(9, 8, 1));
            
            for(int i = 0; i < precision; i++){
                List<TriangleIndices> newfaces = new ArrayList<>();
                for(TriangleIndices face: faces){
                    int a = getMiddle(initialVertices,face.getA(),face.getB());
                    int b = getMiddle(initialVertices,face.getB(),face.getC());
                    int c = getMiddle(initialVertices,face.getC(),face.getA());
                    
                    newfaces.add(new TriangleIndices(face.getA(),a,c));
                    newfaces.add(new TriangleIndices(face.getB(),b,a));
                    newfaces.add(new TriangleIndices(face.getC(),c,b));
                    newfaces.add(new TriangleIndices(a,b,c));
                }
                faces = newfaces;
            }
            Object[] vert = initialVertices.toArray();
            int counter = 0;
            vertices = new float[vert.length*3];
            normals = new float[vert.length*3];
            for(Object v : vert){
                normals[counter] = ((Vector3f) v).x;
                vertices[counter++] = ((Vector3f) v).x;
                normals[counter] = ((Vector3f) v).y;
                vertices[counter++] = ((Vector3f) v).y;
                normals[counter] = ((Vector3f) v).z;
                vertices[counter++] = ((Vector3f) v).z;                
            }
            indices = new int[faces.size()*3];
            counter = 0;
            for(TriangleIndices t : faces){
                indices[counter++] = t.getA();
                indices[counter++] = t.getB();
                indices[counter++] = t.getC();                
            }
            textures = new float[vert.length*2];
            for(int i = 0;i<textures.length;i++){
                textures[i] = 0.0f;
            }
        }
    }
    
    private int getMiddle(List<Vector3f> vertices, int a, int b){
        Vector3f m = Vector3f.add(vertices.get(a), vertices.get(b), null);
        m.scale(0.5f);
        m.normalise();
        vertices.add(m);
        return vertices.size()-1;
    }
    
    private class TriangleIndices{
        int a, b, c;

        public TriangleIndices(int a, int b, int c) {
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public int getA() {
            return a;
        }

        public int getB() {
            return b;
        }

        public int getC() {
            return c;
        }
        
        
    }
}
