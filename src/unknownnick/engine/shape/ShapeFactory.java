package unknownnick.engine.shape;

import static java.lang.Math.max;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;
import static java.lang.Math.max;

public class ShapeFactory implements Shape {
    private float[] vertices = {};
    private float normals[] = {};
    private float[] textures = {};
    private int indices[] = {};
    private float furthestPoint;
    
    public ShapeFactory(){
        
    }
    
    public ShapeFactory(Shape[] shapes){
        this.add(shapes);
    }
    
    @Override
    public void move(Vector3f pos){
        for(int i = 0; i < vertices.length/3;i++){
            vertices[3*i] += pos.x;
            vertices[3*i+1] += pos.y;
            vertices[3*i+2] += pos.z;
        }
    }

    @Override
    public ModelData getModel() {
        return new ModelData(vertices, textures, normals, indices, 1.0f);        
    }
    
    public void add(Shape[] shapes){
        for(Shape s : shapes){
            this.add(s);
        }
    }
    
    public void add(Shape s){
        ModelData md = s.getModel();
        if(vertices.length==0){
            vertices = md.getVertices();
            normals = md.getNormals();
            textures = md.getTextureCoords();
            indices = md.getIndices();
            furthestPoint = md.getFurthestPoint();
            return;
        }
        float[] newVertices = new float[vertices.length+md.getVertices().length];
        float[] newNormals = new float[normals.length+md.getNormals().length];
        float[] newTextures = new float[textures.length+md.getTextureCoords().length];
        int[] newIndices = new int[indices.length+md.getIndices().length];
        System.arraycopy(vertices, 0, newVertices, 0, vertices.length);
        System.arraycopy(md.getVertices(), 0, newVertices, vertices.length, md.getVertices().length);
        System.arraycopy(normals, 0, newNormals, 0, normals.length);
        System.arraycopy(md.getNormals(), 0, newNormals, normals.length, md.getNormals().length);
        System.arraycopy(textures, 0, newTextures, 0, textures.length);
        System.arraycopy(md.getTextureCoords(), 0, newTextures, textures.length, md.getTextureCoords().length);
        System.arraycopy(indices, 0, newIndices, 0, indices.length);
        System.arraycopy(md.getIndices(), 0, newIndices, indices.length, md.getIndices().length);
        for(int i = indices.length;i<(indices.length + md.getIndices().length);i++){
            newIndices[i] += vertices.length/3;
        }
        vertices = newVertices;
        normals = newNormals;
        textures = newTextures;
        indices = newIndices;
        furthestPoint = max(furthestPoint, md.getFurthestPoint());
    }
    
    public void add(Shape s, Matrix4f transformation){
        ModelData md = s.getModel();
        md.transform(transformation);
        if(vertices.length==0){
            vertices = md.getVertices();
            normals = md.getNormals();
            textures = md.getTextureCoords();
            indices = md.getIndices();
            furthestPoint = md.getFurthestPoint();
            return;
        }
        float[] newVertices = new float[vertices.length+md.getVertices().length];
        float[] newNormals = new float[normals.length+md.getNormals().length];
        float[] newTextures = new float[textures.length+md.getTextureCoords().length];
        int[] newIndices = new int[indices.length+md.getIndices().length];
        System.arraycopy(vertices, 0, newVertices, 0, vertices.length);
        System.arraycopy(md.getVertices(), 0, newVertices, vertices.length, md.getVertices().length);
        System.arraycopy(normals, 0, newNormals, 0, normals.length);
        System.arraycopy(md.getNormals(), 0, newNormals, normals.length, md.getNormals().length);
        System.arraycopy(textures, 0, newTextures, 0, textures.length);
        System.arraycopy(md.getTextureCoords(), 0, newTextures, textures.length, md.getTextureCoords().length);
        System.arraycopy(indices, 0, newIndices, 0, indices.length);
        System.arraycopy(md.getIndices(), 0, newIndices, indices.length, md.getIndices().length);
        for(int i = indices.length;i<(indices.length + md.getIndices().length);i++){
            newIndices[i] += vertices.length/3;
        }
        vertices = newVertices;
        normals = newNormals;
        textures = newTextures;
        indices = newIndices;
        furthestPoint = max(furthestPoint, md.getFurthestPoint());
    }
}
