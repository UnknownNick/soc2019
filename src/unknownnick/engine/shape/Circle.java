package unknownnick.engine.shape;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;

public class Circle implements Shape {
    int precision;
    private float[] vertices;
    private float normals[];
    private float[] textures;
    private int indices[];
    
    public Circle(){
        precision = 30;
        populate();
    }

    public Circle(int precision) {
        this.precision = precision;
        populate();
    }
    
    public void flip(){
        for(int i = 0; i < normals.length;i++){
            normals[i] *= -1;
        }
        for(int i = 0; i < indices.length/3 ;i++){
            int temp = indices[3*i];
            indices[3*i] = indices[3*i+1];
            indices[3*i+1] = temp;
        }
    }
            
    private void populate(){
        List<Vector3f> circle = new ArrayList<>();
        for(float alpha = 0; alpha < 2*PI; alpha += 2*PI/precision){
            circle.add(new Vector3f((float)sin(alpha), 0, (float)cos(alpha)));
        }
        vertices = new float[circle.size()*3+3];
        normals = new float[circle.size()*3+3];
        textures = new float[circle.size()*2+2];
        indices = new int[circle.size()*3];
        vertices[0] = 0;
        vertices[1] = 0;
        vertices[2] = 0;
        normals[0] = 0;
        normals[1] = 1;
        normals[2] = 0;
        textures[0] = 0;
        textures[1] = 0;
        for(int i = 0; i < circle.size() ;i++){
            vertices[3*(i+1)] = circle.get(i).x;
            vertices[3*(i+1)+1] = circle.get(i).y;
            vertices[3*(i+1)+2] = circle.get(i).z;
            
            normals[3*(i+1)] = 0;
            normals[3*(i+1)+1] = 1;
            normals[3*(i+1)+2] = 0;
            
            textures[2*(i+1)] = 0;
            textures[2*(i+1)+1] = 0;
        }
        int counter = 0;
        for(int t = 1; t < circle.size() ;t++){
            indices[counter++] = t;
            indices[counter++] = t+1;
            indices[counter++] = 0;
        }
            indices[counter++] = circle.size();
            indices[counter++] = 1;
            indices[counter++] = 0;
    }
    
    public void move(Vector3f pos){
        for(int i = 0; i < vertices.length/3;i++){
            vertices[3*i] += pos.x;
            vertices[3*i+1] += pos.y;
            vertices[3*i+2] += pos.z;
        }
    }

    @Override
    public ModelData getModel() {
        return new ModelData(vertices, textures, normals, indices, 1.0f);        
    }
    
}
