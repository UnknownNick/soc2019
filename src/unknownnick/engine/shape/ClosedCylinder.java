package unknownnick.engine.shape;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.util.math.Maths;

public class ClosedCylinder extends ShapeFactory {
    public ClosedCylinder(int precision, float height){
        super();
        super.add(this.getShapes(precision, height));
    }
    
    public ClosedCylinder(){
        this(30, 1f);
    }
    
    public final Shape[] getShapes(int precision, float height){
        Cylinder cylinder = new Cylinder(precision, height);
        Circle top = new Circle(precision);
        top.move(new Vector3f(0, height/2, 0));
        Circle bottom = new Circle(precision);
        bottom.flip();
        bottom.move(new Vector3f(0, -height/2, 0));
        return new Shape[]{cylinder, top, bottom};
    }
}
