package unknownnick.engine.shape;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;

public class Square implements Shape {
    
    private float[] vertices;
    private float normals[];
    private float[] textures;
    private int indices[];
    
    public Square(){
        populate();
    }
    
    public void move(Vector3f pos){
        for(int i = 0; i < vertices.length/3;i++){
            vertices[3*i] += pos.x;
            vertices[3*i+1] += pos.y;
            vertices[3*i+2] += pos.z;
        }
    }
    
    private void populate(){
        this.vertices = new float[]{
            -0.5f, -0.5f, 0,
            -0.5f, 0.5f, 0,
            0.5f, -0.5f, 0,
            0.5f, 0.5f, 0
        };
        this.textures = new float[]{
            0, 1,
            0, 0,
            1, 1,
            1, 0
        };
        this.normals = new float[]{
            0, 0, 1,
            0, 0, 1,
            0, 0, 1,
            0, 0, 1
        };
        this.indices = new int[]{
            0, 2, 1, 2, 3, 1
        };
    }

    @Override
    public ModelData getModel() {
        return new ModelData(vertices, textures, normals, indices, 0.5f);
    }
    
    
}
