package unknownnick.engine.shape;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;

public interface Shape {
    public ModelData getModel();
    public void move(Vector3f pos);
}
