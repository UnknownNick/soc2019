package unknownnick.engine.io.file.obj;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;

public class ModelData {

	private float[] vertices;
	private float[] textureCoords;
	private float[] normals;
	private int[] indices;
	private float furthestPoint;

	public ModelData(float[] vertices, float[] textureCoords, float[] normals, int[] indices,
			float furthestPoint) {
		this.vertices = vertices;
		this.textureCoords = textureCoords;
		this.normals = normals;
		this.indices = indices;
		this.furthestPoint = furthestPoint;
	}
        
        public void transform(Matrix4f transform){
            for(int i = 0; i < vertices.length/3 ;i++){
                Vector4f vec = new Vector4f(vertices[3*i],vertices[3*i+1],vertices[3*i+2],1);
                Matrix4f.transform(transform, vec, vec);
                vertices[3*i] = vec.x;
                vertices[3*i+1] = vec.y;
                vertices[3*i+2] = vec.z;
            }
            for(int i = 0; i < normals.length/3 ;i++){
                Vector4f vec = new Vector4f(normals[3*i],normals[3*i+1],normals[3*i+2],0);
                Matrix4f.transform(transform, vec, vec);
                normals[3*i] = vec.x;
                normals[3*i+1] = vec.y;
                normals[3*i+2] = vec.z;
            }            
        }
        
        public Vector3f getVertex(int vec){
            return new Vector3f(vertices[3*vec],vertices[3*vec+1],vertices[3*vec+2]);
        }
        
        public void setVertex(int i, Vector3f v){
            vertices[3*i] = v.x;            
            vertices[3*i+1] = v.y;            
            vertices[3*i+2] = v.z;            
        }
        
        public Vector3f getNormal(int normal){
            return new Vector3f(normals[3*normal],normals[3*normal+1],normals[3*normal+2]);
        }
        
        public void setNormal(int i, Vector3f n){
            normals[3*i] = n.x;            
            normals[3*i+1] = n.y;            
            normals[3*i+2] = n.z;            
        }

	public float[] getVertices() {
		return vertices;
	}

	public float[] getTextureCoords() {
		return textureCoords;
	}

	public float[] getNormals() {
		return normals;
	}

	public int[] getIndices() {
		return indices;
	}

	public float getFurthestPoint() {
		return furthestPoint;
	}

        public VAO loadTexturedVAO(Loader loader){
            return loader.loadTexturedModelVAO(vertices, textureCoords, normals, indices);
        }

        public VAO loadPlainVAO(Loader loader){
            return loader.loadPlainModelVAO(vertices,  normals, indices);
        }
        
        @Override
        public ModelData clone(){
            float[] vert = new float[vertices.length];
            float[] text = new float[textureCoords.length];
            float[] norm = new float[normals.length];
            int[] indi = new int[indices.length];
            System.arraycopy(vertices, 0, vert, 0, vertices.length);
            System.arraycopy(textureCoords, 0, text, 0, textureCoords.length);
            System.arraycopy(normals, 0, norm, 0, normals.length);
            System.arraycopy(indices, 0, indi, 0, indices.length);
            return new ModelData(vert,text,norm,indi,furthestPoint);
        }
}
