package unknownnick.engine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import unknownnick.engine.render.entity.RenderableEntity;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.DisplayManager;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.render.MasterRenderer;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.gui.GUIRenderer;
import unknownnick.engine.gui.GUITexture;
import unknownnick.engine.gui.font.FontAttributes;
import unknownnick.engine.gui.font.FontType;
import unknownnick.engine.gui.font.GUIText;
import unknownnick.engine.gui.font.render.TextMaster;
import unknownnick.engine.io.file.obj.OBJFileLoader;
import unknownnick.engine.render.WaterRenderer;
import unknownnick.engine.render.buffer.WaterFrameBuffers;
import unknownnick.engine.render.entity.GroupColoredEntity;
import unknownnick.engine.render.entity.GroupTexturedEntity;
import unknownnick.engine.render.entity.Spectator;
import unknownnick.engine.render.entity.TransformationAttributes;
import unknownnick.engine.render.entity.WaterTile;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.render.shader.WaterShader;
import unknownnick.engine.render.texture.ModelTexture;
import unknownnick.engine.render.texture.TerrainTexture;
import unknownnick.engine.render.texture.TerrainTextureGroup;
import unknownnick.engine.shape.Circle;
import unknownnick.engine.shape.ClosedCylinder;
import unknownnick.engine.shape.Cylinder;
import unknownnick.engine.shape.Sphere;
import unknownnick.engine.shape.Square;
import unknownnick.engine.terrain.Terrain;

public class MainGameLoop {
    
    public static void main(String[] args) {
        try{
        //DisplayManager.createDisplay();
        DisplayManager.createAWTDisplay();
        Loader loader = new Loader();
        TextMaster.init(loader);
        Camera camera = new Camera();
        Light light = new Light(new Vector3f(100,100,50), new Vector3f(1,1,1));
        MasterRenderer renderer = new MasterRenderer(loader);
        
        FontAttributes attribs = new FontAttributes(0.1f, 0.5f, true, true, 5f);
        FontType FreeSans = loader.loadFont("FreeSerif", attribs);
        GUIText text = new GUIText("Test 1234567890 abcdABCD ěščřžýáíéúů .,:?-_%=()!",
                FreeSans, new Vector2f(0f, 0f), 1f,
                true, 0.53f, 0.1f, new Vector3f(0.1f, 0f, 0), new Vector2f(0.0004f, 0.0004f));
        text.setColour(0.1f, 0.1f, 0.9f);
        
        VAO model = OBJFileLoader.loadOBJ("stall").loadTexturedVAO(loader);
        ModelTexture texture = new ModelTexture(loader.loadTexture("stall"));
        TexturedModel tmodel = new TexturedModel(model, texture);
        texture.setShineDamper(10);
        texture.setReflectivity(0.7f);
        RenderableEntity entity = new RenderableEntity(tmodel, new Vector3f(0, 0, -15), new Vector3f(0, 0, 0), 1);
        
        ClosedCylinder cyl = new ClosedCylinder(30,20);
        VAO cylinder = cyl.getModel().loadPlainVAO(loader);
        ColoredModel cylmodel = new ColoredModel(cylinder,new Vector3f(0f, 0.5f, 0.9f));
        cylmodel.setReflectivity(0.8f);
        RenderableEntity val = new RenderableEntity(cylmodel, new Vector3f(-3f, 2f, -5f), new Vector3f(60f, 0, 0f), 1f);
        
        
        VAO model1 = OBJFileLoader.loadOBJ("fern").loadTexturedVAO(loader);
        ModelTexture fernTexture = new ModelTexture(loader.loadTexture("fern_atlas"));
        fernTexture.setNumberOfRows(2);
        fernTexture.setHasTransparency(true);
        fernTexture.setUseFakeLight(true);
        TexturedModel tmodel1 = new TexturedModel(model1, fernTexture);
        fernTexture.setShineDamper(10);
        fernTexture.setReflectivity(0.7f);
        RenderableEntity entity1 = new RenderableEntity(tmodel1, new Vector3f(0, 0, -5), new Vector3f(0, 0, 0), 0.33f);
        
        VAO fernVAO = OBJFileLoader.loadOBJ("fern").loadTexturedVAO(loader);
        TexturedModel fernModel = new TexturedModel(fernVAO, fernTexture);
        Random r = new Random();
        GroupTexturedEntity ferns = new GroupTexturedEntity(fernModel);
        for(int i = 0;i<200;i++){
            ferns.add(new Vector3f(r.nextInt(300)-150, 0, r.nextInt(300)-150),
                    new Vector3f(0, r.nextFloat()*360, 0) , r.nextFloat()*0.66f, r.nextInt(4), 10, 0.2f);
        }
        
        Circle sq = new Circle();
        VAO square = sq.getModel().loadPlainVAO(loader);
        TexturedModel sqmodel = new TexturedModel(square, texture);
        RenderableEntity rect = new RenderableEntity(sqmodel, new Vector3f(1f,0f,0f), new Vector3f(0,0,0), 2f, 1);
        
        Sphere sph = new Sphere(3);
        VAO sphere = sph.getModel().loadPlainVAO(loader);
        ColoredModel cmodel = new ColoredModel(sphere,new Vector3f(1f,0f,0f));
        RenderableEntity ball = new RenderableEntity(cmodel, new Vector3f(10f,15f,-30f), new Vector3f(0,0,0), 12f);
        
        ColoredModel sunmodel = new ColoredModel(sphere, new Vector3f(0.85f,0.95f,0.25f));
        sunmodel.setUseFakeLight(true);
        RenderableEntity sun = new RenderableEntity(sunmodel, light.getPosition(), new Vector3f(0,0,0), 1.5f);
        
        TerrainTexture background = new TerrainTexture(loader.loadTexture("grass"));
        TerrainTexture terR = new TerrainTexture(loader.loadTexture("dirt"));
        TerrainTexture terG = new TerrainTexture(loader.loadTexture("pinkFlowers"));
        TerrainTexture terB = new TerrainTexture(loader.loadTexture("path"));
        TerrainTextureGroup tgroup = new TerrainTextureGroup(background, terR, terG, terB);
        TerrainTexture blendmap = new TerrainTexture(loader.loadTexture("blendmap"));
        
        Terrain terrain = new Terrain(0, -1, loader, tgroup, blendmap);
        Terrain terrain2 = new Terrain(-1, -1, loader, tgroup, blendmap);
        Terrain terrain3 = new Terrain(-1, 0, loader, tgroup, blendmap);
        Terrain terrain4 = new Terrain(0, 0, loader, tgroup, blendmap);
        
        Spectator spectator = new Spectator(tmodel1, camera.getPosition().translate(5, 5, 5),
                new Vector3f(camera.getPitch(), camera.getYaw(), camera.getRoll()), 
                0.1f, camera);
        
        
        List<GUITexture> guis = new ArrayList<>();
        GUITexture gui = new GUITexture(loader.loadTexture("flower"), new Vector2f(0.5f, 0.5f), new Vector2f(1,1));
        //guis.add(gui);
        
        GUIRenderer guiRenderer = new GUIRenderer(loader);
        
        WaterFrameBuffers fbos = new WaterFrameBuffers();
        
        WaterRenderer waterRenderer = new WaterRenderer(loader, new WaterShader(), renderer.getProjectionMatrix(), fbos);
        List<WaterTile> waters = new ArrayList<>();
        WaterTile water = new WaterTile(0, 0, 2);
        WaterTile water1 = new WaterTile(WaterTile.TILE_SIZE*2, 0, 2);
        water1.setColour(new Vector3f(0.7f, 0.5f, 0.3f));
        waters.add(water);
        waters.add(water1);
        
        VAO spher = sph.getModel().loadPlainVAO(loader);
        ColoredModel cmod = new ColoredModel(spher,new Vector3f(0.3f,0f,0.8f));
        GroupColoredEntity group = new GroupColoredEntity(cmod);
        group.add(new TransformationAttributes(new Vector3f(0, 1, 0), new Vector3f(0,0,0), 2f));
        group.add(new TransformationAttributes(new Vector3f(0, -3, 5), new Vector3f(0,0,0), 0.2f));
        
        double time = 0;
        int frames = 0;
        
        System.out.println(GL11.glGetString(GL11.GL_VERSION));
        
        List<Object> entities = new ArrayList<>();
        entities.add(terrain);
        entities.add(terrain2);
        entities.add(terrain3);
        entities.add(terrain4);
        entities.add(entity);
        entities.add(entity1);
        entities.add(ball);
        entities.add(sun);
        entities.add(group);
//        entities.add(ferns);
        entities.add(val);
        
        while (!(Display.isCloseRequested() | DisplayManager.getCloseRequested())) {
            DisplayManager.checkResize(renderer);
            spectator.move();
            val.rotate(new Vector3f(-0.4f, 1f, 1f));
            
            GL11.glEnable(GL30.GL_CLIP_DISTANCE0);
            
            fbos.bindReflectionFrameBuffer();
            float distance = 2*(camera.getPosition().y - water.getHeight());
            camera.getPosition().y -= distance;
            camera.invertPitch();
            renderer.renderScene(entities, light, camera, new Vector4f(0, 1, 0, -water.getHeight()));
            camera.getPosition().y += distance;
            camera.invertPitch();
            
            fbos.bindRefractionFrameBuffer();
            renderer.renderScene(entities, light, camera, new Vector4f(0, -1, 0, water.getHeight()));
            
            GL11.glDisable(GL30.GL_CLIP_DISTANCE0);
            fbos.unbindCurrentFrameBuffer();
            renderer.renderScene(entities, light, camera, new Vector4f(0, 0, 0, 0));
            waterRenderer.render(waters, camera, light);
            guiRenderer.render(guis);
            TextMaster.render();
            DisplayManager.updateDisplay();
            frames++;
            time += DisplayManager.getframeTimems();
            if(time > 1000){
                System.out.println("fps:"+frames);
                time = 0;
                frames = 0;
            }
        }
        
        fbos.cleanUp();
        waterRenderer.cleanUp();
        TextMaster.cleanUp();
        guiRenderer.cleanUp();
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }
}
