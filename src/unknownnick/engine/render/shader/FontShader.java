package unknownnick.engine.render.shader;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;


public class FontShader extends ShaderProgram {

	private static final String VERTEX =   "/unknownnick/engine/render/shader/fontVertex.glsl";
	private static final String FRAGMENT = "/unknownnick/engine/render/shader/fontFragment.glsl";
        
        protected int location_colour;
        protected int location_translation;
        protected int location_width;
        protected int location_edge;
        protected int location_border_width;
        protected int location_border_edge;
        protected int location_border_colour;
        protected int location_offset;
	
	public FontShader() {
            super(VERTEX, FRAGMENT);
            super.getAllUniformLocations(FontShader.class, this);
	}


	@Override
	protected void bindAttributes() {
            super.bindAttribute(0, "position");
            super.bindAttribute(1, "texcoords");
        }

        public void loadColour(Vector3f colour){
            super.loadVector3(location_colour, colour);
        }
        
        public void loatTranslation(Vector2f translation){
            super.loadVector2(location_translation, translation);
        }
        
        public void loadEgdeSmoothing(float width, float edge){
            super.loadFloat(location_width, width);
            super.loadFloat(location_edge, edge);
        }
        
        public void loadBorder(float width, float edge, Vector3f colour, Vector2f offset){
            super.loadFloat(location_border_width, width);
            super.loadFloat(location_border_edge, edge);
            super.loadVector3(location_border_colour, colour);
            super.loadVector2(location_offset, offset);
        }
        
        public void loadDefault(){
            super.loadFloat(location_width, 0.52f);
            super.loadFloat(location_edge, 0.02f);
            super.loadFloat(location_border_width, 0);
            super.loadFloat(location_border_edge, 0.1f);
            super.loadVector3(location_border_colour, new Vector3f(0, 0, 0));
            super.loadVector2(location_offset, new Vector2f(0, 0));
        }
}
