#version 130

in vec2 pass_texture;
in vec3 surface_normal;
in vec3 to_light;
in vec3 to_camera;
in float visibility;

out vec4 out_colour;

uniform sampler2D background_sampler;
uniform sampler2D r_sampler;
uniform sampler2D g_sampler;
uniform sampler2D b_sampler;
uniform sampler2D blendmap_sampler;

uniform vec3 light_colour;
uniform float shine_damper;
uniform float reflectivity;
uniform vec3 sky_colour;

void main(void) {
    vec4 blend = texture(blendmap_sampler,pass_texture);
    
    float background = 1 - (blend.r + blend.g + blend.b);
    vec2 coords = pass_texture * 69;
    vec4 background_color = texture(background_sampler,coords) * background;
    vec4 r_color = texture(r_sampler,coords) * blend.r;
    vec4 g_color = texture(g_sampler,coords) * blend.g;
    vec4 b_color = texture(b_sampler,coords) * blend.b;

    vec4 final = background_color + r_color + g_color + b_color;
    
    
    vec3 unit_normal = normalize(surface_normal);
    vec3 unit_to_light = normalize(to_light);
    float ldot = dot(unit_normal, unit_to_light);
    float brightness = max(ldot,0.2);
    vec3 diffuse = brightness * light_colour;

    vec3 unit_to_camera = normalize(to_camera);
    vec3 light_dir = -unit_to_light;
    vec3 reflected_dir = reflect(light_dir, unit_normal);
    float specular_factor = dot(reflected_dir, unit_to_camera);
    specular_factor = max(specular_factor, 0.0);
    float damped_factor = pow(specular_factor, shine_damper);
    vec3 specular_light = damped_factor * reflectivity * light_colour;

    out_colour = vec4(diffuse,1.0) * final + vec4(specular_light, 1.0);
    out_colour = mix(vec4(sky_colour,1.0),out_colour,visibility);
}

