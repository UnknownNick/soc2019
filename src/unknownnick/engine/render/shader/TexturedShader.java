package unknownnick.engine.render.shader;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.util.Color;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.io.file.Locations;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.util.math.Maths;


public class TexturedShader extends ShaderProgram {

    private static final String VERTEX =    "/unknownnick/engine/render/shader/vertexShader.glsl";
    private static final String FRAGMENT =  "/unknownnick/engine/render/shader/fragmentShader.glsl";
    
    protected int location_transformation_matrix;
    protected int location_projection_matrix;
    protected int location_view_matrix;
    protected int location_camera_position;
    protected int location_light_position;
    protected int location_light_colour;
    protected int location_shine_damper;
    protected int location_reflectivity;
    protected int location_use_fake_light;
    protected int location_sky_colour;
    protected int location_rows;
    protected int location_offset;
    protected int location_plane;
    
    public TexturedShader() {
        super(VERTEX, FRAGMENT);
        super.getAllUniformLocations(TexturedShader.class, this);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position"); 
        super.bindAttribute(1, "texture_coords");
        super.bindAttribute(2, "normal");
    }
    
    public void loadPlane(Vector4f plane){
        super.loadVector4(location_plane, plane);
    }
    
    public void loadOffset(Vector2f offset){
        super.loadVector2(location_offset, offset);
    }
    
    public void loadRows(int rows){
        super.loadFloat(location_rows, rows);
    }
    
    public void loadSkyColour(Vector3f colour){
        super.loadVector3(location_sky_colour, colour);
    }
    
    public void loadUseFakeLight(boolean use){
        super.loadBoolean(location_use_fake_light, use);
    }
    
    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(location_transformation_matrix, matrix);
    }
    
    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projection_matrix, matrix);
    }
    
    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadVector3(location_camera_position, camera.getPosition());
        super.loadMatrix(location_view_matrix, matrix);
    }
    
    public void loadLight(Light light) {
        super.loadVector3(location_light_position, light.getPosition());
        super.loadVector3(location_light_colour, light.getColour());
    }
    
    public void loadShinyness(float shineDamper, float reflectivity) {
        super.loadFloat(location_shine_damper, shineDamper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
    
    
}
