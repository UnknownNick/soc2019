package unknownnick.engine.render.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.io.file.Locations;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.util.math.Maths;


public class ColoredShader extends ShaderProgram {

    private static final String VERTEX =    "/unknownnick/engine/render/shader/cvertexShader.glsl";
    private static final String FRAGMENT =  "/unknownnick/engine/render/shader/cfragmentShader.glsl";
    
    protected int location_transformation_matrix;
    protected int location_projection_matrix;
    protected int location_view_matrix;
    protected int location_camera_position;
    protected int location_light_position;
    protected int location_light_colour;
    protected int location_shine_damper;
    protected int location_reflectivity;
    protected int location_use_fake_light;
    protected int location_sky_colour;
    protected int location_in_colour;
    protected int location_plane;
    
    public ColoredShader() {
        super(VERTEX, FRAGMENT);
        super.getAllUniformLocations(ColoredShader.class, this);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "normal");
    }
    
    public void loadPlane(Vector4f plane){
        super.loadVector4(location_plane, plane);
    }
    
    public void loadColour(Vector3f colour){
        super.loadVector3(location_in_colour, colour);
    }
    
    public void loadSkyColour(Vector3f colour){
        super.loadVector3(location_sky_colour, colour);
    }
    
    public void loadUseFakeLight(boolean use){
        super.loadBoolean(location_use_fake_light, use);
    }
    
    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(location_transformation_matrix, matrix);
    }
    
    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projection_matrix, matrix);
    }
    
    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadVector3(location_camera_position, camera.getPosition());
        super.loadMatrix(location_view_matrix, matrix);
    }
    
    public void loadLight(Light light) {
        super.loadVector3(location_light_position, light.getPosition());
        super.loadVector3(location_light_colour, light.getColour());
    }
    
    public void loadShinyness(float shineDamper, float reflectivity) {
        super.loadFloat(location_shine_damper, shineDamper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
    
    
}
