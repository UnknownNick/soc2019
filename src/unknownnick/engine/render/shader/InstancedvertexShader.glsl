#version 130

in vec3 position;
in vec2 texture_coords;
in vec3 normal;
in mat4 transformation_matrix;
in vec2 offset;
in float shine_damper_in;
in float reflectivity_in;

out vec2 pass_texture;
out vec3 surface_normal;
out vec3 to_light;
out vec3 to_camera;
out float visibility;
out float shine_damper;
out float reflectivity;


uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform vec3 camera_position;
uniform vec3 light_position;
uniform float rows;
uniform float use_fake_light;
uniform vec4 plane;

const float density = 0.002;
const float gradient = 4;

void main(void) {
    shine_damper = shine_damper_in;
    reflectivity = reflectivity_in;
    
    vec4 world_pos = transformation_matrix * vec4(position, 1.0);
    vec4 pos_to_camera = view_matrix * world_pos;
    gl_Position = projection_matrix * pos_to_camera;
    pass_texture = (texture_coords/rows)+offset;

    gl_ClipDistance[0] = dot(world_pos, plane);

    vec3 the_normal = normal;
    if(use_fake_light>0.5){
        the_normal= vec3(0,1,0);
    }

    surface_normal = (transformation_matrix * vec4(the_normal,0.0)).xyz;
    to_light = light_position - world_pos.xyz;
    to_camera = camera_position - world_pos.xyz;

    float distance = length(pos_to_camera.xyz);
    visibility = exp(-pow((distance*density),gradient));
    visibility = clamp(visibility,0,1);
}