package unknownnick.engine.render.shader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.FloatBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;


public abstract class ShaderProgram {
    
    private final int programID;
    private final int vertexShaderID;
    private final int fragmentShaderID;
    private final FloatBuffer fbuff = BufferUtils.createFloatBuffer(16);
    
    public ShaderProgram(String vertexFile, String fragmentFile) {
        this.vertexShaderID = loadShader(vertexFile, GL20.GL_VERTEX_SHADER);
        this.fragmentShaderID = loadShader(fragmentFile, GL20.GL_FRAGMENT_SHADER);
        this.programID = GL20.glCreateProgram();
        GL20.glAttachShader(programID, this.vertexShaderID);
        GL20.glAttachShader(programID, this.fragmentShaderID);
        this.bindAttributes();
        GL20.glLinkProgram(programID);
        GL20.glValidateProgram(programID);
    }
    
    protected abstract void bindAttributes();
    
    protected void bindAttribute(int attribute, String variableName) {
        GL20.glBindAttribLocation(programID, attribute, variableName);
    }
    
    public void start() {
        GL20.glUseProgram(programID);
    }
    
    public void stop() {
        GL20.glUseProgram(0);
    }
    
    public void cleanUp() {
        this.stop();
        GL20.glDetachShader(programID, this.vertexShaderID);
        GL20.glDetachShader(programID, this.fragmentShaderID);
        GL20.glDeleteShader(this.vertexShaderID);
        GL20.glDeleteShader(this.fragmentShaderID);
        GL20.glDeleteProgram(programID);
    }
    
    private static int loadShader(String file, int type) {
        StringBuilder shaderSource = new StringBuilder();
        try{
            InputStream in = Class.class.getResourceAsStream(file);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = reader.readLine())!=null){
                shaderSource.append(line).append("//\n");
            }
            reader.close();
        }catch(IOException e){
            e.printStackTrace();
            System.exit(-1);
        }
        int shaderID = GL20.glCreateShader(type);
        GL20.glShaderSource(shaderID, shaderSource);
        GL20.glCompileShader(shaderID);
        if(GL20.glGetShaderi(shaderID, GL20.GL_COMPILE_STATUS )== GL11.GL_FALSE){
            System.out.println(GL20.glGetShaderInfoLog(shaderID, 500));
            System.err.println("Could not compile shader! " + file);
            System.exit(-1);
        }
        return shaderID;
    }
    
    protected void loadInt(int location, int value){
        GL20.glUniform1i(location, value);
    }
    
    protected void loadFloat(int location, float value) {
        GL20.glUniform1f(location, value);
    }
    
    protected void loadVector2(int location, Vector2f vec) {
        GL20.glUniform2f(location, vec.x, vec.y);
    }
    
    protected void loadVector3(int location, Vector3f vec) {
        GL20.glUniform3f(location, vec.x, vec.y, vec.z);
    }
    
    protected void loadVector4(int location, Vector4f vec) {
        GL20.glUniform4f(location, vec.x, vec.y, vec.z, vec.w);
    }
    
    protected void loadBoolean(int location, boolean value) {
        GL20.glUniform1f(location, (value ? 1.0f : 0.0f));
    }
    
    protected void loadMatrix(int location, Matrix4f matrix) {
        matrix.store(fbuff);
        fbuff.flip();
        GL20.glUniformMatrix4(location, false, fbuff);
    }
    /*
    * Has to be called from subclasse's constructor
    */
    protected <S extends ShaderProgram> void getAllUniformLocations(Class<S> clazz, S shader){
        String prefix = "location_";
        int len = prefix.length();
        for(Field f : clazz.getDeclaredFields()){
            String name = f.getName();
            if(f.getType().equals(Integer.TYPE) && name.startsWith(prefix)){
                try {
                    int location = getUniformLocation(name.substring(len));
                    if(location == -1){
                        Logger.getLogger(TexturedShader.class.getName()).log(Level.WARNING,
                            "location of uniform for <{0}.{1}> returned -1.", new Object[]{clazz.getName(), name});
                    }
                    f.setInt(shader, location);
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(TexturedShader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    protected int getUniformLocation(String uniformName) {
        return GL20.glGetUniformLocation(programID, uniformName);
    }
}
