#version 130

in vec2 position;

out vec4 clipspace;
out vec2 texcoords;
out vec3 to_camera;
out vec3 to_light;

uniform mat4 transformation_matrix;
uniform mat4 projection_matrix;
uniform mat4 view_matrix;
uniform vec3 camera;
uniform vec3 light_position;

const float tiling = 4.0;

void main(void) {
    texcoords = (position.xy / 2.0 + 0.5) * tiling;
    vec4 world_pos = transformation_matrix * vec4(position.x, 0.0, position.y, 1.0);
    clipspace = projection_matrix * view_matrix * world_pos;
    to_camera = camera - world_pos.xyz;
    gl_Position = clipspace;
    to_light = world_pos.xyz-light_position;
}

