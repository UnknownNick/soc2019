package unknownnick.engine.render.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.io.file.Locations;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.util.math.Maths;

public class TerrainShader extends ShaderProgram {

    private static final String VERTEX =    "/unknownnick/engine/render/shader/terrainVertexShader.glsl";
    private static final String FRAGMENT =  "/unknownnick/engine/render/shader/terrainFragmentShader.glsl";
    
    protected int location_transformation_matrix;
    protected int location_projection_matrix;
    protected int location_view_matrix;
    protected int location_camera_position;
    protected int location_light_position;
    protected int location_light_colour;
    protected int location_shine_damper;
    protected int location_reflectivity;
    protected int location_sky_colour;
    protected int location_background_sampler;
    protected int location_r_sampler;
    protected int location_g_sampler;
    protected int location_b_sampler;
    protected int location_blendmap_sampler;
    protected int location_plane;
    
    public TerrainShader() {
        super(VERTEX, FRAGMENT);
        super.getAllUniformLocations(TerrainShader.class, this);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position"); 
        super.bindAttribute(1, "texture_coords");
        super.bindAttribute(2, "normal");
    }
    
    public void connectTextureUnits(){
        super.loadInt(location_background_sampler, 0);
        super.loadInt(location_r_sampler, 1);
        super.loadInt(location_g_sampler, 2);
        super.loadInt(location_b_sampler, 3);
        super.loadInt(location_blendmap_sampler, 4);
    }
    
    public void loadPlane(Vector4f plane){
        super.loadVector4(location_plane, plane);
    }
    
    public void loadSkyColour(Vector3f colour){
        super.loadVector3(location_sky_colour, colour);
    }
    
    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(location_transformation_matrix, matrix);
    }
    
    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projection_matrix, matrix);
    }
    
    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadVector3(location_camera_position, camera.getPosition());
        super.loadMatrix(location_view_matrix, matrix);
    }
    
    public void loadLight(Light light) {
        super.loadVector3(location_light_position, light.getPosition());
        super.loadVector3(location_light_colour, light.getColour());
    }
    
    public void loadShinyness(float shineDamper, float reflectivity) {
        super.loadFloat(location_shine_damper, shineDamper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
    
}
