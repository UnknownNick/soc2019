package unknownnick.engine.render.shader;

import org.lwjgl.util.vector.Matrix4f;
import unknownnick.engine.io.file.Locations;
import unknownnick.engine.render.shader.ShaderProgram;

public class GUIShader extends ShaderProgram {
    private static final String VERTEX_FILE =   "/unknownnick/engine/render/shader/guiVertexShader.glsl";
    private static final String FRAGMENT_FILE = "/unknownnick/engine/render/shader/guiFragmentShader.glsl";
     
    protected int location_transformation_matrix;
 
    public GUIShader() {
        super(VERTEX_FILE, FRAGMENT_FILE);
        super.getAllUniformLocations(GUIShader.class, this);
    }
     
    public void loadTransformation(Matrix4f matrix){
        super.loadMatrix(location_transformation_matrix, matrix);
    }
 
    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
}
