#version 130

in vec2 pass_texcoords;

out vec4 out_colour;

uniform vec3 colour;
uniform sampler2D fontAtlas;
uniform float width;
uniform float edge;
uniform float border_width;
uniform float border_edge;
uniform vec3  border_colour;
uniform vec2  offset;

void main(void){
    float distance = 1.0 - texture(fontAtlas, pass_texcoords).a;
    float alpha = 1.0 - smoothstep(width, width + edge, distance);

    float border_distance = 1.0 - texture(fontAtlas, pass_texcoords + offset).a;
    float border_alpha = 1.0 - smoothstep(border_width, border_width + border_edge, border_distance);

    float final_alpha = alpha + (1.0 - alpha) * border_alpha;
    vec3 final_colour = mix(border_colour, colour, alpha / border_alpha);
    
    out_colour = vec4(final_colour, final_alpha);
}