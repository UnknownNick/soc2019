#version 130

in vec4 clipspace;
in vec2 texcoords;
in vec3 to_camera;
in vec3 to_light;

out vec4 out_colour;

uniform sampler2D reflection;
uniform sampler2D refraction;
uniform sampler2D dudv;
uniform sampler2D normal_map;
uniform sampler2D depth_map;
uniform vec3 light_colour;
uniform vec3 colour;
uniform float move_factor;
uniform float near;
uniform float far;
uniform float wave_strength;
uniform float shine_damper;
uniform float reflectivity;

void main(void){
    vec2 ndc = clipspace.xy/clipspace.w;
    vec2 ref_texcoords = ndc/2.0+0.5;

    float depth = texture(depth_map, ref_texcoords).r;
    float floor_distance = 2*near*far/(far+near-(2.0*depth-1)*(far-near));
    depth = gl_FragCoord.z;
    float water_distance = 2*near*far/(far+near-(2.0*depth-1)*(far-near));
    depth = floor_distance - water_distance;

    vec2 distorted = texture(dudv, vec2(texcoords.x + move_factor, texcoords.y)).rg*0.1;
    distorted = texcoords + vec2(distorted.x, distorted.y+move_factor);
    vec2 total_distortion = (texture(dudv, distorted).rg * 2.0 - 1.0) * wave_strength * clamp(depth/5, 0, 1);
    ref_texcoords += total_distortion;
    ref_texcoords = clamp(ref_texcoords, 0.001, 0.999);

    vec4 reflection_colour = texture(reflection, vec2(ref_texcoords.x,-ref_texcoords.y));
    vec4 refraction_colour = texture(refraction, ref_texcoords);
    vec4 normal_colour = texture(normal_map, distorted);
    vec3 normal = vec3(normal_colour.r * 2 -1, normal_colour.b * 3, normal_colour.g * 2 -1);
    normal = normalize(normal);
    
    vec3 view = normalize(to_camera);
    float ref_factor = dot(view, normal);
    ref_factor = pow(ref_factor, 2);

    vec3 reflected_light = reflect(normalize(to_light), normal);
    float specular = max(dot(reflected_light, view), 0);
    specular = pow(specular, shine_damper);
    vec3 specular_highlights = light_colour * specular * reflectivity * clamp(depth/5, 0, 1);

    out_colour = mix(reflection_colour, refraction_colour, ref_factor);
    out_colour = mix(out_colour, vec4(colour,1), 0.2) + vec4(specular_highlights, 0.0);
    out_colour.a = clamp(depth/2, 0, 1);
}