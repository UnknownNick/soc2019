package unknownnick.engine.render.shader;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.util.math.Maths;

public class WaterShader extends ShaderProgram {
    private static final String VERTEX =    "/unknownnick/engine/render/shader/watervertexShader.glsl";
    private static final String FRAGMENT =  "/unknownnick/engine/render/shader/waterfragmentShader.glsl";
    
    protected int location_transformation_matrix;
    protected int location_projection_matrix;
    protected int location_view_matrix;
    protected int location_reflection;
    protected int location_refraction;
    protected int location_dudv;
    protected int location_move_factor;
    protected int location_camera;
    protected int location_normal_map;
    protected int location_depth_map;
    protected int location_colour;
    protected int location_light_position;
    protected int location_light_colour;
    protected int location_near;
    protected int location_far;
    protected int location_wave_strength;
    protected int location_shine_damper;
    protected int location_reflectivity;
    
    
    public WaterShader() {
        super(VERTEX, FRAGMENT);
        super.getAllUniformLocations(WaterShader.class, this);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
    }
    
    public void connectTextureUnits(){
        super.loadInt(location_reflection, 0);
        super.loadInt(location_refraction, 1);
        super.loadInt(location_dudv, 2);
        super.loadInt(location_normal_map, 3);
        super.loadInt(location_depth_map, 4);
    }
    
    public void loadWaveStrength(float wave_strength){
        super.loadFloat(location_wave_strength, wave_strength);
    }
    
    public void loadFrustrum(float near, float far){
        super.loadFloat(location_near, near);
        super.loadFloat(location_far, far);
    }
    
    public void loadShinyness(float shine_damper, float reflectivity){
        super.loadFloat(location_shine_damper, shine_damper);
        super.loadFloat(location_reflectivity, reflectivity);
    }
    
    public void loadLight(Light light){
        super.loadVector3(location_light_position, light.getPosition());
        super.loadVector3(location_light_colour, light.getColour());
    }
    
    public void loadColour(Vector3f colour){
        super.loadVector3(location_colour, colour);
    }
    
    public void loadCameraPosition(Camera camera){
        super.loadVector3(location_camera, camera.getPosition());
    }
    
    public void loadMoveFactor(float move){
        super.loadFloat(location_move_factor, move);
    }
    
    public void loadTransformationMatrix(Matrix4f matrix) {
        super.loadMatrix(location_transformation_matrix, matrix);
    }
    
    public void loadProjectionMatrix(Matrix4f matrix) {
        super.loadMatrix(location_projection_matrix, matrix);
    }
    
    public void loadViewMatrix(Camera camera) {
        Matrix4f matrix = Maths.createViewMatrix(camera);
        super.loadMatrix(location_view_matrix, matrix);
    }
}
