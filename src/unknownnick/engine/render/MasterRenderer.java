package unknownnick.engine.render;

import unknownnick.engine.render.entity.Light;
import unknownnick.engine.render.entity.RenderableEntity;
import unknownnick.engine.render.entity.Camera;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.entity.GroupColoredEntity;
import unknownnick.engine.render.entity.GroupTexturedEntity;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.render.model.ModelType;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.render.shader.ColoredShader;
import unknownnick.engine.render.shader.InstancedColoredShader;
import unknownnick.engine.render.shader.InstancedTexturedShader;
import unknownnick.engine.render.shader.TexturedShader;
import unknownnick.engine.render.shader.TerrainShader;
import unknownnick.engine.terrain.Terrain;


public class MasterRenderer {
    
    private TexturedShader texturedShader;
    private TexturedRenderer texturedRenderer;
    private ColoredShader coloredShader;
    private ColoredRenderer coloredRenderer;
    private TerrainShader terrainShader;
    private TerrainRenderer terrainRenderer;
    private InstancedColoredShader cinsShader;
    private InstancedColoredRenderer cinsRenderer;
    private InstancedTexturedShader tinsShader;
    private InstancedTexturedRenderer tinsRenderer;
    
    private Map<TexturedModel, List<RenderableEntity>> texturedEntities = new HashMap<>();
    private Map<ColoredModel, List<RenderableEntity>> coloredEntities = new HashMap<>();
    private List<Terrain> terrains = new ArrayList<>();
    private Map<ColoredModel, List<GroupColoredEntity>> cinstancedEntities = new HashMap<>();
    private Map<TexturedModel, List<GroupTexturedEntity>> tinstancedEntities = new HashMap<>();
    
    public static final float FOV = 70;
    public static final float NEAR_PLANE = 0.1f;
    public static final float FAR_PLANE = 1000;
    private static final Vector3f skyColour = new Vector3f(0.6f,0.6f,0.6f);
    
    private Matrix4f projectionMatrix;
    
    public MasterRenderer(Loader loader) {
        enableCulling();
        this.createProjectionMatrix();
        //init//////////////////////////////////////////////////////////////////
        texturedShader = new TexturedShader();
        texturedRenderer = new TexturedRenderer(texturedShader);
        
        coloredShader = new ColoredShader();
        coloredRenderer = new ColoredRenderer(coloredShader);
        
        terrainShader = new TerrainShader();
        terrainRenderer = new TerrainRenderer(terrainShader);
        
        cinsShader = new InstancedColoredShader();
        cinsRenderer = new InstancedColoredRenderer(cinsShader, loader);
        
        tinsShader = new InstancedTexturedShader();
        tinsRenderer = new InstancedTexturedRenderer(tinsShader, loader);
        //load//////////////////////////////////////////////////////////////////
        texturedShader.start();
        texturedShader.loadProjectionMatrix(projectionMatrix);
        texturedShader.stop();
        
        coloredShader.start();
        coloredShader.loadProjectionMatrix(projectionMatrix);
        coloredShader.stop();
        
        terrainShader.start();
        terrainShader.connectTextureUnits();
        terrainShader.loadProjectionMatrix(projectionMatrix);
        terrainShader.stop();
        
        cinsShader.start();
        cinsShader.loadProjectionMatrix(projectionMatrix);
        cinsShader.stop();
        
        tinsShader.start();
        tinsShader.loadProjectionMatrix(projectionMatrix);
        tinsShader.stop();
    }
    
    public void reloadProjectionmatrix(){
        this.createProjectionMatrix();
        texturedShader.start();
        texturedShader.loadProjectionMatrix(projectionMatrix);
        texturedShader.stop();
        
        coloredShader.start();
        coloredShader.loadProjectionMatrix(projectionMatrix);
        coloredShader.stop();
        
        terrainShader.start();
        terrainShader.loadProjectionMatrix(projectionMatrix);
        terrainShader.stop();
        
        cinsShader.start();
        cinsShader.loadProjectionMatrix(projectionMatrix);
        cinsShader.stop();
        
        tinsShader.start();
        tinsShader.loadProjectionMatrix(projectionMatrix);
        tinsShader.stop();
    }
    
    public void render(Light sun, Camera camera, Vector4f clipPlane) {
        this.prepare();
        //render textured///////////////////////////////////////////////////////
        texturedShader.start();
        texturedShader.loadLight(sun);
        texturedShader.loadViewMatrix(camera);
        texturedShader.loadSkyColour(skyColour);
        texturedShader.loadPlane(clipPlane);
        
        texturedRenderer.render(texturedEntities);
        
        texturedShader.stop();
        texturedEntities.clear();
        //render colored////////////////////////////////////////////////////////
        coloredShader.start();
        coloredShader.loadLight(sun);
        coloredShader.loadViewMatrix(camera);
        coloredShader.loadSkyColour(skyColour);
        coloredShader.loadPlane(clipPlane);
        
        coloredRenderer.render(coloredEntities);
        
        coloredShader.stop();
        coloredEntities.clear();
        //render terrains///////////////////////////////////////////////////////
        terrainShader.start();
        terrainShader.loadLight(sun);
        terrainShader.loadViewMatrix(camera);
        terrainShader.loadSkyColour(skyColour);
        terrainShader.loadPlane(clipPlane);
        
        terrainRenderer.render(terrains);
        
        terrainShader.stop();
        terrains.clear();
        //render colored instances//////////////////////////////////////////////
        cinsShader.start();
        cinsShader.loadLight(sun);
        cinsShader.loadViewMatrix(camera);
        cinsShader.loadSkyColour(skyColour);
        cinsShader.loadPlane(clipPlane);
        
        cinsRenderer.render(cinstancedEntities);
        
        cinsShader.stop();
        cinstancedEntities.clear();
        //rendeer textured instances////////////////////////////////////////////
        tinsShader.start();
        tinsShader.loadLight(sun);
        tinsShader.loadViewMatrix(camera);
        tinsShader.loadSkyColour(skyColour);
        tinsShader.loadPlane(clipPlane);
        
        tinsRenderer.render(tinstancedEntities);
        
        tinsShader.stop();
        tinstancedEntities.clear();
    }
    
    public void renderScene(List<Object> objects, Light light, Camera camera, Vector4f clipPlane){
        for(Object o : objects){
            if(o instanceof RenderableEntity){
                this.processEntity((RenderableEntity)o);
            }
            else if(o instanceof GroupColoredEntity){
                this.processGroupColoredEntity((GroupColoredEntity)o);
            }
            else if(o instanceof GroupTexturedEntity){
                this.processGroupTexturedEntity((GroupTexturedEntity)o);
            }
            else if(o instanceof Terrain){
                this.processTerrain((Terrain)o);
            }
        }
        this.render(light, camera, clipPlane);
    }
    
    public void prepare() {
        GL11.glEnable(GL11.GL_DEPTH_TEST);
        GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
        GL11.glClearColor(skyColour.x,skyColour.y,skyColour.z,1);
    }
    
    public static void enableCulling(){
        GL11.glEnable(GL11.GL_CULL_FACE);
        GL11.glCullFace(GL11.GL_BACK);
    }
    
    public Matrix4f getProjectionMatrix(){
        return this.projectionMatrix;
    }
    
    public static void disableCulling(){
        GL11.glDisable(GL11.GL_CULL_FACE);
    }
    
    public void processTerrain(Terrain terrrain) {
        terrains.add(terrrain);
    }
    
    public void processGroupColoredEntity(GroupColoredEntity entity){
        ColoredModel model = entity.getCmodel();
        List<GroupColoredEntity> batch = cinstancedEntities.get(model);
            if(batch!=null) {
                batch.add(entity);
            } else {
                batch = new ArrayList<>();
                batch.add(entity);
                cinstancedEntities.put(model, batch);
            }
    }
    
    public void processGroupTexturedEntity(GroupTexturedEntity entity){
        TexturedModel model = entity.getModel();
        List<GroupTexturedEntity> batch = tinstancedEntities.get(model);
            if(batch!=null) {
                batch.add(entity);
            } else {
                batch = new ArrayList<>();
                batch.add(entity);
                tinstancedEntities.put(model, batch);
            }
    }
    
    public void processEntity(RenderableEntity entity) {
        if(entity.getModelType()==ModelType.TEXTURE) {
            TexturedModel model = entity.getTModel();
            List<RenderableEntity> batch = texturedEntities.get(model);
            if(batch!=null) {
                batch.add(entity);
            } else {
                batch = new ArrayList<>();
                batch.add(entity);
                texturedEntities.put(model, batch);
            }
        } else if(entity.getModelType()==ModelType.COLOR) {
            ColoredModel model = entity.getCmodel();
            List<RenderableEntity> batch = coloredEntities.get(model);
            if(batch!=null) {
                batch.add(entity);
            } else {
                batch = new ArrayList<>();
                batch.add(entity);
                coloredEntities.put(model, batch);
            }            
        }
    }
    
    private void createProjectionMatrix() {
        float aspectRatio = (float) Display.getWidth() / (float) Display.getHeight();
        float y_scale = (float) (1f / Math.tan(Math.toRadians(FOV / 2f)));
        float x_scale = y_scale / aspectRatio;
        float frustum_length = FAR_PLANE - NEAR_PLANE;
 
        projectionMatrix = new Matrix4f();
        projectionMatrix.m00 = x_scale;
        projectionMatrix.m11 = y_scale;
        projectionMatrix.m22 = -((FAR_PLANE + NEAR_PLANE) / frustum_length);
        projectionMatrix.m23 = -1;
        projectionMatrix.m32 = -((2 * NEAR_PLANE * FAR_PLANE) / frustum_length);
        projectionMatrix.m33 = 0;
    }
    
    public void cleanUp() {
        texturedShader.cleanUp();
        terrainShader.cleanUp();
        coloredShader.cleanUp();
        cinsShader.cleanUp();
        tinsShader.cleanUp();
    }
}
