package unknownnick.engine.render;

import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.buffer.WaterFrameBuffers;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.render.entity.WaterTile;
import unknownnick.engine.render.shader.WaterShader;
import unknownnick.engine.util.math.Maths;

public class WaterRenderer {
    public static final String DUDV_MAP = "waterDUDV";
    public static final String NORMAL_MAP = "waterNormal";
    public static final float WAVE_SPEED = 0.03f;
    public static final float WAVE_STRENGTH = 0.02f;
    public static final float SHINE_DAMPER = 10;
    public static final float REFLECTIVITY = 0.7f;
    private WaterShader shader;
    private VAO quad;
    private WaterFrameBuffers fbos;
    private int dudv;
    private int normalMap;
    private float moveFactor = 0;
    
    public WaterRenderer(Loader loader, WaterShader shader, Matrix4f projection, WaterFrameBuffers fbos){
        this.shader = shader;
        this.fbos = fbos;
        shader.start();
        shader.connectTextureUnits();
        shader.loadProjectionMatrix(projection);
        shader.stop();
        this.buildQuad(loader);
        dudv = loader.loadTexture(DUDV_MAP);
        normalMap = loader.loadTexture(NORMAL_MAP);
    }
    
    private void prepare(Camera camera, Light light){
        quad.bind();
        quad.enableAttribs();
        this.moveFactor += WAVE_SPEED * DisplayManager.getframeTimems()/1000;
        this.moveFactor %= 1f;
        shader.start();
        shader.loadViewMatrix(camera);
        shader.loadMoveFactor(moveFactor);
        shader.loadCameraPosition(camera);
        shader.loadLight(light);
        shader.loadFrustrum(MasterRenderer.NEAR_PLANE, MasterRenderer.FAR_PLANE);
        shader.loadShinyness(SHINE_DAMPER, REFLECTIVITY);
        shader.loadWaveStrength(WAVE_STRENGTH);
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getReflectionTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getRefractionTexture());
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, dudv);
        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, normalMap);
        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, fbos.getRefractionDepthTexture());
        
        GL11.glEnable(GL11.GL_BLEND);
        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
    }
    
    public void render(List<WaterTile> water, Camera camera, Light light) {
        prepare(camera, light);  
        for (WaterTile tile : water) {
            Matrix4f transformationMatrix = Maths.createtransformationMatrix(
                    new Vector3f(tile.getX(), tile.getHeight(), tile.getZ()), new Vector3f(0,0,0),
                    WaterTile.TILE_SIZE);
            shader.loadTransformationMatrix(transformationMatrix);
            shader.loadColour(tile.getColour());
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, quad.getVertexCount());
        }
        end();
    }
    
    private void end(){
        GL11.glDisable(GL11.GL_BLEND);
        quad.disableAttribs();
        VAO.unbind();
        shader.stop();
    }
    
    private void buildQuad(Loader loader){
        quad = loader.loadGUIVAO(new float[]{-1, -1, -1, 1,  1, -1, 1, -1, -1, 1, 1, 1});
    }
    
    public void cleanUp(){
        shader.cleanUp();
    }
}
