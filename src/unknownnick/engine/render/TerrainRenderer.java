package unknownnick.engine.render;

import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.render.shader.TerrainShader;
import unknownnick.engine.render.texture.ModelTexture;
import unknownnick.engine.render.texture.TerrainTexture;
import unknownnick.engine.render.texture.TerrainTextureGroup;
import unknownnick.engine.terrain.Terrain;
import unknownnick.engine.util.math.Maths;


public class TerrainRenderer {
    
    private TerrainShader shader;
    
    public TerrainRenderer(TerrainShader shader) {
        this.shader = shader;
        this.shader.connectTextureUnits();
    }
    
    public void render(List<Terrain> terrains) {
        for(Terrain terrain : terrains) {
            this.prepareTerrain(terrain);
            this.loadTransformationMatrix(terrain);
            
                GL11.glDrawElements(GL11.GL_TRIANGLES, terrain.getModel()
                        .getVertexCount(), GL11.GL_UNSIGNED_INT, 0);
            
            this.unbindTexturedModel(terrain.getModel());
        }
    }
    
    private void prepareTerrain(Terrain terrain) {
        VAO model = terrain.getModel();
        model.bind();
        model.enableAttribs();
        
        shader.loadShinyness(1, 0);
        this.bindTextures(terrain);
    }
    
    private void bindTextures(Terrain terrain){
        TerrainTextureGroup textures = terrain.getTextures();
        TerrainTexture blendmap = terrain.getBlendmap();
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures.getBackground().getTextureID());
        GL13.glActiveTexture(GL13.GL_TEXTURE1);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures.getR().getTextureID());
        GL13.glActiveTexture(GL13.GL_TEXTURE2);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures.getG().getTextureID());
        GL13.glActiveTexture(GL13.GL_TEXTURE3);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, textures.getB().getTextureID());
        GL13.glActiveTexture(GL13.GL_TEXTURE4);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, blendmap.getTextureID());        
    }
    
    private void unbindTexturedModel(VAO vao) {
        vao.disableAttribs();
        VAO.unbind();
    }
    
    private void loadTransformationMatrix(Terrain terrain) {
        Matrix4f transformMat = Maths.createtransformationMatrix(
            new Vector3f(terrain.getX(), 0, terrain.getZ()), new Vector3f(0, 0, 0), 1);
        shader.loadTransformationMatrix(transformMat);
    }
}
