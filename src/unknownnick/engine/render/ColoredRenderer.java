package unknownnick.engine.render;

import unknownnick.engine.render.entity.RenderableEntity;
import java.util.List;
import java.util.Map;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.render.shader.ColoredShader;

public class ColoredRenderer {
    
    private ColoredShader shader;

    public ColoredRenderer(ColoredShader shader) {
        this.shader = shader;
    }
    
    public void render(Map<ColoredModel, List<RenderableEntity>> entities) {
        for(ColoredModel model : entities.keySet()) {
            prepareColoredModel(model);
            List<RenderableEntity> batch = entities.get(model);
            for(RenderableEntity entity : batch) {
                prepareInstance(entity);
                
                GL11.glDrawElements(
                        GL11.GL_TRIANGLES,
                        entity.getCmodel().getModel().getVertexCount(),
                        GL11.GL_UNSIGNED_INT,
                        0);
            }
            unbindModel(model.getModel());
        }
    }
    
    private void prepareColoredModel(ColoredModel model){
        VAO vao = model.getModel();
        vao.bind();
        vao.enableAttribs();
        
        shader.loadColour(model.getColor());
        shader.loadUseFakeLight(model.isUseFakeLight());
        shader.loadShinyness(model.getShineDamper(), model.getReflectivity());
    }
    
    private void unbindModel(VAO vao) {
        MasterRenderer.enableCulling();
        vao.disableAttribs();
        VAO.unbind();
    }
    
    private void prepareInstance(RenderableEntity entity) {
        Matrix4f transformMat = entity.getTransformationMatrix();
        shader.loadTransformationMatrix(transformMat);
    }
    
    
}
