package unknownnick.engine.render;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.buffer.VBO;
import unknownnick.engine.render.entity.GroupTexturedEntity;
import unknownnick.engine.render.entity.GroupTexturedEntity.Attributes;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.render.shader.InstancedTexturedShader;
import unknownnick.engine.render.texture.ModelTexture;
import unknownnick.engine.util.math.Maths;

public class InstancedTexturedRenderer {
    private InstancedTexturedShader shader;
    private FloatBuffer buff;
    float[] data;
    private VBO vbo;
    private int maxCount;
    private Loader loader;
    public static final int INSTANCE_DATA_LENGTH = 20;
    private List<VBO> oldVBOs = new ArrayList<>();
    
    private int pointer;

    public InstancedTexturedRenderer(InstancedTexturedShader shader, Loader loader) {
        this.shader = shader;
        this.loader = loader;
        this.rebuildBuffers(100);
    }
    
    public void render(Map<TexturedModel, List<GroupTexturedEntity>> entities) {
        for(TexturedModel model : entities.keySet()) {
            prepareTexturedModel(model);
            VAO vao = model.getModel();
            List<GroupTexturedEntity> batch = entities.get(model);
            pointer = 0;
            for(GroupTexturedEntity entity : batch) {
                List<Attributes> instances = entity.get();
                if(instances.size() > maxCount){
                    this.rebuildBuffers(instances.size());
                }
                data = new float[instances.size() * INSTANCE_DATA_LENGTH];
                for(Attributes atts : instances){
                    this.storeMatrix(Maths.createtransformationMatrix(atts.getTransform().getPosition(),
                            atts.getTransform().getRotation(), atts.getTransform().getScale()));
                    this.storeVector2(entity.getOffset(atts.getTextureIndex()));
                    this.storeFloat(atts.getShineDamper());
                    this.storeFloat(atts.getReflectivity());
                }
                vbo.update(data, buff);
                vao.addInstancedAttribute(vbo, 3, 4, INSTANCE_DATA_LENGTH, 0);
                vao.addInstancedAttribute(vbo, 4, 4, INSTANCE_DATA_LENGTH, 4);
                vao.addInstancedAttribute(vbo, 5, 4, INSTANCE_DATA_LENGTH, 8);
                vao.addInstancedAttribute(vbo, 6, 4, INSTANCE_DATA_LENGTH, 12);
                vao.addInstancedAttribute(vbo, 7, 2, INSTANCE_DATA_LENGTH, 16);
                vao.addInstancedAttribute(vbo, 8, 1, INSTANCE_DATA_LENGTH, 18);
                vao.addInstancedAttribute(vbo, 9, 1, INSTANCE_DATA_LENGTH, 19);
                vao.bind();
                
                GL31.glDrawElementsInstanced(
                        GL11.GL_TRIANGLES,
                        vao.getVertexCount(),
                        GL11.GL_UNSIGNED_INT,
                        0,
                        instances.size());
            }
            unbindModel(vao);
        }
        for(VBO v : oldVBOs){
            if(v != null)
                loader.free(v);
        }
        oldVBOs.clear();
    }
    
    private void storeFloat(float f){
        data[pointer++] = f;
    }
    
    private void storeVector2(Vector2f v){
        data[pointer++] = v.x;
        data[pointer++] = v.y;
    }
    
    private void storeMatrix(Matrix4f m){
        data[pointer++] = m.m00;
        data[pointer++] = m.m01;
        data[pointer++] = m.m02;
        data[pointer++] = m.m03;
        data[pointer++] = m.m10;
        data[pointer++] = m.m11;
        data[pointer++] = m.m12;
        data[pointer++] = m.m13;
        data[pointer++] = m.m20;
        data[pointer++] = m.m21;
        data[pointer++] = m.m22;
        data[pointer++] = m.m23;
        data[pointer++] = m.m30;
        data[pointer++] = m.m31;
        data[pointer++] = m.m32;
        data[pointer++] = m.m33;
    }
    
    private void rebuildBuffers(int newCount){
        oldVBOs.add(vbo);
        maxCount = newCount;
        buff = BufferUtils.createFloatBuffer(newCount * INSTANCE_DATA_LENGTH);
        vbo = loader.getEmptyVBO(newCount * INSTANCE_DATA_LENGTH);
    }
    
    private void prepareTexturedModel(TexturedModel model){
        GL30.glBindVertexArray(model.getModel().getID());
        model.getModel().enableAttribs();
        
        ModelTexture texture = model.getTexture();
        shader.loadRows(texture.getNumberOfRows());
        if(texture.isHasTransparency()){
            MasterRenderer.disableCulling();
        }
        shader.loadUseFakeLight(texture.isUseFakeLight());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTexture().getID());
    }
    
    private void unbindModel(VAO vao) {
        MasterRenderer.enableCulling();
        vao.disableAttribs();
        VAO.unbind();
    }
}
