package unknownnick.engine.render.buffer;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.List;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;

public class IndexBuffer {
    private final int[] indices;
    private final int iboID;
    private boolean alive = true;
    
    public IndexBuffer(int[] data, List<Integer> register){
        this.indices = data;
        this.iboID = GL15.glGenBuffers();
        register.add(iboID);
    }
    
    public int getId(){
        return iboID;
    }
    
    public void destroy(){
        GL15.glDeleteBuffers(iboID);
        alive = false;
    }
    
    public void bind(){
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, iboID);
    }
    
    public static void unbind(){
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);        
    }

    public int[] getData() {
        return indices;
    }

    public int getVboID() {
        return iboID;
    }
        
    public IntBuffer asIntBuffer(){
        IntBuffer buff = BufferUtils.createIntBuffer(indices.length);
        buff.put(indices);
        buff.flip();
        return buff;
    }
    
    
}
