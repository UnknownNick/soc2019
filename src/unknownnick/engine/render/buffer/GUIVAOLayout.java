package unknownnick.engine.render.buffer;

import java.util.List;

public class GUIVAOLayout extends VAOLayout {
    private final float[] positions;
    private final List<Integer> vboReg;
    public GUIVAOLayout(List<Integer> vaoReg, List<Integer> vboReg, float[] positions){
        super(new VAO(positions.length/2, vaoReg));
        this.positions = positions;
        this.vboReg = vboReg;
    }

    @Override
    public VAO load() {
        vao.bind();
        vao.attachVBO(0, new VBO(positions, 2, vboReg));
        VAO.unbind();
        return vao;        
    }
}
