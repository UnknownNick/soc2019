package unknownnick.engine.render.buffer;

public abstract class VAOLayout {
    protected final VAO vao;
    
    public VAOLayout(VAO vao){
        this.vao = vao;
    }
    
    public abstract VAO load();
}
