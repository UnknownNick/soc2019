package unknownnick.engine.render.buffer;

import java.util.List;

public class PlainModelVAOLayout extends VAOLayout {
    private final float[] positions;
    private final float[] normals;
    private final int[] indices;
    private final List<Integer> vboReg;
    public PlainModelVAOLayout(List<Integer> vaoReg, List<Integer> vboReg,
            float[] positions, float[] normals, int[] indices){
        super(new VAO(indices.length, vaoReg));
        this.positions = positions;
        this.normals = normals;
        this.indices = indices;
        this.vboReg = vboReg;
    }    

    @Override
    public VAO load() {
        vao.bind();
        vao.attachIndexBuffer(new IndexBuffer(indices, vboReg));
        vao.attachVBO(0, new VBO(positions, 3, vboReg));
        vao.attachVBO(1, new VBO(normals, 3, vboReg));
        VAO.unbind();
        return vao;    
    }
}
