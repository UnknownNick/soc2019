package unknownnick.engine.render.buffer;

import java.util.List;
import unknownnick.engine.util.math.Prints;

public class GUITextVAOLayout extends VAOLayout {
    private final float[] positions;
    private final float[] texCoords;
    private final List<Integer> vboReg;

    public GUITextVAOLayout(List<Integer> vaoReg, List<Integer> vboReg, float[] positions,
            float[] texCoords) {
        super(new VAO(positions.length/2, vaoReg));
        this.positions = positions;
        this.texCoords = texCoords;
        this.vboReg = vboReg;
    }
    

    @Override
    public VAO load() {
        vao.bind();
        vao.attachVBO(0, new VBO(positions, 2, vboReg));
        vao.attachVBO(1, new VBO(texCoords, 2, vboReg));
        VAO.unbind();
        return vao;   
        
    }
    
}
