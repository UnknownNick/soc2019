package unknownnick.engine.render.buffer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import unknownnick.engine.gui.font.FontAttributes;
import unknownnick.engine.gui.font.FontType;



public class Loader {
    private final List<Integer> vaos = new ArrayList<>();
    private final List<Integer> vbos = new ArrayList<>();
    private final List<Integer> textures = new ArrayList<>();
    
    public VAO loadTexturedModelVAO(float[] positions, float[] texcoords, float[] normals, int[] indices) {
        TexturedModelVAOLayout vl = new TexturedModelVAOLayout(vaos, vbos,
                positions, texcoords, normals, indices);
        return vl.load();
    }
    
    public VAO loadPlainModelVAO(float[] positions, float[] normals, int[] indices){
        PlainModelVAOLayout vl = new PlainModelVAOLayout(vaos, vbos,
                positions, normals, indices);
        return vl.load();
    }
    
    public VAO loadGUITextVAO(float[] positions, float[] texcoords){
        GUITextVAOLayout vl = new GUITextVAOLayout(vaos, vbos, positions, texcoords);
        return vl.load();
    }
    
    public VAO loadGUIVAO(float[] positions){
        GUIVAOLayout vl = new GUIVAOLayout(vaos, vbos, positions);
        return vl.load();
    }
    
    public VAO loadQuadVAO(float[] positions){
        QuadVAOLayout vl = new QuadVAOLayout(vaos, vbos, positions);
        return vl.load();
    }
    
    public VBO getEmptyVBO(int count){
        return VBO.emptyVBO(count, vbos);
    }
    
    public FontType loadFont(String font, FontAttributes attribs){
        String name = font+"/";
        if(attribs.sdf){
            name += "sdf_";
            if(attribs.bold){
                name += "bold";
            }
            if(attribs.italic){
                name += "italic";
            }
            if(!attribs.bold && !attribs.italic){
                name += "normal";
            }
        } else {
            name += "font";
        }
        
        InputStream fnt = Class.class.getResourceAsStream("/assets/font/"+name+".fnt");
        return new FontType(this.loadTexture("font/"+name, 0), fnt, attribs);
    }
    
    public int loadTexture(String filename){
        return loadTexture(filename, -0.4f);
    }
    
    public int loadTexture(String filename, float mipmapBias) {
        Texture texture;
        try {
                texture = TextureLoader.getTexture("PNG", Class.class.getResourceAsStream(
                        "/assets/" + filename + ".png"));
                GL30.glGenerateMipmap(GL11.GL_TEXTURE_2D);
                GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR_MIPMAP_LINEAR);
                GL11.glTexParameterf(GL11.GL_TEXTURE_2D, GL14.GL_TEXTURE_LOD_BIAS, mipmapBias);
                int textureID = texture.getTextureID();
                textures.add(textureID);
                return textureID;
            } catch (IOException ex) {
                ex.printStackTrace();
        }
        return -1;
    }
    
    public void free(VBO vbo){
        int id = vbo.getID();
        if(vbos.contains(id)){
            vbo.destroy();
            vbos.remove((Integer)id);
        }
    }
    
    public void free(VAO vao){
        int vaoId = vao.getID();
        if(vaos.contains(vaoId)){
            for(Integer id : vao.getBufferIds()){
                if(vbos.contains(id)){
                    vbos.remove(id);
                }
            }
            vao.destroyBuffers();
            vao.destroy();
            vaos.remove((Integer) vaoId);
        }
    }
    
    public void cleanUp() {
        for(int vaoID : vaos)
            GL30.glDeleteVertexArrays(vaoID);
        for(int vboID : vbos)
            GL15.glDeleteBuffers(vboID);
        for(int texture : textures)
            GL11.glDeleteTextures(texture);
    }
}
