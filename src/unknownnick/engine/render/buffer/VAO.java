package unknownnick.engine.render.buffer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL33;

public class VAO {
    private final int vaoID;
    private final int vertexCount;
    private IndexBuffer indices;
    private final Map<Integer, VBO> vbos = new TreeMap();
    private final List<Integer> attribs = new ArrayList<>();
    
    public VAO(int vertexCount, List<Integer> register) {
        this.vertexCount = vertexCount;
        this.vaoID = GL30.glGenVertexArrays();
        register.add(vaoID);
    }
    
    public void addInstancedAttribute(VBO vbo, int attribute, int size, int length, int offset){
        attribs.add(attribute);
        vbo.bind();
        this.bind();
        GL20.glVertexAttribPointer(attribute, size, GL11.GL_FLOAT, false, length * Float.BYTES, offset * Float.BYTES);
        GL33.glVertexAttribDivisor(attribute, 1);
        VBO.unbind();
        VAO.unbind();
    }   
    
    public void attachVBO(int attribute, VBO vbo){
        vbos.put(attribute, vbo);
        attribs.add(attribute);
        vbo.bind();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vbo.asFloatBuffer(), GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attribute, vbo.getCoordSize(), GL11.GL_FLOAT, false, 0, 0);
        VBO.unbind();
    }
    
    public void attachIndexBuffer(IndexBuffer buff){
        this.indices = buff;
        indices.bind();
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, indices.asIntBuffer(), GL15.GL_STATIC_DRAW);
    }
    
    public List<Integer> getBufferIds(){
        List<Integer> ret = new ArrayList<>();
        for(VBO vbo : vbos.values()){
            ret.add(vbo.getID());
        }
        if(indices != null){
            ret.add(indices.getId());
        }
        return ret;
    }
    
    public void destroyBuffers(){
        for(VBO vbo : vbos.values()){
            vbo.destroy();
        }
        if(indices != null){
            indices.destroy();
        }
    }
    
    public void destroy(){
        GL30.glDeleteVertexArrays(vaoID);
    }
    
    public void enableAttribs(){
        for(Integer a : attribs){
            GL20.glEnableVertexAttribArray(a);
        }
    }
    
    public void disableAttribs(){
        for(Integer a : attribs){
            GL20.glDisableVertexAttribArray(a);
        }
    }
    
    public void bind(){
        GL30.glBindVertexArray(vaoID);
    }
    
    public int getID() {
        return this.vaoID;
    }
    
    public int getVertexCount() {
        return this.vertexCount;
    }

    public static void unbind() {
        GL30.glBindVertexArray(0);        
    }
}
