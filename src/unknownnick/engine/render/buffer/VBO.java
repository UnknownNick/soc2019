package unknownnick.engine.render.buffer;

import java.nio.FloatBuffer;
import java.util.List;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;

public class VBO {
    private final float[] data;
    private final int vboID;
    private final int coordSize;
    private boolean alive = true;
    
    public VBO(float[] data, int coordSize, List<Integer> register){
        this.data = data;
        this.coordSize = coordSize;
        this.vboID = GL15.glGenBuffers();
        register.add(vboID);
        //System.out.println("created vbo size " + data.length + "/" + coordSize);
    }
    
    private VBO(List<Integer> register){
        data = null;
        this.vboID = GL15.glGenBuffers();
        register.add(vboID);
        coordSize = -1;
    }
    
    public static VBO emptyVBO(int count, List<Integer> register){
        VBO empty = new VBO(register);
        empty.bind();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, count * Float.BYTES, GL15.GL_STREAM_DRAW);
        VBO.unbind();
        return empty;
    }
    
    public void destroy(){
        GL15.glDeleteBuffers(vboID);
        alive = false;
    }
    
    public void update(float[] dat, FloatBuffer buff){
        buff.clear();
        buff.put(dat);
        buff.flip();
        this.bind();
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buff.capacity() * Float.BYTES, GL15.GL_STREAM_DRAW);
        GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, buff);
        VBO.unbind();
    }
    
    public int getCount(){
        return (data.length/coordSize);
    }
    
    public float[] getVertex(int index){
        float[] ret = new float[coordSize];
        for(int i = 0; i < coordSize;i++){
            ret[i] = data[index*coordSize+i];
        }
        return ret;
    }
    
    public void bind(){
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vboID);        
    }
    
    public static void unbind(){
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);        
    }

    public float[] getData() {
        return data;
    }

    public boolean isAlive() {
        return alive;
    }

    public int getID() {
        return vboID;
    }

    public int getCoordSize() {
        return coordSize;
    }
        
    public FloatBuffer asFloatBuffer(){
        FloatBuffer buff = BufferUtils.createFloatBuffer(data.length);
        buff.put(data);
        buff.flip();
        return buff;        
    }
    
    
}
