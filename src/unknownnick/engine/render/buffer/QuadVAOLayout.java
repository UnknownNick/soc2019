package unknownnick.engine.render.buffer;

import java.util.List;

public class QuadVAOLayout extends VAOLayout {
    private final float[] positions;
    private final List<Integer> vboReg;
    public QuadVAOLayout(List<Integer> vaoReg, List<Integer> vboReg, float[] positions){
        super(new VAO(positions.length/3, vaoReg));
        this.positions = positions;
        this.vboReg = vboReg;
    }

    @Override
    public VAO load() {
        vao.bind();
        vao.attachVBO(0, new VBO(positions, 3, vboReg));
        VAO.unbind();
        return vao;        
    }
    
}
