package unknownnick.engine.render;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.util.vector.Matrix4f;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.buffer.VBO;
import unknownnick.engine.render.entity.GroupColoredEntity;
import unknownnick.engine.render.entity.RenderableEntity;
import unknownnick.engine.render.entity.TransformationAttributes;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.render.shader.InstancedColoredShader;
import unknownnick.engine.util.math.Maths;

public class InstancedColoredRenderer {
    
    private InstancedColoredShader shader;
    private FloatBuffer buff;
    float[] data;
    private VBO vbo;
    private int maxCount;
    private Loader loader;
    public static final int INSTANCE_DATA_LENGTH = 16;
    private List<VBO> oldVBOs = new ArrayList<>();
    
    private int pointer;

    public InstancedColoredRenderer(InstancedColoredShader shader, Loader loader) {
        this.shader = shader;
        this.loader = loader;
        this.rebuildBuffers(100);
    }
    
    public void render(Map<ColoredModel, List<GroupColoredEntity>> entities) {
        for(ColoredModel model : entities.keySet()) {
            prepareColoredModel(model);
            VAO vao = model.getModel();
            List<GroupColoredEntity> batch = entities.get(model);
            pointer = 0;
            for(GroupColoredEntity entity : batch) {
                List<TransformationAttributes> instances = entity.get();
                if(instances.size() > maxCount){
                    this.rebuildBuffers(instances.size());
                }
                data = new float[instances.size() * INSTANCE_DATA_LENGTH];
                for(TransformationAttributes atts : instances){
                    this.storeMatrix(Maths.createtransformationMatrix(atts.getPosition(), atts.getRotation(), atts.getScale()));
                }
                vbo.update(data, buff);
                vao.addInstancedAttribute(vbo, 2, 4, INSTANCE_DATA_LENGTH, 0);
                vao.addInstancedAttribute(vbo, 3, 4, INSTANCE_DATA_LENGTH, 4);
                vao.addInstancedAttribute(vbo, 4, 4, INSTANCE_DATA_LENGTH, 8);
                vao.addInstancedAttribute(vbo, 5, 4, INSTANCE_DATA_LENGTH, 12);
                vao.bind();
                
                GL31.glDrawElementsInstanced(
                        GL11.GL_TRIANGLES,
                        vao.getVertexCount(),
                        GL11.GL_UNSIGNED_INT,
                        0,
                        instances.size());
            }
            unbindModel(vao);
        }
        for(VBO v : oldVBOs){
            if(v != null)
                loader.free(v);
        }
        oldVBOs.clear();
    }
    
    private void storeMatrix(Matrix4f m){
        data[pointer++] = m.m00;
        data[pointer++] = m.m01;
        data[pointer++] = m.m02;
        data[pointer++] = m.m03;
        data[pointer++] = m.m10;
        data[pointer++] = m.m11;
        data[pointer++] = m.m12;
        data[pointer++] = m.m13;
        data[pointer++] = m.m20;
        data[pointer++] = m.m21;
        data[pointer++] = m.m22;
        data[pointer++] = m.m23;
        data[pointer++] = m.m30;
        data[pointer++] = m.m31;
        data[pointer++] = m.m32;
        data[pointer++] = m.m33;
    }
    
    private void rebuildBuffers(int newCount){
        oldVBOs.add(vbo);
        maxCount = newCount;
        buff = BufferUtils.createFloatBuffer(newCount * INSTANCE_DATA_LENGTH);
        vbo = loader.getEmptyVBO(newCount * INSTANCE_DATA_LENGTH);
    }
    
    private void prepareColoredModel(ColoredModel model){
        GL30.glBindVertexArray(model.getModel().getID());
        model.getModel().enableAttribs();
        
        shader.loadColour(model.getColor());
        shader.loadUseFakeLight(model.isUseFakeLight());
        shader.loadShinyness(model.getShineDamper(), model.getReflectivity());
    }
    
    private void unbindModel(VAO vao) {
        MasterRenderer.enableCulling();
        vao.disableAttribs();
        VAO.unbind();
    }
    
}
