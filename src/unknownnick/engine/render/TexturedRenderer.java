package unknownnick.engine.render;

import unknownnick.engine.render.entity.RenderableEntity;
import java.util.List;
import java.util.Map;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.render.shader.TexturedShader;
import unknownnick.engine.render.texture.ModelTexture;
import unknownnick.engine.util.math.Maths;



public class TexturedRenderer {
    
    private TexturedShader shader;
    
    public TexturedRenderer(TexturedShader shader) {
        this.shader = shader;
    }
    
    public void render(Map<TexturedModel, List<RenderableEntity>> entities) {
        for(TexturedModel model : entities.keySet()) {
            prepareTexturedModel(model);
            List<RenderableEntity> batch = entities.get(model);
            for(RenderableEntity entity : batch) {
                prepareInstance(entity);
                
                GL11.glDrawElements(
                        GL11.GL_TRIANGLES,
                        entity.getTModel().getModel().getVertexCount(),
                        GL11.GL_UNSIGNED_INT,
                        0);
            }
            unbindModel(model.getModel());
        }
    }
    
    private void prepareTexturedModel(TexturedModel model) {
        GL30.glBindVertexArray(model.getModel().getID());
        model.getModel().enableAttribs();
        
        ModelTexture texture = model.getTexture();
        shader.loadRows(texture.getNumberOfRows());
        if(texture.isHasTransparency()){
            MasterRenderer.disableCulling();
        }
        shader.loadUseFakeLight(texture.isUseFakeLight());
        shader.loadShinyness(texture.getShineDamper(), texture.getReflectivity());
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL11.GL_TEXTURE_2D, model.getTexture().getID());
    }
    
    private void unbindModel(VAO vao) {
        MasterRenderer.enableCulling();
        vao.disableAttribs();
        VAO.unbind();
    }
    
    private void prepareInstance(RenderableEntity entity) {
        Matrix4f transformMat = Maths.createtransformationMatrix(
                entity.getPosition(), entity.getRotation(), entity.getScale());
        shader.loadTransformationMatrix(transformMat);
        shader.loadOffset(entity.getOffset());
    }
}
