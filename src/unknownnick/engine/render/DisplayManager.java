package unknownnick.engine.render;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.lang.Math.max;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.AWTGLCanvas;
import org.lwjgl.opengl.ContextAttribs;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.PixelFormat;

public class DisplayManager {
    private static final int WIDTH = 1280;
    private static final int HEIGHT = 720;
    private static final int TARGET_FPS = 60;
    
    private static long lastFrameTime;
    private static float delta;
    private static AWTGLCanvas pane;
    private static Frame frame;
    private static Panel leftPane;
    
    private static boolean resizeRequested = false;
    private static boolean close = false;
    private static Mode mode;
    private static String title = "renderer";
    private enum Mode {
        AWT, GL
    }
    
    public static boolean getCloseRequested(){
        return close;
    }
    
    public static void createAWTDisplay(){
        try {
            mode = Mode.AWT;
            ContextAttribs attribs = new ContextAttribs(3, 3)
                    .withForwardCompatible(true)
                    .withProfileCore(true);
            frame = new Frame(title);
            frame.setSize(WIDTH+100+frame.getInsets().left, HEIGHT+frame.getInsets().top);
            frame.setLayout(new BorderLayout());
            frame.setMinimumSize(new Dimension(120, 120));
            pane = new AWTGLCanvas();
            frame.add(pane,BorderLayout.CENTER);
            frame.addWindowListener(new WindowAdapter(){
                @Override
                public void windowClosing(WindowEvent e){
                    close = true;
                }
            });
            leftPane  = new Panel();
            leftPane.setLayout(new BorderLayout());
            leftPane.setPreferredSize(new Dimension(100, HEIGHT));
            Button button = new Button("meh");
            button.addActionListener(new ActionListener(){
                @Override
                public void actionPerformed(ActionEvent e) {
                    System.out.println("pressed");
                }
            });
            Panel pa = new Panel();
            pa.add(button);
            setSideGui(pa);
            frame.add(leftPane,BorderLayout.WEST);
            frame.setVisible(true);
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.setParent(pane);
            Display.setVSyncEnabled(true);
            Display.create(new PixelFormat().withDepthBits(24), attribs);
        } catch (LWJGLException ex) {
            ex.printStackTrace();
        }
        GL11.glViewport(0, 0, pane.getWidth(), pane.getHeight());
            frame.addComponentListener(new ComponentAdapter(){
                @Override
                public void componentResized(ComponentEvent e){
                    resizeRequested = true;
                    Component[] c = leftPane.getComponents();                    
                    for(Component comp : c){
                        if(comp instanceof Panel){
                            comp.setSize(leftPane.getSize());
                            break;
                        }
                    }
                }
            });
        lastFrameTime = getCurrentTime();
    }
    
    public static void checkResize(MasterRenderer renderer){
        if(resizeRequested){
            resizeViewport();
            renderer.reloadProjectionmatrix();
        }
    }

    public static String getTitle() {
        return title;
    }

    public static void setTitle(String title) {
        DisplayManager.title = title;
    }

    public static Frame getFrame() {
        return frame;
    }
    
    public static void resizeViewport(){
        resizeRequested = false;
        int width = pane.getWidth();
        int height = pane.getHeight();
        GL11.glViewport(0, 0, width, height);
    }
    
    public static void setSideGui(Panel p){
        leftPane.removeAll();
        leftPane.add(p);
        Dimension s = leftPane.getSize();
        p.setSize(s);
    }
    
    public static void createDisplay() {
        mode = Mode.GL;
        ContextAttribs attribs = new ContextAttribs(3, 2)
                .withForwardCompatible(true)
                .withProfileCore(true);
        
        try {
            Display.setDisplayMode(new DisplayMode(WIDTH, HEIGHT));
            Display.create(new PixelFormat(), attribs);
            Display.setTitle("renderer");
        } catch (LWJGLException ex) {
            ex.printStackTrace();
        }
        
        GL11.glViewport(0, 0, WIDTH, HEIGHT);
        lastFrameTime = getCurrentTime();
    }
    
    public static void updateDisplay() {
        Display.sync(TARGET_FPS);
        Display.update();
        long currentFrameTime = getCurrentTime();
        delta = currentFrameTime - lastFrameTime;
        lastFrameTime = currentFrameTime;
    }
    
    public static float getframeTimems(){
        return delta;
    }
    
    public static void closeDisplay() {
        if(mode == Mode.GL)
            Display.destroy();
        if(mode == Mode.AWT){
            frame.setVisible(false);
            Display.destroy();
            frame.dispose();
            System.exit(0);
        }
    }
    
    private static long getCurrentTime(){
        return Sys.getTime()*1000/Sys.getTimerResolution();
    }
        
}
