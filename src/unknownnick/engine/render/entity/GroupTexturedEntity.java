package unknownnick.engine.render.entity;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.model.TexturedModel;

public class GroupTexturedEntity {
    private TexturedModel model;
    private List<Attributes> entities = new ArrayList<>();

    public GroupTexturedEntity(TexturedModel model) {
        this.model = model;
    }

    public TexturedModel getModel() {
        return model;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }
    
    public Vector2f getOffset(int textureIndex){
        int rows = model.getTexture().getNumberOfRows();
        int xpos = textureIndex % rows;
        float xoffset = (float)xpos / (float)rows;
        int ypos = textureIndex / rows;
        float yoffset = (float)ypos / (float)rows;
        return new Vector2f(xoffset, yoffset);
    }

    public List<Attributes> get() {
        return entities;
    }
    
    public void add(Vector3f position, Vector3f rotation, float scale, int textureIndex,
                    float shineDamper, float reflectivity){
        this.add(new Attributes(new TransformationAttributes(position, rotation, scale),
                textureIndex, shineDamper, reflectivity));
    }
    
    public void add(TransformationAttributes transform, int textureIndex, float shineDamper, float reflectivity){
        this.add(new Attributes(transform, textureIndex, shineDamper, reflectivity));
    }
    
    public void addAll(List<Attributes> newer){
        entities.addAll(newer);
    }
    
    public void removeAll(List<Attributes> newer){
        entities.removeAll(newer);
    }
    
    public void add(Attributes atts){
        entities.add(atts);
    }
    
    public void remove(Attributes atts){
        entities.remove(atts);
    }
    
    public static class Attributes {
        private TransformationAttributes transform;
        private int textureIndex;
        private float shineDamper;
        private float reflectivity;

        public Attributes(TransformationAttributes transform, int textureIndex, float shineDamper, float reflectivity) {
            this.transform = transform;
            this.textureIndex = textureIndex;
            this.shineDamper = shineDamper;
            this.reflectivity = reflectivity;
        }

        public TransformationAttributes getTransform() {
            return transform;
        }

        public void setTransform(TransformationAttributes transform) {
            this.transform = transform;
        }

        public int getTextureIndex() {
            return textureIndex;
        }

        public void setTextureIndex(int textureIndex) {
            this.textureIndex = textureIndex;
        }

        public float getShineDamper() {
            return shineDamper;
        }

        public void setShineDamper(float shineDamper) {
            this.shineDamper = shineDamper;
        }

        public float getReflectivity() {
            return reflectivity;
        }

        public void setReflectivity(float reflectivity) {
            this.reflectivity = reflectivity;
        }
    }
}
