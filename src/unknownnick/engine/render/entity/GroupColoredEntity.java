package unknownnick.engine.render.entity;

import java.util.ArrayList;
import java.util.List;
import unknownnick.engine.render.model.ColoredModel;

public class GroupColoredEntity {
    
    private ColoredModel cmodel;
    
    private List<TransformationAttributes> attribs = new ArrayList<>();

    public GroupColoredEntity(ColoredModel cmodel) {
        this.cmodel = cmodel;
    }

    public ColoredModel getCmodel() {
        return cmodel;
    }

    public void setCmodel(ColoredModel cmodel) {
        this.cmodel = cmodel;
    }
    
    public List<TransformationAttributes> get(){
        return attribs;
    }
    
    public void addAll(List<TransformationAttributes> newer){
        attribs.addAll(newer);
    }
    
    public void removeAll(List<TransformationAttributes> newer){
        attribs.removeAll(newer);
    }
    
    public void add(TransformationAttributes atts){
        attribs.add(atts);
    }
    
    public void remove(TransformationAttributes atts){
        attribs.remove(atts);
    }
    
}
