package unknownnick.engine.render.entity;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.util.math.Maths;


public class Camera {
    private Vector3f position = new Vector3f(0, 0, 0);
    private float pitch;//rotates XZ
    private float yaw;//rotates YZ
    private float roll;//rotates XY
    
    public Camera() {
        
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }
    
    public void invertPitch(){
        pitch = -pitch;
    }
    
    public void move(float x, float y, float z) {
        position.translate(x, y, z);
    }

    public Vector3f getPosition() {
        return position;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public float getRoll() {
        return roll;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public void setRoll(float roll) {
        this.roll = roll;
    }
    
    
}
