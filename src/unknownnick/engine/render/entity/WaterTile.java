package unknownnick.engine.render.entity;

import org.lwjgl.util.vector.Vector3f;

public class WaterTile {
    public static final float TILE_SIZE = 60;
    private Vector3f colour = new Vector3f(0, 0.3f, 0.9f);
     
    private float height;
    private float x,z;
     
    public WaterTile(float centerX, float centerZ, float height){
        this.x = centerX;
        this.z = centerZ;
        this.height = height;
    }

    public Vector3f getColour() {
        return colour;
    }

    public void setColour(Vector3f colour) {
        this.colour = colour;
    }
 
    public float getHeight() {
        return height;
    }
 
    public float getX() {
        return x;
    }
 
    public float getZ() {
        return z;
    }
}
