package unknownnick.engine.render.entity;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.render.model.ModelType;
import unknownnick.engine.render.model.TexturedModel;
import unknownnick.engine.util.math.Maths;


public class RenderableEntity {
    
    private final ModelType modelType;
    private TexturedModel tmodel;
    private ColoredModel cmodel;
    private TransformationAttributes atts;
    private int textureIndex = 0;
    private TransformationMode mode = TransformationMode.LOCAL;
    private Matrix4f remoteMatrix;
    public enum TransformationMode{
        REMOTE, LOCAL, BOTH
    }
    
    public RenderableEntity(TexturedModel tmodel, Vector3f position, Vector3f rotation, float scale){
        this.modelType = ModelType.TEXTURE;
        this.tmodel = tmodel;
        this.atts = new TransformationAttributes(position, rotation, scale);
    }
    
    public RenderableEntity(TexturedModel tmodel, Vector3f position, Vector3f rotation, float scale, int index){
        this.modelType = ModelType.TEXTURE;
        this.tmodel = tmodel;
        this.atts = new TransformationAttributes(position, rotation, scale);
        this.textureIndex = index;
    }
    
    public RenderableEntity(ColoredModel cmodel, Vector3f position, Vector3f rotation, float scale){
        this.modelType = ModelType.COLOR;
        this.cmodel = cmodel;
        this.atts = new TransformationAttributes(position, rotation, scale);
    }      
    
    public Vector2f getOffset(){
        int rows = tmodel.getTexture().getNumberOfRows();
        int xpos = textureIndex % rows;
        float xoffset = (float)xpos / (float)rows;
        int ypos = textureIndex / rows;
        float yoffset = (float)ypos / (float)rows;
        return new Vector2f(xoffset, yoffset);
    }
    
    public void rotate(Vector3f rot) {
        atts.getRotation().x += rot.x;
        atts.getRotation().y += rot.y;
        atts.getRotation().z += rot.z;
    }
    
    public void move(Vector3f add) {
        atts.getPosition().x += add.x;
        atts.getPosition().y += add.y;
        atts.getPosition().z += add.z;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public ColoredModel getCmodel() {
        return cmodel;
    }

    public void setCmodel(ColoredModel cmodel) {
        this.cmodel = cmodel;
    }
    
    public TexturedModel getTModel() {
        return tmodel;
    }

    public void setTModel(TexturedModel model) {
        this.tmodel = model;
    }

    public Vector3f getPosition() {
        return atts.getPosition();
    }

    public void setPosition(Vector3f position) {
        this.atts.setPosition(position);
    }

    public Vector3f getRotation() {
        return atts.getRotation();
    }

    public void setRotation(Vector3f rotation) {
        this.atts.setRotation(rotation);
    }

    public float getScale() {
        return atts.getScale();
    }

    public void setScale(float scale) {
        this.atts.setScale(scale);
    }

    public TransformationMode getMode() {
        return mode;
    }

    public void setMode(TransformationMode mode) {
        this.mode = mode;
    }
        
    public Matrix4f getTransformationMatrix(){
        switch(mode){
            case LOCAL:
                return Maths.createtransformationMatrix(atts.getPosition(), atts.getRotation(), atts.getScale());
            case REMOTE:
                return remoteMatrix;
            case BOTH:
                return Matrix4f.mul(remoteMatrix, Maths.createtransformationMatrix(atts.getPosition(), atts.getRotation(), atts.getScale()), null);
            default:
                return null;
        }
    }
}
