package unknownnick.engine.render.entity;

import unknownnick.engine.render.entity.Camera;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.DisplayManager;
import unknownnick.engine.render.model.TexturedModel;
import static java.lang.Math.max;
import static java.lang.Math.min;

public class Spectator extends RenderableEntity {
    
    private static final float FLY_SPEED = 0.015f, FLOAT_SPEED = 0.015f, BOOST_RATIO = 5f;
    private static final float TURN_SPEED = 0.080f;
    protected Camera camera;
    protected Vector3f diff, rotdiff;
    private float cameraDistance = 0;
    private float cameraAngle = 0;
    
    
    public Spectator(TexturedModel model, Vector3f position, Vector3f rotation, float scale, Camera camera) {
        super(model, position, rotation, scale);
        this.camera = camera;
        diff = new Vector3f();
        rotdiff = new Vector3f();
    }
    
    public void move(){
        checkInputs();
        cameraDistance = max(0,min(cameraDistance,60));
        rotdiff.scale(DisplayManager.getframeTimems());
        diff.scale(DisplayManager.getframeTimems());
        super.rotate(rotdiff);
        super.move(diff);
        camera.setPitch(min(90,max(camera.getPitch()+rotdiff.x,-90)));
        super.getRotation().x = camera.getPitch();
        camera.setYaw(-super.getRotation().y-cameraAngle);
        Vector3f translation = new Vector3f(cameraDistance*(float)( cos(Math.toRadians(camera.getPitch()))* sin(Math.toRadians(super.getRotation().y+cameraAngle))),
                cameraDistance*(float)sin(Math.toRadians(camera.getPitch())),
                cameraDistance*(float)(cos(Math.toRadians(camera.getPitch()))*cos(Math.toRadians(super.getRotation().y+cameraAngle))));
        camera.setPosition(new Vector3f(this.getPosition()).translate(translation.x, translation.y, translation.z));
        rotdiff.set(0, 0, 0);
        diff.set(0, 0, 0);
    }
    
    private void checkInputs(){
        
        Vector3f move = new Vector3f();
        float step = FLY_SPEED, fstep = FLOAT_SPEED;
        if(Keyboard.isKeyDown(Keyboard.KEY_LCONTROL)) {
            step *= BOOST_RATIO;
            fstep *= BOOST_RATIO;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_W)) {
            move.z -= step;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_A)) {
            move.x -= step;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_S)) {
            move.z += step;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_D)) {
            move.x += step;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            move.y -= fstep;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_SPACE)) {
            move.y += fstep;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_H)) {
            rotdiff.y = -TURN_SPEED;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_L)) {
            rotdiff.y = TURN_SPEED;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_J)) {
            rotdiff.x = TURN_SPEED;
        }
        if(Keyboard.isKeyDown(Keyboard.KEY_K)) {
            rotdiff.x = -TURN_SPEED;
        }
        diff.set((float) (move.x * cos(Math.toRadians(camera.getYaw())) 
                - move.z * sin(Math.toRadians(camera.getYaw()))),
                move.y,
                (float) (move.x * sin(Math.toRadians(camera.getYaw()))
                + move.z * cos(Math.toRadians(camera.getYaw()))));
        
        cameraDistance += Mouse.getDWheel() * 0.1f;
        if(Mouse.isButtonDown(1)){
                rotdiff.y -= Mouse.getDX() * 0.02f;
                rotdiff.x -= Mouse.getDY() * 0.02f;
        } else {
            if(Mouse.isButtonDown(0)){
                cameraAngle += Mouse.getDX() *0.3f;
            } else {
            }            
        }
            
    }
}
