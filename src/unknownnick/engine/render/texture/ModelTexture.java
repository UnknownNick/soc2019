package unknownnick.engine.render.texture;

public class ModelTexture {
    
    private int textureID;
    
    private float shineDamper = 1;
    private float reflectivity = 0;
    boolean hasTransparency = false;
    boolean useFakeLight = false;
    
    private int numberOfRows = 1;
    
    public ModelTexture(int id) {
        this.textureID = id;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
    
    public boolean isUseFakeLight() {
        return useFakeLight;
    }

    public void setUseFakeLight(boolean useFakeLight) {
        this.useFakeLight = useFakeLight;
    }
    
    public boolean isHasTransparency() {
        return hasTransparency;
    }

    public void setHasTransparency(boolean hasTransparency) {
        this.hasTransparency = hasTransparency;
    }
    
    public int getID() {
        return this.textureID;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }
    
    
}
