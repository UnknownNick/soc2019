package unknownnick.engine.render.texture;

public class TerrainTextureGroup {
    private TerrainTexture background;
    private TerrainTexture r;
    private TerrainTexture g;
    private TerrainTexture b;

    public TerrainTextureGroup(TerrainTexture background, TerrainTexture r, TerrainTexture g, TerrainTexture b) {
        this.background = background;
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public TerrainTexture getBackground() {
        return background;
    }

    public TerrainTexture getR() {
        return r;
    }

    public TerrainTexture getG() {
        return g;
    }

    public TerrainTexture getB() {
        return b;
    }
    
    
    
}
