package unknownnick.engine.render.model;

import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.texture.ModelTexture;


public class TexturedModel {
    
    private VAO model;
    private ModelTexture texture;
    
    public TexturedModel(VAO model, ModelTexture texture) {
        this.model = model;
        this.texture = texture;
    }
    
    public VAO getModel() {
        return this.model;
    }
    
    public ModelTexture getTexture() {
        return this.texture;
    }
}
