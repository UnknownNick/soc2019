package unknownnick.engine.render.model;

import unknownnick.engine.render.buffer.VAO;
import org.lwjgl.util.vector.Vector3f;

public class ColoredModel {
    private VAO model;
    private Vector3f color;
    private float reflectivity = 0;
    private float shineDamper = 1;
    private boolean useFakeLight = false;

    public ColoredModel(VAO model, Vector3f color) {
        this.model = model;
        this.color = color;
    }

    public float getReflectivity() {
        return reflectivity;
    }

    public void setReflectivity(float reflectivity) {
        this.reflectivity = reflectivity;
    }

    public float getShineDamper() {
        return shineDamper;
    }

    public void setShineDamper(float shineDamper) {
        this.shineDamper = shineDamper;
    }

    public boolean isUseFakeLight() {
        return useFakeLight;
    }

    public void setUseFakeLight(boolean useFakeLight) {
        this.useFakeLight = useFakeLight;
    }
        
    public VAO getModel() {
        return model;
    }

    public Vector3f getColor() {
        return color;
    }

    public void setColor(Vector3f color) {
        this.color = color;
    }
    
}
