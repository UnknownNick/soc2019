package unknownnick.engine.gui.font.render;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import unknownnick.engine.gui.font.FontType;
import unknownnick.engine.gui.font.GUIText;
import unknownnick.engine.gui.font.TextMeshData;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;

public class TextMaster {
    private static Loader loader;
    private static Map<FontType, List<GUIText>> texts = new HashMap<>();
    private static FontRenderer renderer;
    
    public static void init(Loader loader){
        TextMaster.loader = loader;
        renderer = new FontRenderer();
    }
    
    public static void render(){
        renderer.render(texts);
    }
    
    public static void load(GUIText text){
        FontType font = text.getFont();
        TextMeshData data = font.loadText(text);
        VAO vao = loader.loadGUITextVAO(data.getVertexPositions(), data.getTextureCoords());
        text.setMeshInfo(vao);
        List<GUIText> batch = texts.get(font);
        if(batch == null){
            batch = new ArrayList<>();
            texts.put(font, batch);
        }
        batch.add(text);
    }
    
    public static void addLoaded(GUIText text){
        FontType font = text.getFont();
        List<GUIText> batch = texts.get(font);
        if(batch == null){
            batch = new ArrayList<>();
            texts.put(font, batch);
        }
        batch.add(text);
    }
    
    public static void remove(GUIText text){
        FontType font = text.getFont();
        List<GUIText> batch = texts.get(font);
        if(batch != null){
            batch.remove(text);
            if(batch.isEmpty()){
                texts.remove(font);
            }
        }
    }
    
    public static void destroy(GUIText text){
        remove(text);
        if(text.isAlive()){
            loader.free(text.getVAO());
            text.setDead();
        }
    }
    
    public static void cleanUp(){
        renderer.cleanUp();
    }
    
}
