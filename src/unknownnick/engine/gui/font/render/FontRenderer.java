package unknownnick.engine.gui.font.render;

import java.util.List;
import java.util.Map;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import unknownnick.engine.gui.font.FontType;
import unknownnick.engine.render.shader.FontShader;
import unknownnick.engine.gui.font.GUIText;
import unknownnick.engine.render.buffer.VAO;

public class FontRenderer {

	private FontShader shader;

	public FontRenderer() {
		shader = new FontShader();
	}
        
        public void render(Map<FontType, List<GUIText>> texts){
            this.prepare();
            for(FontType font : texts.keySet()){
                GL13.glActiveTexture(GL13.GL_TEXTURE0);
                GL11.glBindTexture(GL11.GL_TEXTURE_2D, font.getTextureAtlas());
                shader.loadEgdeSmoothing(font.attribs.width, font.attribs.edge);
                for(GUIText text : texts.get(font)){
                    renderText(text);
                }
            }
            endRendering();
        }

	public void cleanUp(){
		shader.cleanUp();
	}
	
	private void prepare(){
            GL11.glEnable(GL11.GL_BLEND);
            GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
            GL11.glDisable(GL11.GL_DEPTH_TEST);
            shader.start();
        }
	
	private void renderText(GUIText text){
            VAO vao = text.getVAO();
            vao.bind();
            vao.enableAttribs();
            shader.loadColour(text.getColour());
            shader.loadBorder(text.getBorderWidth(), text.getBorderEdge(),
                    text.getBorderColour(), text.getBorderOffset());
            shader.loatTranslation(text.getPosition());
            GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, vao.getVertexCount());
            vao.disableAttribs();
            VAO.unbind();
        }
	
	private void endRendering(){
            shader.stop();
            GL11.glDisable(GL11.GL_BLEND);
            GL11.glEnable(GL11.GL_DEPTH_TEST);
        }

}
