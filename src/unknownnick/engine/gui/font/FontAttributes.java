package unknownnick.engine.gui.font;

public class FontAttributes {
    public final boolean sdf;
    public final float edge;
    public final float width;
    public final boolean bold;
    public final boolean italic;
    public final float size;

    public FontAttributes(float edge, float width, boolean bold, boolean italic,
            float size) {
        this.sdf = true;
        this.edge = edge;
        this.width = width;
        this.bold = bold;
        this.italic = italic;
        this.size = size;
    }
}
