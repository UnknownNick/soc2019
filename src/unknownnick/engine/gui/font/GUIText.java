package unknownnick.engine.gui.font;

import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.gui.font.render.TextMaster;
import unknownnick.engine.render.buffer.VAO;

/**
 * Represents a piece of text in the game.
 * 
 * @author Karl
 *
 */
public class GUIText {

    private String textString;
    private float fontSize;

    private VAO vao;
    private Vector3f colour = new Vector3f(0f, 0f, 0f);
    private float borderWidth = 0;
    private float borderEdge = 0.1f;
    private Vector3f borderColour;
    private Vector2f borderOffset;

    private Vector2f position;
    private float lineMaxSize;
    private int numberOfLines;

    private FontType font;

    private boolean centerText = false;
    private boolean alive = true;

    /**
     * Creates a new text, loads the text's quads into a VAO, and adds the text
     * to the screen.
     * 
     * @param text
     *            - the text.
     * @param fontSize
     *            - the font size of the text, where a font size of 1 is the
     *            default size.
     * @param font
     *            - the font that this text should use.
     * @param position
     *            - the position on the screen where the top left corner of the
     *            text should be rendered. The top left corner of the screen is
     *            (0, 0) and the bottom right is (1, 1).
     * @param maxLineLength
     *            - basically the width of the virtual page in terms of screen
     *            width (1 is full screen width, 0.5 is half the width of the
     *            screen, etc.) Text cannot go off the edge of the page, so if
     *            the text is longer than this length it will go onto the next
     *            line. When text is centered it is centered into the middle of
     *            the line, based on this line length value.
     * @param centered
     *            - whether the text should be centered or not.
     */
    public GUIText(String text, FontType font, Vector2f position, float maxLineLength,
                    boolean centered) {
            this.textString = text;
            this.fontSize = font.attribs.size;
            this.font = font;
            this.position = position;
            this.lineMaxSize = maxLineLength;
            this.centerText = centered;
            this.borderColour = new Vector3f(0,0,0);
            this.borderOffset = new Vector2f(0, 0);
            TextMaster.load(this);
    }
    
    public GUIText(String text, FontType font, Vector2f position, float maxLineLength,
                    boolean centered, float borderWidth, float borderEdge, 
                    Vector3f borderColour, Vector2f borderOffset) {
            this.textString = text;
            this.fontSize = font.attribs.size;
            this.font = font;
            this.position = position;
            this.lineMaxSize = maxLineLength;
            this.centerText = centered;
            this.borderEdge = borderEdge;
            this.borderWidth = borderWidth;
            this.borderColour = borderColour;
            this.borderOffset = borderOffset;
            TextMaster.load(this);
    }

    public boolean isAlive() {
        return alive;
    }

    public void setDead() {
        this.alive = false;
    }
    
    public void destroy(){
        if(alive){
            TextMaster.destroy(this);
        }
    }

    /**
     * Remove the text from the screen.
     */
    public void remove() {
            TextMaster.remove(this);
    }

    /**
     * @return The font used by this text.
     */
    public FontType getFont() {
            return font;
    }

    /**
     * Set the colour of the text.
     * 
     * @param r
     *            - red value, between 0 and 1.
     * @param g
     *            - green value, between 0 and 1.
     * @param b
     *            - blue value, between 0 and 1.
     */
    public void setColour(float r, float g, float b) {
            colour.set(r, g, b);
}

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public float getBorderEdge() {
        return borderEdge;
    }

    public void setBorderEdge(float borderEdge) {
        this.borderEdge = borderEdge;
    }

    public Vector3f getBorderColour() {
        return borderColour;
    }

    public void setBorderColour(Vector3f borderColour) {
        this.borderColour = borderColour;
    }

    public Vector2f getBorderOffset() {
        return borderOffset;
    }

    public void setBorderOffset(Vector2f borderOffset) {
        this.borderOffset = borderOffset;
    }
        

    /**
     * @return the colour of the text.
     */
    public Vector3f getColour() {
            return colour;
    }

    /**
     * @return The number of lines of text. This is determined when the text is
     *         loaded, based on the length of the text and the max line length
     *         that is set.
     */
    public int getNumberOfLines() {
            return numberOfLines;
    }

           public VAO getVAO(){
               return vao;
           }

    /**
     * @return The position of the top-left corner of the text in screen-space.
     *         (0, 0) is the top left corner of the screen, (1, 1) is the bottom
     *         right.
     */
    public Vector2f getPosition() {
            return position;
    }

    /**
     * @return the ID of the text's VAO, which contains all the vertex data for
     *         the quads on which the text will be rendered.
     */
    public int getMesh() {
            return vao.getID();
    }

    public void setMeshInfo(VAO vao) {
            this.vao = vao;
    }

    /**
     * @return The total number of vertices of all the text's quads.
     */
    public int getVertexCount() {
            return this.vao.getVertexCount();
    }

    /**
     * @return the font size of the text (a font size of 1 is normal).
     */
    protected float getFontSize() {
            return fontSize;
    }

    /**
     * Sets the number of lines that this text covers (method used only in
     * loading).
     * 
     * @param number
     */
    protected void setNumberOfLines(int number) {
            this.numberOfLines = number;
    }

    /**
     * @return {@code true} if the text should be centered.
     */
    protected boolean isCentered() {
            return centerText;
    }

    /**
     * @return The maximum length of a line of this text.
     */
    protected float getMaxLineSize() {
            return lineMaxSize;
    }

    /**
     * @return The string of text.
     */
    protected String getTextString() {
            return textString;
}

}
