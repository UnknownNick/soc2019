package unknownnick.engine.gui;

import org.lwjgl.util.vector.Vector2f;

public class GUITexture {
    private final int ID;
    private Vector2f position;
    private Vector2f scale;

    public GUITexture(int ID, Vector2f position, Vector2f scale) {
        this.ID = ID;
        this.position = position;
        this.scale = scale;
    }

    public int getID() {
        return ID;
    }

    public Vector2f getPosition() {
        return position;
    }

    public Vector2f getScale() {
        return scale;
    }
    
    
}
