package unknownnick.engine.terrain;

import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.render.texture.ModelTexture;
import unknownnick.engine.render.texture.TerrainTexture;
import unknownnick.engine.render.texture.TerrainTextureGroup;


public class Terrain {
    
    private final static float SIZE = 800;
    private final static int VERTEX_COUNT = 128;
    
    private float x;
    private float z;
    private VAO model;
    private TerrainTextureGroup textures;
    private TerrainTexture blendmap;
    
    public Terrain(int gridX, int gridZ, Loader loader, TerrainTextureGroup texture, TerrainTexture blendmap) {
        this.textures = texture;
        this.blendmap = blendmap;
        this.x = gridX * SIZE;
        this.z = gridZ * SIZE;
        this.model = this.generateTerrain(loader);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public VAO getModel() {
        return model;
    }

    public void setModel(VAO model) {
        this.model = model;
    }

    public TerrainTextureGroup getTextures() {
        return textures;
    }

    public TerrainTexture getBlendmap() {
        return blendmap;
    }

    public void setBlendmap(TerrainTexture blendmap) {
        this.blendmap = blendmap;
    }
    public void setTextures(TerrainTextureGroup texture) {
        this.textures = texture;
    }
    
    private VAO generateTerrain(Loader loader){
	int count = VERTEX_COUNT * VERTEX_COUNT;
	float[] vertices = new float[count * 3];
	float[] normals = new float[count * 3];
	float[] textureCoords = new float[count*2];
	int[] indices = new int[6*(VERTEX_COUNT-1)*(VERTEX_COUNT-1)];
	int vertexPointer = 0;
	for(int i=0;i<VERTEX_COUNT;i++){
		for(int j=0;j<VERTEX_COUNT;j++){
			vertices[vertexPointer*3] = (float)j/((float)VERTEX_COUNT - 1) * SIZE;
			vertices[vertexPointer*3+1] = 0;
			vertices[vertexPointer*3+2] = (float)i/((float)VERTEX_COUNT - 1) * SIZE;
			normals[vertexPointer*3] = 0;
			normals[vertexPointer*3+1] = 1;
			normals[vertexPointer*3+2] = 0;
			textureCoords[vertexPointer*2] = (float)j/((float)VERTEX_COUNT - 1);
			textureCoords[vertexPointer*2+1] = (float)i/((float)VERTEX_COUNT - 1);
			vertexPointer++;
		}
	}
	int pointer = 0;
	for(int gz=0;gz<VERTEX_COUNT-1;gz++){
		for(int gx=0;gx<VERTEX_COUNT-1;gx++){
			int topLeft = (gz*VERTEX_COUNT)+gx;
			int topRight = topLeft + 1;
			int bottomLeft = ((gz+1)*VERTEX_COUNT)+gx;
			int bottomRight = bottomLeft + 1;
			indices[pointer++] = topLeft;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = topRight;
			indices[pointer++] = topRight;
			indices[pointer++] = bottomLeft;
			indices[pointer++] = bottomRight;
		}
	}
	return loader.loadTexturedModelVAO(vertices, textureCoords, normals, indices);
    }
}
