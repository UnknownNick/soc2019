package unknownnick.engine.util.math;

import static java.lang.Math.PI;
import static java.lang.Math.acos;
import static java.lang.Math.sqrt;
import java.nio.FloatBuffer;
import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.render.entity.Camera;


public class Maths {
        public static Matrix4f createTransformationMatrix(Vector2f translation, Vector2f scale) {
	Matrix4f matrix = new Matrix4f();
	matrix.setIdentity();
	Matrix4f.translate(translation, matrix, matrix);
	Matrix4f.scale(new Vector3f(scale.x, scale.y, 1f), matrix, matrix);
	return matrix;
    }
    
    public static Matrix4f createtransformationMatrix(Vector3f translation, Vector3f rotation, float scale) {
        Matrix4f matrix = new Matrix4f();
        matrix.setIdentity();
        Matrix4f.translate(translation, matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(rotation.x), new Vector3f(1, 0, 0), matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(rotation.y), new Vector3f(0, 1, 0), matrix, matrix);
        Matrix4f.rotate((float) Math.toRadians(rotation.z), new Vector3f(0, 0, 1), matrix, matrix);
        Matrix4f.scale(new Vector3f(scale, scale, scale), matrix, matrix);
        
        return matrix;
    }
    
    public static Matrix4f createViewMatrix(Camera camera) {
        Matrix4f viewMatrix = new Matrix4f();
        viewMatrix.setIdentity();
        Matrix4f.rotate((float) Math.toRadians(camera.getPitch()), new Vector3f(1, 0, 0), viewMatrix,
                viewMatrix);
        Matrix4f.rotate((float) Math.toRadians(camera.getYaw()), new Vector3f(0, 1, 0), viewMatrix, viewMatrix);
        Vector3f cameraPos = camera.getPosition();
        Vector3f negativeCameraPos = new Vector3f(-cameraPos.x,-cameraPos.y,-cameraPos.z);
        Matrix4f.translate(negativeCameraPos, viewMatrix, viewMatrix);
        return viewMatrix;
    }
    
    public static Vector3f sum(Vector3f a, Vector3f b){
        return new Vector3f(a.x+b.x,a.y+b.y,a.z+b.z);
    }
    
    public static float angle(Vector3f a, Vector3f b){
        float dot = Vector3f.dot(a, b);
        float cos = (float) (dot / sqrt(a.lengthSquared()*b.lengthSquared()));
        return (float) acos(cos);
    }
    
    public static boolean compareMatrices3(Matrix4f a, Matrix4f b){
        if(Float.compare(a.m00, b.m00)!=0)
            return false;
        if(Float.compare(a.m10, b.m10)!=0)
            return false;
        if(Float.compare(a.m20, b.m20)!=0)
            return false;
        if(Float.compare(a.m01, b.m01)!=0)
            return false;
        if(Float.compare(a.m11, b.m11)!=0)
            return false;
        if(Float.compare(a.m21, b.m21)!=0)
            return false;
        if(Float.compare(a.m02, b.m02)!=0)
            return false;
        if(Float.compare(a.m12, b.m12)!=0)
            return false;
        if(Float.compare(a.m22, b.m22)!=0)
            return false;
        return true;
    }
    
    /*
     *Pure rotation matrix!
     */
    public static AxisAnglePair axisFromRotationMatrix(Matrix4f mat) {
        float angle;
        Vector3f axis = new Vector3f(
                mat.m21-mat.m12,
                mat.m02-mat.m20,
                mat.m10-mat.m01
        );
        if(Float.compare(axis.lengthSquared(),0)==0){
            if(compareMatrices3(mat,Matrix4f.setIdentity(null))){
                return new AxisAnglePair(new Vector3f(0,1,0),0);//arbitrary
            } else {
                angle = (float) PI;
		float xx = (mat.m00+1)/2;
		float yy = (mat.m11+1)/2;
		float zz = (mat.m22+1)/2;
		float xy = (mat.m01+mat.m10)/4;
		float xz = (mat.m02+mat.m20)/4;
		float yz = (mat.m12+mat.m21)/4;
                float x,y,z;
                float epsilon = 0.01f;
                if ((xx > yy) && (xx > zz)) {
			if (xx< epsilon) {
				x = 0;
				y = (float) (1/sqrt(2));
				z = (float) (1/sqrt(2));
			} else {
				x = (float) sqrt(xx);
				y = xy/x;
				z = xz/x;
			}
		} else if (yy > zz) {
			if (yy< epsilon) {
				x = (float) (1/sqrt(2));
				y = 0;
				z = (float) (1/sqrt(2));
			} else {
				y = (float) sqrt(yy);
				x = xy/y;
				z = yz/y;
			}	
		} else {
			if (zz< epsilon) {
				x = (float) (1/sqrt(2));
				y = (float) (1/sqrt(2));
				z = 0;
			} else {
				z = (float) sqrt(zz);
				x = xz/z;
				y = yz/z;
			}
		}
                return new AxisAnglePair(new Vector3f(x,y,z), (float) PI);
            }
        }    
        angle=(float) acos((mat.m00+mat.m11+mat.m22)/2);
        
        return new AxisAnglePair(axis,angle);
    }
    
    public static class AxisAnglePair {
        Vector3f axis;
        float angle;

        public AxisAnglePair(Vector3f axis, float angle) {
            this.axis = axis;
            this.angle = angle;
        }

        public Vector3f getAxis() {
            return axis;
        }

        public float getAngle() {
            return angle;
        }
        
        
    }
}
