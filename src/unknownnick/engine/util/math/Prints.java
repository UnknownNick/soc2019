package unknownnick.engine.util.math;

import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

public class Prints {
    
    public static void printMatrix(Matrix4f m) {
        System.out.println("---------------------------");
        System.out.println(m.m00 + " " + m.m10 + " " + m.m20 + " " + m.m30);
        System.out.println(m.m01 + " " + m.m11 + " " + m.m21 + " " + m.m31);
        System.out.println(m.m02 + " " + m.m12 + " " + m.m22 + " " + m.m32);
        System.out.println(m.m03 + " " + m.m13 + " " + m.m23 + " " + m.m33);
        System.out.println("---------------------------");
    }
    
    public static String printVector(Vector3f vec){
        return "("+vec.x+", "+vec.y+", "+vec.z+")";
    }
    
    public static void printArray(Object[] a){
        for(Object o : a){
            System.out.print(o+", ");
        }
    }
    
    public static void printArray(float[] a){
        for(float o : a){
            System.out.print(o+", ");
        }
    }
    
    public static void printArray(int[] a){
        for(int o : a){
            System.out.print(o+", ");
        }
    }
}
