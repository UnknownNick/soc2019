package unknownnick.rotation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DataFile {
    private String filename;
    private final int VERSION = 1;
    private float[] points;
    
    
    public DataFile(String filename){
        this.filename = filename;
    }

    public DataFile(String filename, float[] points) {
        this.filename = filename;
        this.points = points;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public float[] getPoints() {
        return points;
    }

    public void setPoints(float[] points) {
        this.points = points;
    }
       
    public void write(){
        switch(VERSION){
            case 1:
                this.writeDataV1();
                break;
            default:
                
        }
    }
    
    public void read(){
        int ver = this.readVersion();
        switch(ver){
            case 1:
                this.readDataV1();
                break;
            default:
                ;
        }
    }
    
    private void writeDataV1(){
        try {
            BufferedWriter bw;
            File file = new File(filename);
            if(!file.exists()){
                file.createNewFile();
            }
            bw = new BufferedWriter(new FileWriter(file));
            bw.write("#ver 1\n");
            for(int i = 0 ; i < points.length/3 ;i++){
                bw.write(points[3*i] + " " + points[3*i+1] + " " + points[3*i+2] + "\n");
            }
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, "Exception writing data", ex);
        }
    }
    
    private void readDataV1(){
        BufferedReader br;
        String line;
        try {
            br = new BufferedReader(new FileReader(new File(filename)));
            List<Float> floats = new ArrayList<>();
            while((line = br.readLine()) != null) {
                if(line.startsWith("//") || line.startsWith("#")){
                    continue;
                }
                String[] data = line.split(" ");
                for(String f : data)
                    floats.add(Float.parseFloat(f));
            }
            int counter = 0;
            points = new float[floats.size()];
            for(Float f:floats){
                points[counter++] = f;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private int readVersion(){
        BufferedReader br;
        int ver = -1;
        try {
            br = new BufferedReader(new FileReader(new File(filename)));
            String line = br.readLine();
            String version = line.substring("#ver ".length());
            ver = Integer.parseInt(version);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DataFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ver;
    }
}
