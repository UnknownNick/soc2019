package unknownnick.rotation;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.render.entity.Camera;
import unknownnick.engine.render.DisplayManager;
import unknownnick.engine.render.entity.Light;
import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.MasterRenderer;
import unknownnick.engine.render.entity.Spectator;
import unknownnick.rotation.gui.GuiPanel;

public class Rotation {
    static GuiPanel gui;
    static Camera camera;
    static Light light;
    static MasterRenderer renderer;
    static Loader loader;
    static Spectator spectator;
    static List<ActionRequester> requesters = new ArrayList<>();
    static List<Renderable> renderable = new ArrayList<>();

    public static void main(String[] args) {
        try{
            prepare();
        int frames = 0;
        double time = 0;
            while (!(Display.isCloseRequested() | DisplayManager.getCloseRequested())) {
                runActions();
                spectator.move();
                render();
            frames++;
            time += DisplayManager.getframeTimems();
                if(time > 1000){
                    time = 0;
//                    System.out.println("fps:"+frames);
                    frames = 0;
                }
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        } finally{
            end();
        }
    }
    
    private static void prepare(){
        DisplayManager.setTitle("rotace");
        DisplayManager.createAWTDisplay();
        gui = new GuiPanel(DisplayManager.getFrame());
        DisplayManager.setSideGui(gui);
        gui.doLayout();
        loader = new Loader();
        camera = new Camera();
        light = new Light(new Vector3f(100,100,50), new Vector3f(1,1,1));
        spectator = new Spectator(null, camera.getPosition().translate(5, 5, 5),
                new Vector3f(camera.getPitch(), camera.getYaw(), camera.getRoll()),
                0.1f, camera);
        renderer = new MasterRenderer(loader);
        AxisandPoints.setRenderer(renderer);
        requesters.add(new AxisandPoints(new Vector3f()));
    }
    
    private synchronized static void runActions(){
        for(ActionRequester ar : requesters){
            if(!ar.isActionListEmpty()){
                ar.runAllActions();
            }
        }
    }
    
    private static void render(){
        DisplayManager.checkResize(renderer);
        
        for(Renderable r : renderable){
            r.processEntities();
        }
        
        renderer.render(light, camera, new Vector4f());
        DisplayManager.updateDisplay();
    }
    
    private static void end(){
        renderer.cleanUp();
        loader.cleanUp();
        DisplayManager.closeDisplay();
    }
    
    public static Loader getLoader(){
        return loader;
    }
}
