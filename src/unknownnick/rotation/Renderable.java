package unknownnick.rotation;

public interface Renderable {
    public void processEntities();
}
