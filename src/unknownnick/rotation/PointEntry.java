package unknownnick.rotation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.util.math.Prints;
import unknownnick.math.PointData;
import unknownnick.math.PointData.Transformation;
import unknownnick.math.QuaternionRotation;

public class PointEntry implements ActionRequester {
    AxisandPoints vis;
    List<Vector3f> points = new ArrayList<>();
    List<Action> actions = new CopyOnWriteArrayList<>();
    List<Vector3f> toRemove = new CopyOnWriteArrayList<>();
    List<Vector3f> toAdd = new CopyOnWriteArrayList<>();
    boolean remove = false;
    boolean add = false;
    public String optRotationString;
    
    public PointEntry(){
        vis = AxisandPoints.getNext();
        Rotation.requesters.add(this);
        vis.setShowAxis(false);
        Rotation.requesters.add(vis);
    }
    
    public PointEntry findTransormedNAIVE(PointEntry dest, int phi, int alpha){
        PointData srcData = new PointData(this.points);
        PointData destData = new PointData(dest.points);
        Transformation res = PointData.findMinimumNAIVE(srcData, destData, phi, alpha);
        PointEntry entry = getTransformed(res.translation, res.rotation);
        entry.getVis().setAxisVector(res.rotation.getAxis());
        entry.optRotationString = res.rotation.toString(true) +" + "+ Prints.printVector(res.translation);
        return entry;
    }
    
    public PointEntry findTransormedMINE(PointEntry dest, int iter, int phi, int alpha){
        PointData srcData = new PointData(this.points);
        PointData destData = new PointData(dest.points);
        Transformation res = PointData.findMinimum(srcData, destData, iter, phi, alpha);
        PointEntry entry = getTransformed(res.translation, res.rotation);
        entry.getVis().setAxisVector(res.rotation.getAxis());
        entry.optRotationString = res.rotation.toString(true) +" + "+ Prints.printVector(res.translation);
        return entry;
    }
    
    public PointEntry findTransormedREF(PointEntry dest){
        PointData srcData = new PointData(this.points);
        PointData destData = new PointData(dest.points);
        Transformation res = PointData.findMinimumReferenceAlgorithmBigD(srcData, destData);
        PointEntry entry = getTransformed(res.translation, res.rotation);
        entry.getVis().setAxisVector(res.rotation.getAxis());
        entry.optRotationString = res.rotation.toString(true) +" + "+ Prints.printVector(res.translation);
        return entry;
    }
    
    public PointEntry getTransformed(Vector3f translation, QuaternionRotation rot){
        PointEntry tr = new PointEntry();
        for(Vector3f point : points){
            tr.add(rot.rotate(point).translate(translation.x, translation.y, translation.z));
        }
        return tr;
    }

    public AxisandPoints getVis() {
        return vis;
    }
    
    public List<Vector3f> getPoints(){
        return points;
    }
        
    public void add(Vector3f point){
        if(!add)
            actions.add(new ActionAdd());
        add = true;
        points.add(point);
        toAdd.add(point);
    }
    
    public void remove(Vector3f pos){
        if(!remove)
            actions.add(new ActionRemove());
        remove = true;
        points.remove(pos);
        toRemove.add(pos);
    }
    
    public float[] exportArray(){
        float[] pos = new float[points.size()*3];
        int counter = 0;
        for(Vector3f vec : points){
            pos[3*counter] = vec.x;
            pos[3*counter+1] = vec.y;
            pos[3*counter+2] = vec.z;
            counter++;
        }
        return pos;
    }
    
    public void readFromArray(float[] a){
        for(int i = 0; i<a.length/3 ;i++){
            this.add(new Vector3f(a[3*i],a[3*i+1],a[3*i+2]));
        }
    }
    
    public Vector3f getColor(){
        return vis.getColour();
    }

    @Override
    public void runAllActions() {
        List<Action> temp = new ArrayList<>();
        for(Action a : actions){
            a.run();
            temp.add(a);
        }
        actions.removeAll(temp);
    }

    @Override
    public boolean isActionListEmpty() {
        return actions.isEmpty();
    }
    
    public class ActionRemove implements Action {
        @Override
        public void run() {
            for(Vector3f vec : toRemove){
                vis.remove(vec);
            }
            remove = false;
            toRemove.clear();
        }        
    }
    
    public class ActionAdd implements Action {
        @Override
        public void run() {
            for(Vector3f vec : toAdd){
                vis.add(vec);
            }
            add = false;
            toAdd.clear();
        }        
    }
}
