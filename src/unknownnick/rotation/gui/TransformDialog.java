package unknownnick.rotation.gui;

import java.awt.Button;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BoxLayout;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.util.math.Prints;
import unknownnick.math.QuaternionRotation;

public class TransformDialog extends Dialog {
    Frame owner;
    GuiPanel pan;
    QuaternionRotation rot;
    Vector3f translation;
    VertexPanel panelAxis;
    VertexPanel panelTrans;
    TextField fieldRot;
    Label labelRot, labelAxis, labelTrans;
    Button buttonOK;
    
    
    public TransformDialog(Frame owner, GuiPanel pan){
        super(owner, "transformation");
        this.owner = owner;
        this.pan = pan;
        panelAxis = new VertexPanel();
        panelTrans = new VertexPanel();
        fieldRot = new TextField();
        labelRot = new Label("angle");
        labelAxis = new Label("axis");
        labelTrans = new Label("translation");
        buttonOK = new Button("OK");
        
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        Panel panel1, panel2, panel3;
        panel1 = new Panel();
        panel1.setLayout(new BoxLayout(panel1, BoxLayout.LINE_AXIS));
        panel2 = new Panel();
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.LINE_AXIS));
        panel3 = new Panel();
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.LINE_AXIS));
        this.add(panel1);
        this.add(panel2);
        this.add(panel3);
        panel1.add(labelTrans);
        panel1.add(panelTrans);
        panel2.add(labelAxis);
        panel2.add(panelAxis);
        panel3.add(labelRot);
        panel3.add(fieldRot);
        panel3.add(buttonOK);
        
        buttonOK.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                pan.runTransform();
                TransformDialog.this.setVisible(false);
            }            
        });
        
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                setVisible(false);
            }
        });
        this.pack();
    }
    
    public QuaternionRotation getRotation(){
        float angle = Float.parseFloat(fieldRot.getText());
        Vector3f axis = panelAxis.read();
        System.out.println("[manual transform] Normalized axis: "+Prints.printVector(axis.normalise(null)));
        return new QuaternionRotation(axis, angle);
    }
    
    public Vector3f getTranslation(){
        return panelTrans.read();
    }
}
