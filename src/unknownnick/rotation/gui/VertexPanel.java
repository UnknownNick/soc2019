package unknownnick.rotation.gui;

import java.awt.Dimension;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import javax.swing.BoxLayout;
import org.lwjgl.util.vector.Vector3f;

public class VertexPanel extends Panel {
    TextField first, second, third;
    Label x, y, z;
    public VertexPanel(){
        first = new TextField();
        second = new TextField();
        third = new TextField();
        x = new Label("x:");
        y = new Label("y:");
        z = new Label("z:");
        
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.setPreferredSize(new Dimension(250,30));
        
        this.add(x);
        this.add(first);
        this.add(y);
        this.add(second);
        this.add(z);
        this.add(third);
    }
    
    public Vector3f read(){
        float fx = 0, fy = 0, fz = 0;
        
        fx = Float.parseFloat(first.getText());
        fy = Float.parseFloat(second.getText());
        fz = Float.parseFloat(third.getText());
        
        return new Vector3f(fx, fy, fz);
    }
}
