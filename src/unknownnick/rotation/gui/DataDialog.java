package unknownnick.rotation.gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.rotation.DataFile;
import unknownnick.rotation.PointEntry;

public class DataDialog extends Dialog {
    List points;
    PointEntry entry;
    Button loadButton;
    Button saveButton;
    Button addButton;
    Button removeButton;
    String group;
    GuiPanel parent;
    ColorIcon icon;
    VertexPanel floatField;
    DataDialog me;
    Map<String, Vector3f> pointmap = new HashMap();
    Label rotationInfo;
    
    public DataDialog(Frame owner, String group, GuiPanel parent, PointEntry ent) {
        super(owner);
        this.group = group;
        this.parent = parent;
        this.setTitle(group);
        this.setModalityType(ModalityType.MODELESS);
        
        entry = ent;
        icon = new ColorIcon(entry.getColor());
        points = new List();
        loadButton   = new Button("load");
        saveButton   = new Button("save");
        addButton    = new Button("add");
        removeButton = new Button("remove");
        floatField = new VertexPanel();
        
        this.setLayout(new BorderLayout(2,1));
        Panel panel1 = new Panel();
        panel1.setLayout(new GridLayout());
        Panel panel2 = new Panel(); 
        panel2.setLayout(new GridLayout(1,2));
        Panel panel21 = new Panel();
        panel21.setLayout(new FlowLayout());
        
        this.add(points,BorderLayout.CENTER);
        this.add(panel1,BorderLayout.SOUTH);
        panel1.add(panel2);
        panel2.add(panel21);
        panel2.add(floatField);
        panel21.add(loadButton );  
        panel21.add(saveButton );  
        panel21.add(addButton  );  
        panel21.add(removeButton);
        panel21.add(icon);
        
        floatField.setSize(30, 120);
        entry.getVis().setShowAxis(false);
        
        addButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                Vector3f vec = floatField.read();
                String key = vecToString(vec);
                pointmap.put(key, vec);
                points.add(key);
                entry.add(vec);
            }
        });
        removeButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = points.getSelectedItem();
                if(s != null){
                    Vector3f vec = pointmap.get(s);
                    entry.remove(vec);
                    points.remove(s);
                    pointmap.remove(s);
                }
            }
        });
        saveButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                FileDialog dialog = new FileDialog(me,"save");
                dialog.setMode(FileDialog.SAVE);
                dialog.setVisible(true);
                String filename = dialog.getDirectory()+dialog.getFile();
                if(filename.equals("nullnull"))
                    return;
                DataFile file = new DataFile(filename);
                file.setPoints(entry.exportArray());
                file.write();
            }
        });
        loadButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                FileDialog dialog = new FileDialog(me,"load");
                dialog.setMode(FileDialog.LOAD);
                dialog.setVisible(true);
                String filename = dialog.getDirectory()+dialog.getFile();
                if(filename.equals("nullnull"))
                    return;
                DataFile file = new DataFile(filename);
                file.read();
                entry.readFromArray(file.getPoints());
                fillMap();
            }
        });
        
        this.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosing(WindowEvent e){
                setVisible(false);
            }
        });
        if(!entry.getPoints().isEmpty()){
            fillMap();
        }
        
        this.pack();
        this.setVisible(true);
    }
    
    public void populateRotationInfo(String info){
        if(info == null)
            return;
        rotationInfo = new Label(info);
        this.add(rotationInfo, BorderLayout.NORTH);
    }
    
    public DataDialog(Frame owner, String group, GuiPanel parent){
        this(owner, group, parent, new PointEntry());
    }

    public PointEntry getEntry() {
        return entry;
    }
    
    public String vecToString(Vector3f vec){
        return ("("+vec.x + ";"+vec.y+";"+vec.z+")");
    }
    
    private void fillMap(){
        for(Vector3f vec : entry.getPoints()){
            String key = vecToString(vec);
            pointmap.put(key, vec);
            points.add(key);
        }
    }
    
    private class ColorIcon extends Panel {
        public ColorIcon(Vector3f color){
            this.setBackground(new Color(color.x,color.y,color.z));
        }
    }
}
