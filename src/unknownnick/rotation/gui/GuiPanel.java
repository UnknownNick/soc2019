package unknownnick.rotation.gui;

import java.awt.Button;
import java.awt.Choice;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Label;
import java.awt.List;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.util.math.Prints;
import unknownnick.math.PointData;
import unknownnick.math.QuaternionRotation;
import unknownnick.rotation.AxisandPoints;
import unknownnick.rotation.PointEntry;

public class GuiPanel extends Panel {
    Button buttonAdd, buttonEdit, buttonSelectDestination, buttonFind;
    Button buttonTransform;
    List listGroups;
    Frame owner;
    Map<String, DataDialog> dialogs = new HashMap<>();
    Label destination, iterations, verticalPrecision, horizontalPrecision;
    TransformDialog diagTransform;
    TextField fieldIterations, fieldVertical, fieldHorizontal;
    Choice algo;
    
    public enum AlgoType{
        REF,
        NAIVE,
        MINE;
    }
    
    public GuiPanel(Frame owner){
        this.owner = owner;
        buttonAdd = new Button("add group");
        buttonEdit = new Button("edit");
        buttonSelectDestination = new Button("select dest");
        buttonFind = new Button("find transform");
        buttonTransform = new Button("transform");
        listGroups = new List();
        destination = new Label("nothing sel.");
        iterations = new Label("iterations:");
        verticalPrecision = new Label("vert samples:");
        horizontalPrecision = new Label("horiz. samples:");
        diagTransform = new TransformDialog(owner, this);
        fieldIterations = new TextField(""+PointData.MAXITERATIONS);
        fieldVertical = new TextField(""+PointData.PRECISIONPhi);
        fieldHorizontal = new TextField(""+PointData.PRECISIONAlpha);
        algo = new Choice();
        for(AlgoType t : AlgoType.values()){
            algo.add(t.name());
        }
        
        this.setLayout(new FlowLayout());
        buttonAdd.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                listGroups.add("    vertices " + listGroups.getItemCount());
            }
        });
        buttonEdit.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String selected = listGroups.getSelectedItem();
                if(dialogs.containsKey(selected)){
                    DataDialog d = dialogs.get(selected);
                    d.setVisible(true);
                    d.toFront();
                } else {
                    if(selected != null)
                        dialogs.put(selected, new DataDialog(owner, selected, GuiPanel.this));
                }
            }
        });
        buttonSelectDestination.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String key = listGroups.getSelectedItem();
                if(key == null){
                    destination.setText("nothing selected");
                } else {
                    destination.setText(key);
                }
                GuiPanel.this.doLayout();
            }
        });
        buttonFind.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                String sel = listGroups.getSelectedItem();
                String des = destination.getText();
                AlgoType algorithm = AlgoType.valueOf(algo.getSelectedItem());
                if(sel != null && dialogs.containsKey(des)) {
                    String name = "    gened " + algorithm.name() + " " + listGroups.getItemCount();
                    listGroups.add(name);
                    PointEntry selected = dialogs.get(sel).getEntry();
                    PointEntry dest = dialogs.get(des).getEntry();
                    PointEntry entry = null;
                    switch(algorithm){
                        case REF:
                            entry = selected.findTransormedREF(dest);
                            break;
                        case MINE:
                            entry = selected.findTransormedMINE(dest, GuiPanel.this.readIterations(),
                                    GuiPanel.this.readVerticalPrecision(), GuiPanel.this.readHorizontalPrecision());
                            break;
                        case NAIVE:
                            entry = selected.findTransormedNAIVE(dest,
                                    GuiPanel.this.readVerticalPrecision(), GuiPanel.this.readHorizontalPrecision());
                        default:
                            break;
                    }
                    DataDialog diag = new DataDialog(owner, name, GuiPanel.this, entry);
                    diag.populateRotationInfo(entry.optRotationString);
                    AxisandPoints vis = diag.getEntry().getVis();
                    vis.requestAxisRotate();
                    vis.setShowAxis(true);
                    dialogs.put(name, diag);
                }
            }
        });
        buttonTransform.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listGroups.getSelectedItem() != null)
                    diagTransform.setVisible(true);
            }            
        });
        
        this.add(listGroups);
        this.add(buttonAdd);
        this.add(buttonEdit);
        this.add(buttonSelectDestination);
        this.add(destination);
        this.add(buttonFind);
        this.add(buttonTransform);
        this.add(algo);
        this.add(iterations);
        this.add(fieldIterations);
        this.add(verticalPrecision);
        this.add(fieldVertical);
        this.add(horizontalPrecision);
        this.add(fieldHorizontal);
    }
    
    public void remove(String dialog){
        dialogs.remove(dialog);
    }
    
    public void runTransform(){
        String selected = listGroups.getSelectedItem();
        if(selected == null)
            return;
        PointEntry entry = dialogs.get(selected).getEntry();
        Vector3f translation = diagTransform.getTranslation();
        QuaternionRotation rot = diagTransform.getRotation();
        if(translation == null || rot == null){
            return;
        }
        PointEntry dest = entry.getTransformed(translation, rot);
        String name = "    transformed " + listGroups.getItemCount();
        listGroups.add(name);
        DataDialog diag = new DataDialog(owner, name, GuiPanel.this, dest);
        diag.populateRotationInfo(rot.toString(true) + " + " + Prints.printVector(translation));
        dialogs.put(name, diag);
    }
    
    public int readIterations(){
        try{
            return Integer.parseInt(fieldIterations.getText());
        } catch (Exception ex){
            return PointData.MAXITERATIONS;
        }
    }
    
    public int readVerticalPrecision(){
        try{
            return Integer.parseInt(fieldVertical.getText());
        } catch (Exception ex){
            return PointData.PRECISIONPhi;
        }
    }
    
    public int readHorizontalPrecision(){
        try{
            return Integer.parseInt(fieldVertical.getText());
        } catch (Exception ex){
            return PointData.PRECISIONAlpha;
        }
    }
}
