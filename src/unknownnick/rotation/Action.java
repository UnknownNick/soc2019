package unknownnick.rotation;

public interface Action {
    public void run();
}
