package unknownnick.rotation.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.entity.GroupColoredEntity;
import unknownnick.engine.render.entity.TransformationAttributes;
import unknownnick.engine.render.model.ColoredModel;

public class PointGroup {
    private GroupColoredEntity gentity;
    private ColoredModel model;
    private Vector3f color;
    private List<Vector3f> positions = new ArrayList<>();
    private Map<Vector3f, TransformationAttributes> atts = new HashMap<>();

    public PointGroup(Vector3f color) {
        this.color = color;
        model = new ColoredModel(Models.getSphere(), color);
        gentity = new GroupColoredEntity(model);
    }
    
    public float[] getPosArray(){
        float[] pos = new float[positions.size() * 3];
        int pointer = 0;
        for(Vector3f vec : positions){
            pos[pointer++] = vec.x;
            pos[pointer++] = vec.y;
            pos[pointer++] = vec.z;
        }
        return pos;
    }
    
    public void remove(Vector3f position){
        positions.remove(position);
        gentity.remove(atts.remove(position));
    }
    
    public void add(Vector3f position){
        positions.add(position);
        TransformationAttributes att = new TransformationAttributes(position, new Vector3f(0,0,0), 0.1f);
        gentity.add(att);
        atts.put(position, att);
    }
    
    public void setColor(Vector3f color){
        this.color = color;
        model.setColor(color);
    }

    public GroupColoredEntity getGentity() {
        return gentity;
    }

    public ColoredModel getModel() {
        return model;
    }

    public Vector3f getColor() {
        return color;
    }

    public List<Vector3f> getPositions() {
        return positions;
    }
    
    
    
}
