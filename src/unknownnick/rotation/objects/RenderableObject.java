package unknownnick.rotation.objects;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.MasterRenderer;
import unknownnick.engine.render.entity.RenderableEntity;

public class RenderableObject {
    Vector3f posRelativeToOrigin;
    RenderableEntity attachedEntity;
    Vector3f baseRot;
    boolean render = true;

    public RenderableObject(Vector3f posRelativeToOrigin, RenderableEntity attachedEntity) {
        this.posRelativeToOrigin = posRelativeToOrigin;
        this.attachedEntity = attachedEntity;
        this.baseRot = new Vector3f();
    }
    
    public void processEntity(MasterRenderer renderer){
        renderer.processEntity(attachedEntity);
    }

    public Vector3f getBaseRot() {
        return baseRot;
    }

    public void setBaseRot(Vector3f baseRot) {
        this.baseRot = baseRot;
    }
    
    public Vector3f getPosRelativeToOrigin() {
        return posRelativeToOrigin;
    }

    public RenderableEntity getAttachedEntity() {
        return attachedEntity;
    }

    public void setPosRelativeToOrigin(Vector3f posRelativeToOrigin) {
        this.posRelativeToOrigin = posRelativeToOrigin;
    }

    public boolean isVisible() {
        return render;
    }

    public void setVisible(boolean render) {
        this.render = render;
    }
    
    
}
