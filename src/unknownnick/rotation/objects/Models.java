package unknownnick.rotation.objects;

import unknownnick.engine.render.buffer.Loader;
import unknownnick.engine.render.buffer.VAO;
import unknownnick.engine.shape.ClosedCylinder;
import unknownnick.engine.shape.Sphere;

public class Models {
    public static Loader loader = new Loader();
    
    public static VAO getSphere(){
        Sphere sphere = new Sphere(2);
        return sphere.getModel().loadPlainVAO(loader);
    }
    
    public static VAO getClosedCylinder(float heightRatio){
        ClosedCylinder cyl = getClosedCylinderObject(heightRatio);
        return cyl.getModel().loadPlainVAO(loader);
    }
    
    public static ClosedCylinder getClosedCylinderObject(float heightRatio){
        ClosedCylinder cyl = new ClosedCylinder(30,heightRatio);
        return cyl;
    }
    
}
