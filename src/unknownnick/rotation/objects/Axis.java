package unknownnick.rotation.objects;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.entity.RenderableEntity;
import unknownnick.engine.render.model.ColoredModel;

public class Axis extends RenderableObject {
    public static final float HEIGHT = 400f;
    
    public Axis(Vector3f pos, Vector3f colour){
        super(pos, getNewEntity(colour));
    }
    
    private static RenderableEntity getNewEntity(Vector3f colour){
        return new RenderableEntity(new ColoredModel(Models.getClosedCylinder(HEIGHT),colour),
                new Vector3f(0,0,0), new Vector3f(0,0,0), 0.1f);
    }
}
