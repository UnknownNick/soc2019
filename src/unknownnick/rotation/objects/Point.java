package unknownnick.rotation.objects;

import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.render.entity.RenderableEntity;
import unknownnick.engine.render.model.ColoredModel;

public class Point extends RenderableObject {
    public Point(Vector3f pos, Vector3f colour){
        super(pos, getNewEntity(colour));
    }
    
    private static RenderableEntity getNewEntity(Vector3f colour){
        return new RenderableEntity(new ColoredModel(Models.getSphere(), colour),
                new Vector3f(0,0,0), new Vector3f(0,0,0), 0.1f);
    }
}
