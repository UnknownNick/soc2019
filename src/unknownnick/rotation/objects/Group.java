package unknownnick.rotation.objects;

import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.render.MasterRenderer;
import unknownnick.engine.util.math.Maths;
import unknownnick.math.Hf;
import static unknownnick.math.Hf.mul;
import unknownnick.math.QuaternionRotation;
import static unknownnick.math.Hf.mul;

public class Group {
    Vector3f origin;
    List<RenderableObject> objects = new ArrayList<>();
    Vector3f axisVec;
    float rotation;

    public Group() {
        origin = new Vector3f();
        axisVec = new Vector3f(0,1,0);
        rotation = 0;
    }
    
    public void rotate(Vector3f axis, float angle){
        QuaternionRotation curr = new QuaternionRotation(axisVec,rotation);
        QuaternionRotation apply = new QuaternionRotation(axis,angle);
        QuaternionRotation newer = new QuaternionRotation(mul(curr.getQ(), apply.getQ()));
        axisVec = newer.getAxis();
        rotation = newer.getAngle();
    }
    
    public void updateChild(){    }

    public Vector3f getAxisVec() {
        return axisVec;
    }

    public void setAxisVec(Vector3f axisVec) {
        this.axisVec = axisVec;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }
        
    public void add(RenderableObject o){
        objects.add(o);
    }
    
    public void remove(RenderableObject o){
        objects.remove(o);
    }

    public Vector3f getOrigin() {
        return origin;
    }

    public void setOrigin(Vector3f origin) {
        this.origin = origin;
    }
    
    public void processEntities(MasterRenderer renderer){
        for(RenderableObject o : objects){
            o.processEntity(renderer);
        }
    }
    
    public void updatePositions(){
        QuaternionRotation rot = new QuaternionRotation(axisVec,rotation);
        Hf or = new Hf(0,origin);
        for(RenderableObject o : objects){
            Vector3f p = o.getPosRelativeToOrigin();
            p = Hf.sum(new Hf(0,rot.rotate(p)),or).getVector();
            o.getAttachedEntity().setPosition(p);
        }
        this.updateChild();
    }
}
