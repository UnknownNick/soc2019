package unknownnick.rotation;

public interface ActionRequester {
    public void runAllActions();
    public boolean isActionListEmpty();
}
