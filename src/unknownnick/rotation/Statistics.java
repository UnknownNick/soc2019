package unknownnick.rotation;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Math.PI;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.util.math.Maths;
import unknownnick.math.Cbd;
import unknownnick.math.PointData;
import unknownnick.math.PointData.Transformation;
import unknownnick.math.QuaternionRotation;

public class Statistics {
    public static int TESTS = 2;
    public static int PRECISION_PHI = 10;
    public static int PRECISION_ALPHA = 40;
    public static int ITERATIONS = 19;
    public static int BIGDECIMAL_PRECISION = 50;
    public static int MAX = 100;
    public static String filename = "stat output.txt";

    public static void main(String[] args) throws FileNotFoundException, IOException {
        readArgs(args);
        Cbd.context = new MathContext(BIGDECIMAL_PRECISION, RoundingMode.HALF_EVEN);
        PointData.context = Cbd.context;
        warmUp();
        PrintWriter out = new PrintWriter(filename);
        printSettings(out);
        for(int count = 10; count <= MAX ;count *= 10){
            printTest(runTests(count, TESTS, ITERATIONS, PRECISION_PHI, PRECISION_ALPHA), out);
        }
        out.close();
    }
    
    public static void readArgs(String[] args){
        if(args.length == 0)
            return;
        else if(args.length != 7){
            System.err.println("Argument count invalid.");
            System.out.println("layout: <filename> <TESTS> <PRECISION_PHI> <PRECISION_ALPHA> <ITERATIONS> <BIGDECIMAL_PRECISION> <MAX>");
            System.exit(1);
        } else {
            filename = args[0];
            TESTS = Integer.parseInt(args[1]);
            PRECISION_PHI = Integer.parseInt(args[2]);
            PRECISION_ALPHA = Integer.parseInt(args[3]);
            ITERATIONS = Integer.parseInt(args[4]);
            BIGDECIMAL_PRECISION = Integer.parseInt(args[5]);
            MAX = Integer.parseInt(args[6]);
        }
    }
    
    public static void printSettings(PrintWriter out){
        out.println("Tests run: "+TESTS);
        out.println("settings for REF algorithm:");
        out.println("digits of BigDecimal: "+BIGDECIMAL_PRECISION);
        out.println("settings for MINE algorithm:");
        out.println("Phi precision: "+PRECISION_PHI+" samples");
        out.println("Alpha precision: "+PRECISION_ALPHA+" samples");
        out.println("Iterations: "+ITERATIONS);
        out.println("settings for NAIVE algorithm:");
        out.println("Phi precision: "+PRECISION_PHI+" samples");
        out.println("Alpha precision: "+PRECISION_ALPHA+" samples");
    }
    
    public static void printTest(Map<String, List<Result>> data, PrintWriter out) throws IOException{
        List<Result> input = data.remove("input");
        for(int i = 0; i < input.size() ;i++){
            Result in = input.get(i);
            out.println("-------------------");
            out.println("Transformation: " + in.transform);
            out.println("points: "+in.samples);
            out.println();
            for(String key : data.keySet()){
                List<Result> run = data.get(key);
                out.println(key+":");
                Result res = run.get(i);
                out.println("found transform: "+res.transform);
                out.println("took: "+res.time+"ms");
                float dist = res.totalQuadraticDistance;
                out.println("Total quadratic distance: "+ dist);
                out.println("avg: "+dist/in.samples);
                out.println();
            }
        }
        out.println("statistics:");
        for(String key : data.keySet()){
            List<Result> run = data.get(key);
            long totalTime = 0L;
            double totalDistance = 0L;
            TreeSet<Double> distance = new TreeSet<>();
            TreeSet<Double> time = new TreeSet<>();
            int samp = input.get(0).samples;
            for(Result res : run){
                totalTime += res.time;
                totalDistance += res.totalQuadraticDistance;
                distance.add((double)res.totalQuadraticDistance/samp);
                time.add((double)res.time);
            }
            double maxdis = distance.last();
            double mindis = distance.first();
            double vardis = variance(distance);
            double sddis = Math.sqrt(vardis);
            double maxtim = time.last();
            double mintim = time.first();
            double vartim = variance(time);
            double sdtim = Math.sqrt(vartim);
            out.println(key+":");
            out.println("avg time: " + totalTime/run.size() + "ms");
            out.println("min time: " + mintim + "ms");
            out.println("max time: " + maxtim + "ms");
            out.println("var time: " + vartim + "ms^2");
            out.println("std time: " + sdtim + "ms");
            out.println("avg total quadratic distance: " + totalDistance/run.size());
            out.println("avg average quadratic distance: " + totalDistance/run.size()/samp);
            out.println("min average quadratic distance: " + mindis);
            out.println("max average quadratic distance: " + maxdis);
            out.println("var average quadratic distance: " + vardis);
            out.println("std average quadratic distance: " + sddis);
        }
    }
    
    public static double variance(Set<Double> data){
        double var = 0;
        double sum = 0;
        int count = 0;
        for(Double el : data){
            sum += el;
            count++;
        }
        double mean = sum/count;
        for(Double el : data){
            double term = el - mean;
            var += term*term;
        }
        var /= count;
        return var;
    }
    
    public static Map<String, List<Result>> runTests(int pointCount, int tests,
            int iter, int phi, int alpha){
        Map<String, List<Result>> ret = new HashMap<>();
        List<Result> mine = new ArrayList<>();
        List<Result> ref = new ArrayList<>();
        List<Result> naive = new ArrayList<>();
        List<Result> input = new ArrayList<>();
        for(int t = 0; t < tests ;t++){
            List<Vector3f> points = getRandom(pointCount, 20);
            PointData src = new PointData(points);
            Transformation trans = randomTransform(10);
            transform(points, trans);
            PointData dest = new PointData(points);
            Result in = new Result();
            in.transform = trans;
            in.samples = pointCount;
            input.add(in);
            
            mine.add(findMINE(src, dest, iter, phi, alpha));
            
            ref.add(findREF(src, dest));
            
            naive.add(findNAIVE(src, dest, phi, alpha));
        }
        ret.put("input", input);
        ret.put("MINE", mine);
        ret.put("REF", ref);
        ret.put("NAIVE", naive);
        return ret;
    }
    
    public static void warmUp(){
        List<Vector3f> points = getRandom(100, 20);
        PointData src = new PointData(points);
        Transformation trans = randomTransform(10);
        transform(points, trans);
        PointData dest = new PointData(points);
        for(int i = 0; i < 10;i++){
            findMINE(src, dest, 19, 10, 40);
            findNAIVE(src, dest , 5, 20);
            findREF(src, dest);
        }
    }
    
    public static Result findMINE(PointData src, PointData dest, int iter, int phi, int alpha){
        long time0 = System.currentTimeMillis();
        Transformation t = PointData.findMinimum(src, dest, iter, phi, alpha);
        long time = System.currentTimeMillis();
        Result res = new Result();
        res.time = time - time0;
        res.transform = t;
        res.samples = src.pointCount();
        res.totalQuadraticDistance = dest.compareAllSqLen(src.clone().rotate(t.rotation));
        return res;
    }
    
    public static Result findNAIVE(PointData src, PointData dest, int phi, int alpha){
        long time0 = System.currentTimeMillis();
        Transformation t = PointData.findMinimumNAIVE(src, dest, phi, alpha);
        long time = System.currentTimeMillis();
        Result res = new Result();
        res.time = time - time0;
        res.transform = t;
        res.samples = src.pointCount();
        res.totalQuadraticDistance = dest.compareAllSqLen(src.clone().rotate(t.rotation));
        return res;
    }
    
    public static Result findREF(PointData src, PointData dest){
        long time0 = System.currentTimeMillis();
        Transformation t = PointData.findMinimumReferenceAlgorithmBigD(src, dest);
        long time = System.currentTimeMillis();
        Result res = new Result();
        res.time = time - time0;
        res.transform = t;
        res.samples = src.pointCount();
        res.totalQuadraticDistance = dest.compareAllSqLen(src.clone().rotate(t.rotation));
        return res;
    }
    
    public static Transformation randomTransform(float range){
        Random rnd = new Random();
        Transformation t = new Transformation();
        t.translation = new Vector3f((rnd.nextFloat()-0.5f)*range,
                    (rnd.nextFloat()-0.5f)*range, (rnd.nextFloat()-0.5f)*range);
        t.rotation = new QuaternionRotation(new Vector3f((rnd.nextFloat()-0.5f),
                    (rnd.nextFloat()-0.5f), (rnd.nextFloat()-0.5f)), (float) (2*PI*rnd.nextFloat()));
        return t;
    }
    
    public static void transform(List<Vector3f> points, Transformation t){
        for(Vector3f vec : points){
            vec.set(Maths.sum(t.rotation.rotate(vec), t.translation));
        }
    }
    
    public static List<Vector3f> getRandom(int count, float range){
        Random rnd = new Random();
        List<Vector3f> points = new ArrayList<>();
        for(int i = 0; i < count ;i++){
            points.add(new Vector3f((rnd.nextFloat()-0.5f)*range,
                    (rnd.nextFloat()-0.5f)*range, (rnd.nextFloat()-0.5f)*range));
        }
        return points;
    }
    
    public static List<Vector3f> loadFile(String filename){
        List<Vector3f> points = new ArrayList<>();
        DataFile file = new DataFile(filename);
        file.read();
        float[] arr = file.getPoints();
        
        for(int i = 0; i<arr.length/3 ;i++){
            points.add(new Vector3f(arr[3*i],arr[3*i+1],arr[3*i+2]));
        }
        
        return points;
    }
    
    public static void writeFile(List<Vector3f> points, String filename){
        float[] pos = new float[points.size()*3];
        int counter = 0;
        for(Vector3f vec : points){
            pos[3*counter] = vec.x;
            pos[3*counter+1] = vec.y;
            pos[3*counter+2] = vec.z;
            counter++;
        }
        DataFile file = new DataFile(filename);
        file.setPoints(pos);
        file.write();
    }
    
    public static class Result{
        public long time;
        public Transformation transform;
        public int samples;
        public float totalQuadraticDistance;
    }
}
