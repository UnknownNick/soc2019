package unknownnick.rotation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.lwjgl.util.vector.Vector3f;
import unknownnick.engine.io.file.obj.ModelData;
import unknownnick.engine.render.MasterRenderer;
import unknownnick.engine.render.model.ColoredModel;
import unknownnick.engine.shape.ClosedCylinder;
import unknownnick.engine.util.math.Maths;
import unknownnick.math.QuaternionRotation;
import unknownnick.rotation.objects.Axis;
import unknownnick.rotation.objects.Models;
import unknownnick.rotation.objects.PointGroup;

public class AxisandPoints extends PointGroup implements ActionRequester, Renderable {
    private Axis axis;
    private Vector3f axisVector;
    private static ClosedCylinder cachedAxisModel;
    private static List<Action> staticActions = new ArrayList<>();
    private List<Action> instanceActions = new ArrayList<>();
    private static MasterRenderer renderer;
    private boolean showAxis = false;
    
    protected static AxisandPoints next = new AxisandPoints(getRandomColor());
    
    public AxisandPoints(Vector3f colour) {
        super(colour);
        axis = new Axis(new Vector3f(0,0,0),colour);
        if(cachedAxisModel==null)
            cachedAxisModel = Models.getClosedCylinderObject(Axis.HEIGHT);
        Rotation.renderable.add(this);
    }
    
    public void processEntities(){
        if(showAxis)
            renderer.processEntity(axis.getAttachedEntity());
        renderer.processGroupColoredEntity(super.getGentity());
    }

    public static void setRenderer(MasterRenderer renderer) {
        AxisandPoints.renderer = renderer;
    }

    @Override
    public void runAllActions() {
        List<Action> temp = new ArrayList<>();
        for(Action action : instanceActions){
            action.run();
            temp.add(action);
        }
        instanceActions.removeAll(temp);
        temp.clear();
        for(Action action : staticActions){
            action.run();
            temp.add(action);
        }
        staticActions.removeAll(temp);
    }

    @Override
    public boolean isActionListEmpty() {
        return (instanceActions.isEmpty() & staticActions.isEmpty());
    }

    public boolean isShowAxis() {
        return showAxis;
    }

    public void setShowAxis(boolean showAxis) {
        this.showAxis = showAxis;
    }
    
    public static AxisandPoints getNext(){
        staticActions.add(new ActionBufferNextObject());
        AxisandPoints ret = next;
        next = null;
        return ret;
    }
    
    public static void checkNext(){
        if(next == null){
            next = new AxisandPoints(getRandomColor());
        }
    }

    public Vector3f getAxisVector() {
        return axisVector;
    }

    public void setAxisVector(Vector3f axisVector) {
        this.axisVector = axisVector;
    }

    public Vector3f getColour() {
        return super.getColor();
    }
    
    public void requestAxisRotate(){
        this.instanceActions.add(new ActionRotateAxis());
    }
    
    public static Vector3f getRandomColor(){
        Random rnd = new Random();
        return new Vector3f(rnd.nextFloat(),rnd.nextFloat(),rnd.nextFloat());
    }
    
    private void rotateAxis(){
        ModelData temp = cachedAxisModel.getModel().clone();
        
        float angle = Maths.angle(new Vector3f(0,1,0), getAxisVector());
        QuaternionRotation rot = new QuaternionRotation(Vector3f.cross(new Vector3f(0,1,0),
                getAxisVector(), null), angle);
        if(Float.compare(angle, 0)==0){
            return;
        }
        int count = temp.getVertices().length/3;
        for(int i = 0; i < count ;i++){
            Vector3f vec = temp.getVertex(i);
            vec = rot.rotate(vec);
            temp.setVertex(i, vec);
            
            Vector3f normal = temp.getNormal(i);
            normal = rot.rotate(normal);
            temp.setNormal(i, normal);
        }
        axis.getAttachedEntity().setCmodel(new ColoredModel(temp.loadPlainVAO(Models.loader),
                axis.getAttachedEntity().getCmodel().getColor()));
    }
    
    private class ActionRotateAxis implements Action {

        @Override
        public void run() {
            AxisandPoints.this.rotateAxis();
        }
        
    }
    
    private static class ActionBufferNextObject implements Action {

        @Override
        public void run() {
            checkNext();
        }
        
    }
    
}
