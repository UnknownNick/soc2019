/*
 * Copyright (c) 1996, 2018, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

/*
 * Portions Copyright IBM Corporation, 2001. All Rights Reserved.
 */

package unknownnick.math;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import static java.math.BigDecimal.TEN;
import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import java.math.BigInteger;

/**
 * This class is an import of BigDecimal.sqrt() method from openjdk-11 implementation
 * and refactored to work locally
 * The application targets jdk8, so this was necessary
 */
public class jdk11BigDecimal {
    static BigDecimal ONE_HALF = new BigDecimal(0.5f);
    static BigDecimal ONE_TENTH = new BigDecimal("0.1");
    
    public static BigDecimal sqrt(BigDecimal b, MathContext mc) {
        int signum = b.signum();
        if (signum == 1) {
            /*
             * The following code draws on the algorithm presented in
             * "Properly Rounded Variable Precision Square Root," Hull and
             * Abrham, ACM Transactions on Mathematical Software, Vol 11,
             * No. 3, September 1985, Pages 229-237.
             *
             * The BigDecimal computational model differs from the one
             * presented in the paper in several ways: first BigDecimal
             * numbers aren't necessarily normalized, second many more
             * rounding modes are supported, including UNNECESSARY, and
             * exact results can be requested.
             *
             * The main steps of the algorithm below are as follows,
             * first argument reduce the value to the numerical range
             * [1, 10) using the following relations:
             *
             * x = y * 10 ^ exp
             * sqrt(x) = sqrt(y) * 10^(exp / 2) if exp is even
             * sqrt(x) = sqrt(y/10) * 10 ^((exp+1)/2) is exp is odd
             *
             * Then use Newton's iteration on the reduced value to compute
             * the numerical digits of the desired result.
             *
             * Finally, scale back to the desired exponent range and
             * perform any adjustment to get the preferred scale in the
             * representation.
             */

            // The code below favors relative simplicity over checking
            // for special cases that could run faster.

            int preferredScale = b.scale()/2;
            BigDecimal zeroWithFinalPreferredScale = valueOf(0L, preferredScale);

            // First phase of numerical normalization, strip trailing
            // zeros and check for even powers of 10.
            BigDecimal stripped = b.stripTrailingZeros();
            int strippedScale = stripped.scale();

            // Numerically sqrt(10^2N) = 10^N
            if (isPowerOfTen(stripped) &&
                strippedScale % 2 == 0) {
                BigDecimal result = valueOf(1L, strippedScale/2);
                if (result.scale() != preferredScale) {
                    // Adjust to requested precision and preferred
                    // scale as appropriate.
                    result = result.add(zeroWithFinalPreferredScale, mc);
                }
                return result;
            }

            // After stripTrailingZeros, the representation is normalized as
            //
            // unscaledValue * 10^(-scale)
            //
            // where unscaledValue is an integer with the mimimum
            // precision for the cohort of the numerical value. To
            // allow binary floating-point hardware to be used to get
            // approximately a 15 digit approximation to the square
            // root, it is helpful to instead normalize this so that
            // the significand portion is to right of the decimal
            // point by roughly (scale() - precision() +1).

            // Now the precision / scale adjustment
            int scaleAdjust = 0;
            int scale = stripped.scale() - stripped.precision() + 1;
            if (scale % 2 == 0) {
                scaleAdjust = scale;
            } else {
                scaleAdjust = scale - 1;
            }

            BigDecimal working = stripped.scaleByPowerOfTen(scaleAdjust);

            assert  // Verify 0.1 <= working < 10
                ONE_TENTH.compareTo(working) <= 0 && working.compareTo(TEN) < 0;

            // Use good ole' Math.sqrt to get the initial guess for
            // the Newton iteration, good to at least 15 decimal
            // digits. This approach does incur the cost of a
            //
            // BigDecimal -> double -> BigDecimal
            //
            // conversion cycle, but it avoids the need for several
            // Newton iterations in BigDecimal arithmetic to get the
            // working answer to 15 digits of precision. If many fewer
            // than 15 digits were needed, it might be faster to do
            // the loop entirely in BigDecimal arithmetic.
            //
            // (A double value might have as much many as 17 decimal
            // digits of precision; it depends on the relative density
            // of binary and decimal numbers at different regions of
            // the number line.)
            //
            // (It would be possible to check for certain special
            // cases to avoid doing any Newton iterations. For
            // example, if the BigDecimal -> double conversion was
            // known to be exact and the rounding mode had a
            // low-enough precision, the post-Newton rounding logic
            // could be applied directly.)

            BigDecimal guess = new BigDecimal(Math.sqrt(working.doubleValue()));
            int guessPrecision = 15;
            int originalPrecision = mc.getPrecision();
            int targetPrecision;

            // If an exact value is requested, it must only need about
            // half of the input digits to represent since multiplying
            // an N digit number by itself yield a 2N-1 digit or 2N
            // digit result.
            if (originalPrecision == 0) {
                targetPrecision = stripped.precision()/2 + 1;
            } else {
                targetPrecision = originalPrecision;
            }

            // When setting the precision to use inside the Newton
            // iteration loop, take care to avoid the case where the
            // precision of the input exceeds the requested precision
            // and rounding the input value too soon.
            BigDecimal approx = guess;
            int workingPrecision = working.precision();
            do {
                int tmpPrecision = Math.max(Math.max(guessPrecision, targetPrecision + 2),
                                           workingPrecision);
                MathContext mcTmp = new MathContext(tmpPrecision, RoundingMode.HALF_EVEN);
                // approx = 0.5 * (approx + fraction / approx)
                approx = ONE_HALF.multiply(approx.add(working.divide(approx, mcTmp), mcTmp));
                guessPrecision *= 2;
            } while (guessPrecision < targetPrecision + 2);

            BigDecimal result;
            RoundingMode targetRm = mc.getRoundingMode();
            if (targetRm == RoundingMode.UNNECESSARY || originalPrecision == 0) {
                RoundingMode tmpRm =
                    (targetRm == RoundingMode.UNNECESSARY) ? RoundingMode.DOWN : targetRm;
                MathContext mcTmp = new MathContext(targetPrecision, tmpRm);
                result = approx.scaleByPowerOfTen(-scaleAdjust/2).round(mcTmp);

                // If result*result != this numerically, the square
                // root isn't exact
                if (b.subtract(result.multiply(result)).compareTo(ZERO) != 0) {
                    throw new ArithmeticException("Computed square root not exact.");
                }
            } else {
                result = approx.scaleByPowerOfTen(-scaleAdjust/2).round(mc);
            }

            if (result.scale() != preferredScale) {
                // The preferred scale of an add is
                // max(addend.scale(), augend.scale()). Therefore, if
                // the scale of the result is first minimized using
                // stripTrailingZeros(), adding a zero of the
                // preferred scale rounding the correct precision will
                // perform the proper scale vs precision tradeoffs.
                result = result.stripTrailingZeros().
                    add(zeroWithFinalPreferredScale,
                        new MathContext(originalPrecision, RoundingMode.UNNECESSARY));
            }
            assert squareRootResultAssertions(b, result, mc);
            return result;
        } else {
            switch (signum) {
            case -1:
                throw new ArithmeticException("Attempted square root " +
                                              "of negative BigDecimal");
            case 0:
                return valueOf(0L, b.scale()/2);

            default:
                throw new AssertionError("Bad value from signum");
            }
        }
    }
    
    private static boolean squareRootResultAssertions(BigDecimal b, BigDecimal result, MathContext mc) {
        if (result.signum() == 0) {
            return squareRootZeroResultAssertions(b, result, mc);
        } else {
            RoundingMode rm = mc.getRoundingMode();
            BigDecimal ulp = result.ulp();
            BigDecimal neighborUp   = result.add(ulp);
            // Make neighbor down accurate even for powers of ten
            if (isPowerOfTen(b)) {
                ulp = ulp.divide(TEN);
            }
            BigDecimal neighborDown = result.subtract(ulp);

            // Both the starting value and result should be nonzero and positive.
            if (result.signum() != 1 ||
                b.signum() != 1) {
                return false;
            }

            switch (rm) {
            case DOWN:
            case FLOOR:
                return
                    result.multiply(result).compareTo(b)         <= 0 &&
                    neighborUp.multiply(neighborUp).compareTo(b) > 0;

            case UP:
            case CEILING:
                return
                    result.multiply(result).compareTo(b)             >= 0 &&
                    neighborDown.multiply(neighborDown).compareTo(b) < 0;

            case HALF_DOWN:
            case HALF_EVEN:
            case HALF_UP:
                BigDecimal err = result.multiply(result).subtract(b).abs();
                BigDecimal errUp = neighborUp.multiply(neighborUp).subtract(b);
                BigDecimal errDown =  b.subtract(neighborDown.multiply(neighborDown));
                // All error values should be positive so don't need to
                // compare absolute values.

                int err_comp_errUp = err.compareTo(errUp);
                int err_comp_errDown = err.compareTo(errDown);

                return
                    errUp.signum()   == 1 &&
                    errDown.signum() == 1 &&

                    err_comp_errUp   <= 0 &&
                    err_comp_errDown <= 0 &&

                    ((err_comp_errUp   == 0 ) ? err_comp_errDown < 0 : true) &&
                    ((err_comp_errDown == 0 ) ? err_comp_errUp   < 0 : true);
                // && could check for digit conditions for ties too

            default: // Definition of UNNECESSARY already verified.
                return true;
            }
        }
    }

    private static boolean squareRootZeroResultAssertions(BigDecimal b, BigDecimal result, MathContext mc) {
        return b.compareTo(ZERO) == 0;
    }

    private static boolean isPowerOfTen(BigDecimal b) {
        return BigInteger.ONE.equals(b.unscaledValue());
    }
}
