package unknownnick.math;

import static java.lang.Math.PI;
import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;

public class Hf {
    float s;
    float x;
    float y;
    float z;
    
    public Hf(){
        s = 0;
        x = 0;
        y = 0;
        z = 0;
    }

    public Hf(float s, float x, float y, float z) {
        this.s = s;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public Hf(float s, Vector3f v){
        this.s = s;
        this.x = v.x;
        this.y = v.y;
        this.z = v.z;
    }
    
    public Hf(Vector4f vec) {
        this.s = vec.x;
        this.x = vec.y;
        this.y = vec.z;
        this.z = vec.w;
    }
    
    public Hf(float norm, float angle, Vector3f v){
        float cos = (float) cos(angle);
        float sin = (float) sin(angle);
        Vector3f u = v.normalise(null);
        s = norm * cos;
        x = norm * sin * u.x;
        y = norm * sin * u.y;
        z = norm * sin * u.z;        
    }
    
    
    
    public float arg(){
        Hf normalised = normalised();
        float acos = (float) acos(normalised.s);
        Vector3f v = normalised.getVector();
        Vector3f u = v.normalise(null);
        float sin = v.x / u.x;
        float asin = (float) asin(sin);
        float angle = (float) (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public static Hf divL(Hf divisor, Hf right){
        return mul(divisor.inverseMul(),right);
    }
    
    public static Hf div(Hf left, Hf divisor){
        return mul(left, divisor.inverseMul());
    }
    
    public Hf inverseMul(){
        float div = 1/normSq();
        return mul(mul(this,conjugate()),div);
    }
    
    public static Hf mul(Hf q, float r){
        return new Hf(q.s*r,q.x*r,q.y*r,q.z*r);
    }
    
    public Hf normalised(){
        float norm = norm();
        return new Hf(s/norm,x/norm,y/norm,z/norm);
    }
    
    public Hf conjugate(){
        return new Hf(s,-x,-y,-z);
    }
    
    public static Hf mul(Hf left, Hf right){
        return new Hf(
                left.s * right.s - left.x*right.x - left.y*right.y - left.z*right.z,
                left.s * right.x + right.s * left.x + left.y * right.z - left.z * right.y,
                left.s * right.y + right.s * left.y + left.z * right.x - left.x * right.z,
                left.s * right.z + right.s * left.z + left.x * right.y - left.y * right.x
        );
    }
    
    public static Hf sum(Hf left, Hf right){
        return new Hf(left.s + right.s,
                      left.x + right.x,
                      left.y + right.y,
                      left.z + right.z
                      );
    }
    
    public float norm(){
        return (float)sqrt(normSq());
    }
    
    public float normSq(){
        return (s*s+x*x+y*y+z*z);
    }
    
    public Vector4f asVec4(){
        return new Vector4f(s,x,y,z);
    }
    
    public Vector3f getVector(){
        return new Vector3f(x,y,z);
    }

    public float getS() {
        return s;
    }

    public void setS(float s) {
        this.s = s;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
    
    
}
