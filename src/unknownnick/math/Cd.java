package unknownnick.math;

import static java.lang.Math.PI;
import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import org.lwjgl.util.vector.Vector2f;
import unknownnick.engine.util.math.Prints;

public class Cd {
    public double x;
    public double y;

    public Cd(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Cd(){
        this(0,0);
    }
    
    public Cd(double re){
        this(re,0);
    }
    
    public Cd(Vector2f vec){
        x = vec.x;
        y = vec.y;
    }
    
    public static Cd geom(double norm, double angle){
        return new Cd((norm*cos(angle)), (norm*sin(angle)));
    }
    
    public double normSq(){
        return x*x+y*y;
    }
    
    public double norm(){
        return  Math.sqrt(normSq());
    }
    
    public double arg(){
        double norm = norm();
        double nx = x/norm;
        double ny = y/norm;
        double acos =   acos(nx);
        double asin =   asin(ny);
        double angle =   (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public double argFP64(){
        double norm = norm();
        double nx = x/ norm;
        double ny = y/ norm;
        double acos =  acos(nx);
        double asin =  asin(ny);
        double angle =  (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public Cd normalized(){
        double norm = norm();
        return new Cd(x/norm, y/norm);
    }
    
    public static Cd sum(Cd a, Cd b){
        return new Cd(a.x + b.x, a.y+b.y);
    }
    
    public Cd add(Cd a){
        return sum(this, a);
    }
    
    public Cd add(double re){
        return new Cd(x+re,y);
    }
    
    public static Cd mul(Cd a, Cd b){
        return new Cd(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x);
    }
    
    public Cd neg(){
        return new Cd(-x,-y);
    }
    
    public Cd conj(){
        return new Cd(x, -y);
    }
    
    public Cd div(double divisor){
        return new Cd(x/divisor, y/divisor);
    }
    
    public Cd div(Cd divisor){
        return (this.mul(divisor.conj()).mul(1f/divisor.normSq()));
    }
    
    public Cd mul(Cd a){
        return mul(this, a);
    }
    
    public Cd mul(double k){
        return new Cd(k*x, k*y);
    }
    
    public static Cd mul(Cd z, double k){
        return z.mul(k);
    }
    
    public Cd[] sqrt(){
        double norm = norm();
        double a =   Math.sqrt((norm+x)/2);
        double b =   Math.sqrt((norm-x)/2);
        if(y>0){
            return new Cd[]{
                new Cd(a,b),
                new Cd(-a,-b)
            };
        } else {
            return new Cd[]{
                new Cd(-a,b),
                new Cd(a,-b)
            };
        }
    }
    
    public Cd[] nthrt(int n){
        double norm = norm();
        double arg = argFP64();
        Cd[] res = new Cd[n];
        double newnorm =   Math.pow(norm, 1f/n);
        for(int k = 0; k < n ;k++){
            res[k] = geom(newnorm, ((arg+
                    2*k*PI)/n));
        }
        return res;
    }
    
    public Cd pow(int exp){
        if(exp < 8){
            Cd temp = new Cd(1,0);
            for(int i = 0;i<exp;i++){
                temp = this.mul(temp);
            }
            return temp;
        } else {
            double arg = arg();
            double norm = norm();
            return geom(  Math.pow(norm, exp),exp*arg);
        }
    }
    
    public double Re(){
        return x;
    }
    
    public double Im(){
        return y;
    }
    
    public static double Re(Cd z){
        return z.x;
    }
    
    public static double Im(Cd z){
        return z.y;
    }
    
    @Override
    public String toString(){
        return x + " + " + y +"i";
    }
    
    public String debugMethodsOtput(){
        String s = "";
        s+= "x:" +x+"\n";
        s+= "y:" +y+"\n";
        s+= "Im()" +Im()+"\n";
        s+= "Re()" +Re()+"\n";
        s+= "add(new Cf(1,2))" +add(new Cd(1,2))+"\n";
        s+= "add(1)" +add(1)+"\n";
        s+= "arg()" +arg()+"\n";
        s+= "conj()" +conj()+"\n";
        s+= "div(new Cf(1, 2))" +div(new Cd(1, 2))+"\n";
        s+= "geom(2, PI/3):"+geom(2,   (PI/3))+"\n";
        s+= "mul(new Cf(1,2))" +mul(new Cd(1,2))+"\n";
        s+= "mul(2)" +mul(2)+"\n";
        s+= "neg()" +neg()+"\n";
        s+= "norm()" +norm()+"\n";
        s+= "normSq()" +normSq()+"\n";
        s+= "normalized()" +normalized()+"\n";
        Cd[] nt = nthrt(3);
        s+= "nthrt(3)" +nt[0]+", "+nt[1]+", "+nt[2]+"\n";
        s+= "pow(2)" +pow(2)+"\n";
        s+= "pow(30)" +pow(30)+"\n";
        nt = sqrt();
        s+= "sqrt()" +nt[0]+", "+nt[1];
        return s;
    }
}
