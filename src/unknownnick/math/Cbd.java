package unknownnick.math;

import static java.lang.Math.PI;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import org.lwjgl.util.vector.Vector2f;

public class Cbd {
    public final static BigDecimal TWO = new BigDecimal(2);
    public final static BigDecimal BDPI = new BigDecimal("3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196442881097566593344612847564823378678316527120190914564856692346034861045432664821339360726024914127372458700660631558817488152092096282925409171536436789259036001133053054882046652138414695194151160943305727036575959195309218611738193261179310511854807446237996274956735188575272489122793818301194912983367336244065664308602139494639522473719070217986094370277053921717629317675238467481846766940513200056812714526356082778577134275778960917363717872146844090122495343014654958537105079227968925892354201995611212902196086403441815981362977477130996051870721134999999837297804995105973173281609631859502445945534690830264252230825334468503526193118817101000313783875288658753320838142061717766914730359825349042875546873115956286388235378759375195778185778053217122680661300192787661119590921642019893809525720106548586327886593615338182796823030195203530185296899577362259941389124972177528347913151557485724245415069595082953311686172785588907509838175463746493931925506040092770167113900984882401285836160356370766010471018194295559619894676783744944825537977472684710404753464620804668425906949129331367702898915210475216205696602405803815019351125338243003558764024749647326391419927260426992279678235478163600934172164121992458631503028618297455570674983850549458858692699569092721079750930295532116534498720275596023648066549911988183479775356636980742654252786255181841757467289097777279380008164706001614524919217321721477235014144197356854816136115735255213347574184946843852332390739414333454776241686251898356948556209921922218427255025425688767179049460165346680498862723279178608578438382796797668145410095388378636095068006422512520511739298489608412848862694560424196528502221066118630674427862203919494504712371378696095636437191728746776465757396241389086583264599581339047802759009946576407895126946839835259570982582262052248940772671947826848260147699090264013639443745530506820349625245174939965143142980919065925093722169646151570985838741059788595977297549893016175392846813826868386894277415599185592524595395943104997252468084598727364469584865383673622262609912460805124388439045124413654976278079771569143599770012961608944169486855584840635342207222582848864815845602850601684273945226746767889525213852254995466672782398645659611635488623057745649803559363456817432411251507606947945109659609402522887971089314566913686722874894056010150330861792868092087476091782493858900971490967598526136554978189312978482168299894872265880485756401427047755513237964145152374623436454285844479526586782105114135473573952311342716610213596953623144295248493718711014576540359027993440374200731057853906219838744780847848968332144571386875194350643021845319104848100537061468067491927819119793995206141966342875444064374512371819217999839101591956181467514269123974894090718649423196156794520809514655022523160388193014209376213785595663893778708303906979207734672218256259966150142150306803844773454920260541466592520149744285073251866600213243408819071048633173464965145390579626856100550810665879699816357473638405257145910289706414011097120628043903975951567715770042033786993600723055876317635942187312514712053292819182618612586732157919841484882916447060957527069572209175671167229109816909152801735067127485832228718352093539657251210835791513698820914442100675103346711031412671113699086585163983150197016515116851714376576183515565088490998985998238734552833163550764791853589322618548963213293308985706420467525907091548141654985946163718027098199430992448895757128289059232332609729971208443357326548938239119325974636673058360414281388303203824903758985243744170291327656180937734440307074692112019130203303801976211011004492932151608424448596376698389522868478312355265821314495768572624334418930396864262434107732269780280731891544110104468232527162010526522721116603966655730925471105578537634668206531098965269186205647693125705863566201855810072936065987648611791045334885034611365768675324944166803962657978771855608455296541266540853061434443185867697514566140680070023787765913440171274947042056223053899456131407112700040785473326993908145466464588079727082668306343285878569830523580893306575740679545716377525420211495576158140025012622859413021647155097925923099079654737612551765675135751782966645477917450112996148903046399471329621073404375189573596145890193897131117904297828564750320319869151402870808599048010941214722131794764777262241425485454033215718530614228813758504306332175182979866223717215916077166925474873898665494945011465406284336639379003976926567214638530673609657120918076383271664162748888007869256029022847210403172118608204190004229661711963779213375751149595015660496318629472654736425230817703675159067350235072835405670403867435136222247715891504953098444893330963408780769325993978054193414473774418426312986080998886874132604721569516239658645730216315981931951673538129741677294786724229246543668009806769282382806899640048243540370141631496589794092432378969070697794223625082216889573837986230015937764716512289357860158816175578297352334460428151262720373431465319777741603199066554187639792933441952154134189948544473456738316249934191318148092777710386387734317720754565453220777092120190516609628049092636019759882816133231666365286193266863360627356763035447762803504507772355471058595487027908143562401451718062464362679456127531813407833033625423278394497538243720583531147711992606381334677687969597030983391307710987040859133746414428227726346594704745878477872019277152807317679077071572134447306057007334924369311383504931631284042512192565179806941135280131470130478164378851852909285452011658393419656213491434159562586586557055269049652098580338507224264829397285847831630577775606888764462482468579260395352773480304802900587607582510474709164396136267604492562742042083208566119062545433721315359584506877246029016187667952406163425225771954291629919306455377991403734043287526288896399587947572917464263574552540790914513571113694109119393251910760208252026187985318877058429725916778131496990090192116971737278476847268608490033770242429165130050051683233643503895170298939223345172201381280696501178440874519601212285993716231301711444846409038906449544400619869075485160263275052983491874");
    public final static BigDecimal BDTWOPI = BDPI.multiply(TWO);
    public final static BigDecimal BDHALFPI = BDPI.divide(TWO);
    public final static BigDecimal BDQUARTERPI = BDHALFPI.divide(TWO);
    public static MathContext context = new MathContext(100, RoundingMode.HALF_EVEN);
    
    public BigDecimal x;
    public BigDecimal y;

    public Cbd(BigDecimal x, BigDecimal y) {
        this.x = x;
        this.y = y;
    }
    
    public Cbd(float x, float y){
        this.x = new BigDecimal(x);
        this.y = new BigDecimal(y);
    }
    
    public Cbd(double x, double y){
        this.x = new BigDecimal(x);
        this.y = new BigDecimal(y);
    }
    
    public Cbd(){
        this.x = new BigDecimal(0);
        this.y = new BigDecimal(0);
    }
    
    public Cbd(Cf z){
        this(z.x, z.y);
    }
    
    public Cbd(Vector2f vec){
        this(vec.x, vec.y);
    }
    
    public Cbd(BigDecimal re){
        this(re,new BigDecimal(0));
    }
    
    public Cbd(double re){
        this(new BigDecimal(re));
    }
    
    public static Cbd geom(BigDecimal norm, BigDecimal angle){
        BigDecimal cos = cos(angle);
        BigDecimal sin = sin(angle);
        return new Cbd(cos.multiply(norm), sin.multiply(norm));
    }
    
    public static Cbd geom(BigDecimal norm, double angle){
        BigDecimal cos = new BigDecimal(Math.cos(angle));
        BigDecimal sin = new BigDecimal(Math.sin(angle));
        return new Cbd(cos.multiply(norm), sin.multiply(norm));
    }
    
    public BigDecimal normSq(){
        return x.pow(2).add(y.pow(2));
    }
    
    public BigDecimal norm(){
        return sqrt(normSq());
    }
    
    public BigDecimal argBD(){
        BigDecimal norm = norm();
        BigDecimal nx = x.divide(norm, context);
        BigDecimal ny = y.divide(norm, context);
        BigDecimal acos = arccos(nx);
        BigDecimal asin = arcsin(ny);
        BigDecimal angle = (asin.compareTo(BigDecimal.ZERO) > 0 ? acos : BDTWOPI.subtract(acos));
        return angle;
    }
    
    public double arg(){
        BigDecimal norm = norm();
        double nx = x.divide(norm, context).doubleValue();
        double ny = y.divide(norm, context).doubleValue();
        double acos = Math.acos(nx);
        double asin = Math.asin(ny);
        double angle = (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public Cbd normalized(){
        BigDecimal norm = norm();
        return new Cbd(x.divide(norm, context), y.divide(norm, context));
    }
    
    public Cbd add(Cbd a){
        return new Cbd(x.add(a.x), y.add(a.y));
    }
    
    public Cbd add(BigDecimal b){
        return new Cbd(x.add(b), y);
    }
    
    public Cbd mul(Cbd a){
        return new Cbd(x.multiply(a.x).subtract(y.multiply(a.y)),
            x.multiply(a.y).add(y.multiply(a.x)));
    }
    
    public Cbd mul(BigDecimal k){
        return new Cbd(x.multiply(k), y.multiply(k));
    }
    
    public Cbd neg(){
        return new Cbd(x.negate(), y.negate());
    }
    
    public Cbd conj(){
        return new Cbd(x, y.negate());
    }
    
    public Cbd div(BigDecimal divisor){
        return (mul(BigDecimal.ONE.divide(divisor, context)));
    }
    
    public Cbd div(Cbd divisor){
        return (mul(divisor.conj().mul(BigDecimal.ONE.divide(divisor.normSq(), context))));
    }
    
    public BigDecimal real(){
        if(y.abs().compareTo(new BigDecimal(0.000001)) < 0){
            return x;
        } else {
            System.err.println("Imaginary part: "+y);
            throw new ArithmeticException("Not a real number");
        }
    }
    
    public BigDecimal Re(){
        return x;
    }
    
    public BigDecimal Im(){
        return y;
    }
    
    public Cbd[] sqrt(){
        BigDecimal norm = norm();
        BigDecimal aarg = norm.add(x);
        BigDecimal a = (aarg.compareTo(BigDecimal.ZERO)>0)? sqrt(aarg.divide(TWO, context)):BigDecimal.ZERO;
        BigDecimal barg = norm.subtract(x);
        BigDecimal b = (barg.compareTo(BigDecimal.ZERO)>0)? sqrt(barg.divide(TWO, context)):BigDecimal.ZERO;
        if(y.compareTo(BigDecimal.ZERO)>0){
            return new Cbd[]{
                new Cbd(a,b),
                new Cbd(a.negate(),b.negate())
            };
        } else {
            return new Cbd[]{
                new Cbd(a.negate(),b),
                new Cbd(a,b.negate())
            };
        }        
    }
    
    public Cbd[] nthrt(int n){
        BigDecimal norm = norm();
        BigDecimal arg = argBD();
        Cbd[] res = new Cbd[n];
        BigDecimal newnorm =  nthRoot(norm, n);
        for(int k = 0; k < n ;k++){
            res[k] = geom(newnorm, ((arg.add(new BigDecimal(2*k).multiply(BDPI, context), context)).divide(new BigDecimal(n), context)));
        }
        return res;
    }
    
    public Cbd pow(int exp){
        if(exp < 8){
            Cbd temp = new Cbd(1,0);
            for(int i = 0;i<exp;i++){
                temp = this.mul(temp);
            }
            return temp;
        } else {
            BigDecimal arg = argBD();
            BigDecimal norm = norm();
            return geom(norm.pow(exp),arg.multiply(new BigDecimal(exp)));
        }
    }
    
    @Override
    public String toString(){
        return x + " + " + y +"i";
    }
    
    /**
    * caution with large numbers, takes longer the larger a is.
    */
    public static BigDecimal exp(BigDecimal a){
        BigDecimal val = BigDecimal.ZERO;
        BigDecimal old = BigDecimal.ZERO;
        BigDecimal pow = BigDecimal.ONE;
        BigDecimal factorial = BigDecimal.ONE;
        val = val.add(pow.divide(factorial, context), context); // term = 0
        for(int term =1 ; val.compareTo(old) != 0 ;term++){
            old = val;
            pow = pow.multiply(a, context);
            factorial = factorial.multiply(new BigDecimal(term), context);
            val = val.add(pow.divide(factorial, context), context);
            System.out.println(val);
        }
        return val;
    }
    
    
    /**
    * radius of convergence only 1
    */
    public static BigDecimal arctan(BigDecimal a){
        int precision = 51;
        BigDecimal val = BigDecimal.ZERO;
        for(int term = 0; term < precision;term++){
            val = val.add(a.pow(2*term+1).multiply(new BigDecimal((0.5f-term%2)*2)).divide(new BigDecimal(2*term+1), context), context);
        }
        return val;
    }
    
    public static BigDecimal arccos(BigDecimal a){
        return BDHALFPI.subtract(arcsin(a), context);
    }
    
    /**
     * Clamps input between -1 and 1
     */
    public static BigDecimal arcsin(BigDecimal a){
        BigDecimal SRT2 = new BigDecimal("0.707106781");
        BigDecimal sign = BigDecimal.ONE;
        BigDecimal s = a.abs().compareTo(BigDecimal.ONE) <= 0 ?
                a : (a.signum() == 1 ? BigDecimal.ONE : BigDecimal.ONE.negate());
        boolean reversed = false;
        if(s.compareTo(SRT2) > 0){
            s = sqrt(BigDecimal.ONE.subtract(s.pow(2)));
            reversed = true;
        } else if(s.compareTo(SRT2.negate()) < 0){
            s = sqrt(BigDecimal.ONE.subtract(s.pow(2)));
            reversed = true;
            sign = sign.negate();
        }
        BigDecimal L4 = new BigDecimal(4);
        BigDecimal val = BigDecimal.ZERO;
        BigDecimal old = new BigDecimal(100);
        BigDecimal exp4 = BigDecimal.ONE;
        BigDecimal comb = BigDecimal.ONE;
        BigDecimal pow = s;
        val = val.add(comb.multiply(pow).divide(exp4.multiply(new BigDecimal(1)), context));
        for(int term = 1; val.compareTo(old) != 0 ;term++){
            old = val;
            exp4 = exp4.multiply(L4, context);
            pow = pow.multiply(s, context).multiply(s, context);
            comb = comb.multiply(new BigDecimal(2*term-1).multiply(new BigDecimal(2*term), context), context)
                       .divide(new BigDecimal(term).pow(2, context), context);
            val = val.add(comb.multiply(pow, context).divide(exp4.multiply(new BigDecimal(2*term+1), context), context), context);
        }
        return reversed ? BDHALFPI.subtract(val, context).multiply(sign) : val;
    }
    
    public static BigDecimal sin(BigDecimal a){
        //trim
        BigDecimal padd = a.divide(BDTWOPI, context);
        BigDecimal s = padd.subtract(padd.setScale(0, RoundingMode.FLOOR));
        s = s.multiply(BDTWOPI);
        //sum Taylor
        BigDecimal val = BigDecimal.ZERO;
        BigDecimal old = new BigDecimal(100);//invalid initial state to not pass comparison
        BigDecimal pow = s;
        BigDecimal factorial = BigDecimal.ONE;
        val = val.add(pow/* *1/1 */);// for term = 0
        for(int term = 1; val.compareTo(old) != 0 ;term++){
            old = val;
            pow = pow.multiply(s, context).multiply(s, context).negate();
            factorial = factorial.multiply(new BigDecimal(2*term), context).multiply(new BigDecimal(2*term +1), context);
            val = val.add(pow.divide(factorial, context), context);
        }
        return val;
    }
    
    public static BigDecimal cos(BigDecimal a){
        //trim
        BigDecimal padd = a.divide(BDTWOPI, context);
        BigDecimal s = padd.subtract(padd.setScale(0, RoundingMode.FLOOR));
        s = s.multiply(BDTWOPI);
        //sum Taylor
        BigDecimal val = BigDecimal.ZERO;
        BigDecimal old = new BigDecimal(100);//invalid initial state to not pass comparison
        BigDecimal pow = BigDecimal.ONE;
        BigDecimal factorial = BigDecimal.ONE;
        val = val.add(pow/* *1/1 */);// for term = 0        
        for(int term = 1; val.compareTo(old) != 0 ;term++){
            old = val;
            pow = pow.multiply(s, context).multiply(s, context).negate();
            factorial = factorial.multiply(new BigDecimal(2*term-1), context).multiply(new BigDecimal(2*term), context);
            val = val.add(pow.divide(factorial, context),context);
        }
        return val;
    }
    
    public static BigDecimal factorial(int arg){
        if(arg == 0)
            return BigDecimal.ONE;
        BigDecimal fact = BigDecimal.ONE;
        for(int i = 2; i <= arg;i++){
            fact = fact.multiply(new BigDecimal(i));
        }
        return fact;
    }
    
    //wrapers for imported methods//////////////////////////////////////////////
    public static BigDecimal sqrt(BigDecimal b){
        BigDecimal ret = null;
        try{
            ret = jdk11BigDecimal.sqrt(b, context);
        } catch(ArithmeticException ex){
            System.err.println("Sqrt argument = "+b);
            throw ex;
        }
        return ret;
    }
    
    public static BigDecimal nthRoot(BigDecimal b, int n){
        return Imported.nthRoot(n, b);
    }
}
