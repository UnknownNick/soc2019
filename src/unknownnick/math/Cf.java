package unknownnick.math;

import static java.lang.Math.PI;
import static java.lang.Math.acos;
import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import org.lwjgl.util.vector.Vector2f;
import unknownnick.engine.util.math.Prints;

public class Cf {
    public float x;
    public float y;

    public Cf(float x, float y) {
        this.x = x;
        this.y = y;
    }
    
    public Cf(){
        this(0,0);
    }
    
    public Cf(float re){
        this(re,0);
    }
    
    public Cf(Vector2f vec){
        x = vec.x;
        y = vec.y;
    }
    
    public static Cf geom(float norm, float angle){
        return new Cf((float)(norm*cos(angle)), (float)(norm*sin(angle)));
    }
    
    public static Cf geom(float norm, double angle){
        return new Cf((float)(norm*cos(angle)), (float)(norm*sin(angle)));
    }
    
    public float normSq(){
        return x*x+y*y;
    }
    
    public float norm(){
        return (float) Math.sqrt(normSq());
    }
    
    public float arg(){
        float norm = norm();
        float nx = x/norm;
        float ny = y/norm;
        float acos = (float) acos(nx);
        float asin = (float) asin(ny);
        float angle = (float) (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public double argFP64(){
        float norm = norm();
        double nx = x/(double)norm;
        double ny = y/(double)norm;
        double acos =  acos(nx);
        double asin =  asin(ny);
        double angle =  (asin > 0 ? acos : 2*PI-acos);
        return angle;
    }
    
    public Cf normalized(){
        float norm = norm();
        return new Cf(x/norm, y/norm);
    }
    
    public static Cf sum(Cf a, Cf b){
        return new Cf(a.x + b.x, a.y+b.y);
    }
    
    public Cf add(Cf a){
        return sum(this, a);
    }
    
    public Cf add(float re){
        return new Cf(x+re,y);
    }
    
    public static Cf mul(Cf a, Cf b){
        return new Cf(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x);
    }
    
    public Cf neg(){
        return new Cf(-x,-y);
    }
    
    public Cf conj(){
        return new Cf(x, -y);
    }
    
    public Cf div(float divisor){
        return new Cf(x/divisor, y/divisor);
    }
    
    public Cf div(Cf divisor){
        return (this.mul(divisor.conj()).mul(1f/divisor.normSq()));
    }
    
    public Cf mul(Cf a){
        return mul(this, a);
    }
    
    public Cf mul(float k){
        return new Cf(k*x, k*y);
    }
    
    public static Cf mul(Cf z, float k){
        return z.mul(k);
    }
    
    public Cf[] sqrt(){
        float norm = norm();
        float a = (float) Math.sqrt((norm+x)/2);
        float b = (float) Math.sqrt((norm-x)/2);
        if(y>0){
            return new Cf[]{
                new Cf(a,b),
                new Cf(-a,-b)
            };
        } else {
            return new Cf[]{
                new Cf(-a,b),
                new Cf(a,-b)
            };
        }
    }
    
    public Cf[] nthrt(int n){
        float norm = norm();
        double arg = argFP64();
        Cf[] res = new Cf[n];
        float newnorm = (float) Math.pow(norm, 1f/n);
        for(int k = 0; k < n ;k++){
            res[k] = geom(newnorm, ((arg+
                    2*k*PI)/n));
        }
        return res;
    }
    
    public Cf pow(int exp){
        if(exp < 8){
            Cf temp = new Cf(1,0);
            for(int i = 0;i<exp;i++){
                temp = this.mul(temp);
            }
            return temp;
        } else {
            float arg = arg();
            float norm = norm();
            return geom((float) Math.pow(norm, exp),exp*arg);
        }
    }
    
    public float Re(){
        return x;
    }
    
    public float Im(){
        return y;
    }
    
    public static float Re(Cf z){
        return z.x;
    }
    
    public static float Im(Cf z){
        return z.y;
    }
    
    @Override
    public String toString(){
        return x + " + " + y +"i";
    }
    
    public String debugMethodsOtput(){
        String s = "";
        s+= "x:" +x+"\n";
        s+= "y:" +y+"\n";
        s+= "Im()" +Im()+"\n";
        s+= "Re()" +Re()+"\n";
        s+= "add(new Cf(1,2))" +add(new Cf(1,2))+"\n";
        s+= "add(1)" +add(1)+"\n";
        s+= "arg()" +arg()+"\n";
        s+= "conj()" +conj()+"\n";
        s+= "div(new Cf(1, 2))" +div(new Cf(1, 2))+"\n";
        s+= "geom(2, PI/3):"+geom(2, (float) (PI/3))+"\n";
        s+= "mul(new Cf(1,2))" +mul(new Cf(1,2))+"\n";
        s+= "mul(2)" +mul(2)+"\n";
        s+= "neg()" +neg()+"\n";
        s+= "norm()" +norm()+"\n";
        s+= "normSq()" +normSq()+"\n";
        s+= "normalized()" +normalized()+"\n";
        Cf[] nt = nthrt(3);
        s+= "nthrt(3)" +nt[0]+", "+nt[1]+", "+nt[2]+"\n";
        s+= "pow(2)" +pow(2)+"\n";
        s+= "pow(30)" +pow(30)+"\n";
        nt = sqrt();
        s+= "sqrt()" +nt[0]+", "+nt[1];
        return s;
    }
}
