package unknownnick.math;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import org.lwjgl.util.vector.Vector3f;
import static unknownnick.math.Hf.mul;
import static unknownnick.math.Hf.mul;

public class QuaternionRotation {
    private Vector3f axis;
    private float doubleangle;
    
    private Hf Q;
    private Hf Q_;
    
    /*Cached values
    *Suppose the rotation quaternion Q = q0 + q(vector) =
    *   cos(phi/2) + sin(phi/2)*u , where u is normalised axis vector
    *Then the rotation R_Q(vector v)=Q*v*Q_ can be rewriten as:
    *    (q0^2-|q|^2)*v+2*(q dot v)*q+2*q0*(q cross v)
    *for efficient calculation, it is beneficial to substitute, precalculate
    *and cache constant values, so here we use the form:
    *   a*v+(q dot v)*b+c*(q cross v)
    *where a = (q0^2-|q|^2) ; real
    *      b = 2*q          ; vector R^3
    *      c = 2*q0         ; real
    */
    float a;
    float b1, b2, b3;
    float c;
    
    float q1;
    float q2;
    float q3;
    
    public QuaternionRotation(Hf rot){
        Q = rot;
        Q_ = Q.conjugate();
        axis = rot.getVector();
        doubleangle = 2*rot.arg();
        updateCache();
    }

    public QuaternionRotation(Vector3f axis, float doubleangle) {
        this.axis = axis;
        this.doubleangle = doubleangle;
        Q = QfromAxisAngle(axis, this.doubleangle);
        Q_ = Q.conjugate();
        updateCache();
    }
    
    private void updateCache(){
        float q0 = Q.s;
        q1 = Q.x;
        q2 = Q.y;
        q3 = Q.z;
        
        a = q0*q0 - q1*q1 - q2*q2 - q3*q3;
        b1 = 2*q1;
        b2 = 2*q2;
        b3 = 2*q3;
        c  = 2*q0;
    }
    
    public Vector3f rotateByMul(Vector3f v){
        Hf V = new Hf(0,v);
        Hf ret = mul(mul(Q,V), Q_);
        return ret.getVector();        
    }
    
    public Vector3f rotate(Vector3f v){
        float v1 = v.x;
        float v2 = v.y;
        float v3 = v.z;
        
        float q_dot_v = q1*v1+q2*v2+q3*v3;
        
        float x = a*v1 + q_dot_v*b1 + c*(q2*v3-q3*v2);
        float y = a*v2 + q_dot_v*b2 + c*(q3*v1-q1*v3);
        float z = a*v3 + q_dot_v*b3 + c*(q1*v2-q2*v1);
        
        return new Vector3f(x, y, z);
    }
    
    public static Vector3f rotateByMul(Vector3f v, float angle, Vector3f axis){
        Hf V = new Hf(0,v);
        Hf Q = new Hf(1,angle/2,axis);
        Hf ret = mul(mul(Q,V),Q.conjugate());
        return ret.getVector();
    }
    
    public Hf QfromAxisAngle(Vector3f axis, float doubleangle){
        return new Hf((float)cos(doubleangle/2),(Vector3f)axis.normalise(null).scale((float) sin(doubleangle/2)));
    }
    
    private void update(){
        Q = QfromAxisAngle(axis, doubleangle);
        Q_ = Q.conjugate();
        updateCache();
    }

    public Vector3f getAxis() {
        return axis;
    }

    public void setAxis(Vector3f axis) {
        this.axis = axis;
        update();
    }

    public float getAngle() {
        return doubleangle;
    }

    public void setAngle(float angle) {
        this.doubleangle = angle;
        update();
    }

    public Hf getQ() {
        return Q;
    }
    
    @Override
    public String toString(){
        return doubleangle + "rad around:" + "("+ axis.x + ", " + axis.y + ", " + axis.z +")";
    }
    
    public String toString(boolean normalize){
        Vector3f ax = this.axis.normalise(null);
        return doubleangle + "rad around:" + "("+ ax.x + ", " + ax.y + ", " + ax.z +")";
    }
}
