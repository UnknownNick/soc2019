package unknownnick.math;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.tan;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import unknownnick.engine.util.math.Maths;
import unknownnick.engine.util.math.Prints;

public class PointData {
    private final List<Vector3f> points = new ArrayList<>();
    private Vector3f centroid;
    
    public final static int PRECISIONPhi = 10;
    public final static int PRECISIONAlpha = 40;
    public final static int PRECISIONRho = PRECISIONAlpha;
    public final static float DPhi = (float) (PI/(PRECISIONPhi*2));
    public final static float DAlpha = (float) (2*PI/PRECISIONAlpha);
    public final static float DRho = (float) (2*PI/PRECISIONRho);
    public final static int MAXITERATIONS = 23;
    
    public static final float HALFPI = (float) PI/2;
    public static final float DOUBLEPI = (float) PI*2;
    
    public PointData(List<Vector3f> p){
        for(Vector3f point : p){
            points.add(new Vector3f(point));
        }
        this.centralize();
    }
    
    public PointData(List<Vector3f> p, boolean flag){
        for(Vector3f point : p){
            points.add(new Vector3f(point));
        }
        if(flag)
            this.centralize();
    }
    
    public static Vector3f getCentroidDiff(PointData src, PointData dest){
        return Maths.sum(dest.getCentroid(), src.getCentroid().negate(null));
    }
    
    public static Transformation findMinimumNAIVE(PointData src, PointData dest){
        return findMinimumNAIVE(src, dest, PRECISIONPhi, PRECISIONAlpha);
    }
    
    public static Transformation findMinimumNAIVE(PointData src, PointData dest, int precPhi, int precAlpha){
        if(dest.points.size() != src.points.size()) {
            System.err.println("Set sizes differ!");
        }
        long time = System.currentTimeMillis();
        float DPhi = (float) (PI/(precPhi*2));
        float DAlpha = (float) (2*PI/precAlpha);
        float DRho = (float) (2*PI/precAlpha);
        
        QuaternionRotation min = new QuaternionRotation(new Vector3f(1,0,0),0);
        float minVal = (src.clone().rotate(min)).compareAllSqLen(dest);
        
        for(float phi = 0; phi < HALFPI ; phi += DPhi){
            for(float alpha = 0; alpha < DOUBLEPI; alpha += DAlpha){
                Vector3f axis = getPointOnTopUnitSphere(phi, alpha);
                for(float rho = 0; rho < DOUBLEPI ;rho += DRho){
                    QuaternionRotation rot = new QuaternionRotation(axis, rho);
                    float val = (src.clone().rotate(rot)).compareAllSqLen(dest);
                    if(val < minVal){
                        min = rot;
                        minVal = val;
                    }
                }
            }
        }
        Transformation res = new Transformation();
        res.rotation = min;
        res.translation = Maths.sum(dest.getCentroid(), min.rotate(src.getCentroid()).negate(null));
        System.out.println(min);
        System.out.println("translated: " + Prints.printVector(res.translation));
        System.out.println("took " + (System.currentTimeMillis()-time) + "ms" );
        System.out.println();
        return res;
    }
    
    public static Transformation findMinimum(PointData src, PointData dest){
        return findMinimum(src, dest, MAXITERATIONS, PRECISIONPhi, PRECISIONAlpha);
    }
    
    public static Transformation findMinimum(PointData src, PointData dest, int iterations, int precPhi, int precAlpha){
        if(dest.points.size() != src.points.size()) {
            System.err.println("Set sizes differ!");
        }
        long time = System.currentTimeMillis();
        float DPhi = (float) (PI/(precPhi*2));
        float DAlpha = (float) (2*PI/precAlpha);
        float DRho = (float) (2*PI/precAlpha);
        
        QuaternionRotation min = new QuaternionRotation(new Vector3f(1,0,0),0);
        float minVal = (src.clone().rotate(min)).compareAllSqLen(dest);
        
        for(float phi = 0; phi < HALFPI ; phi += DPhi){
            for(float alpha = 0; alpha < DOUBLEPI; alpha += DAlpha){
                Vector3f axis = getPointOnTopUnitSphere(phi, alpha);
                for(float rho = 0; rho < DOUBLEPI ;rho += DRho){
                    QuaternionRotation rot = new QuaternionRotation(axis, rho);
                    float val = (src.clone().rotate(rot)).compareAllSqLen(dest);
                    if(val < minVal){
                        min = rot;
                        minVal = val;
                    }
                }
            }
        }
//        System.out.println(min);
        float sindA = (float) sin(DAlpha);
        float tgdF = (float) tan(DPhi);
        float range = (float) (sqrt(sindA*sindA+tgdF*tgdF)/(2*cos(DAlpha)));
        
        top:
        for(int it = 0; it <= iterations; it++){
            QuaternionRotation newMin = min;
            Vector3f minaxis = min.getAxis();
            float rhoStep = DRho/(1L<<it);
            float rhoRange =  0.5f*precAlpha*rhoStep;
            float rhoStop = min.getAngle()+rhoRange;
            float newVal = minVal;
            Vector3f tang = getTangentVector(minaxis, range/(1L<<it));
            tang = (Vector3f) Maths.sum(tang, minaxis).normalise();
            
            for(float beta = 0; beta < DOUBLEPI ;beta += DAlpha) {
                QuaternionRotation shift = new QuaternionRotation(minaxis, beta);
                Vector3f newaxis = shift.rotate(tang);
                
                for(float rho = min.getAngle()-rhoRange; rho < rhoStop ; rho += rhoStep) {
                    if(Float.compare(rho, rho+rhoStep) == 0){
                        System.out.println("precision underflow " + it);
                        break top;                        
                    }
                    QuaternionRotation rot = new QuaternionRotation(newaxis, rho);
                    float val = (src.clone().rotate(rot)).compareAllSqLen(dest);
                    if(val < newVal){
                        newMin = rot;
                        newVal = val;
//                        System.out.println("precision increased " + it);
//                        System.out.println(newMin);
                    }
                }
            }
            min = newMin;
            minVal = newVal;
        }
        Transformation res = new Transformation();
        res.rotation = min;
        res.translation = Maths.sum(dest.getCentroid(), min.rotate(src.getCentroid()).negate(null));
        System.out.println(min);
        System.out.println("translated: " + Prints.printVector(res.translation));
        System.out.println("took " + (System.currentTimeMillis()-time) + "ms" );
        System.out.println();
        return res;
    }
    
    public static Vector3f getTangentVector(Vector3f axis, float norm){
        Vector3f A = new Vector3f(2,0,0);
        Vector3f B = new Vector3f(0,0,2);
        Vector3f negaxis = axis.negate(null);
        Vector3f a = Maths.sum(A, negaxis);
        Vector3f b = Maths.sum(B, negaxis);
        float angleA = Vector3f.angle(a, axis);
        float angleB = Vector3f.angle(b, axis);
        Vector3f tang;
        if(angleA > angleB){
            tang = Vector3f.cross(axis, a, null);
        } else {
            tang = Vector3f.cross(axis, b, null);            
        }
        tang.normalise().scale(norm);
        return tang;
    }
    
    public static class Transformation {
        public QuaternionRotation rotation;
        public Vector3f translation;
        @Override
        public String toString(){
            return "rotated: " + rotation.toString(true) + ", translated: " + Prints.printVector(translation);
        }
    }
    
    private static Vector3f getPointOnTopUnitSphere(float att, float lon){
        float cosA = (float) cos(att);
        float sinA = (float) sin(att);
        float cosL = (float) cos(lon);
        float sinL = (float) sin(lon);
        return new Vector3f(cosA*cosL, sinA, sinL*cosA);
    }
    
    public int pointCount(){
        return points.size();
    }
    
    public List<Vector3f> getPoints(){
        return points;
    }

    public Vector3f getCentroid() {
        return centroid;
    }
        
    public float compareAllSqLen(PointData other){
        float sum = 0;
        for(int i = 0; i < points.size() ;i++){
            Vector3f v1 = points.get(i);
            Vector3f v2 = other.points.get(i);
            sum += Maths.sum(v1, v2.negate(null)).lengthSquared();
        }
        return sum;
    }
    
    public PointData rotate(QuaternionRotation rot){
        for(Vector3f point : points){
            point.set(rot.rotateByMul(point));
        }
        return this;
    }
    
    @Override
    public PointData clone(){
        PointData newer = new PointData(points, false);
        newer.centroid = new Vector3f(this.centroid);
        return newer;
    }
    
    public PointData uncentralize(){
        for(Vector3f point : points){
            point.set(Maths.sum(point, centroid));
        }
        centroid = new Vector3f(0,0,0);
        return this;
    }
    
    public void centralize(){
        this.getNewCentroid();
        for(Vector3f point : points){
            point.set(Maths.sum(point, centroid.negate(null)));
        }
    }
    
    public Vector3f getNewCentroid(){
        double x = 0, y = 0, z = 0;
        int count = 0;
        for(Vector3f point : points){
            x += point.x;
            y += point.y;
            z += point.z;
            count++;
        }
        x /= count;
        y /= count;
        z /= count;
        return centroid = new Vector3f((float)x, (float)y, (float)z);
    }
    
    //reference calculation methods/////////////////////////////////////////////
    public static Matrix4f sourceVecToMat(Vector3f p){
        Matrix4f P = new Matrix4f();
        /*  0  -p'1  -p'2  -p'3
        *  p'1   0    p'3  -p'2
        *  p'2 -p'3    0    p'1
        *  p'3  p'2  -p'1    0
        */
        P.m00 =  0;  P.m01 = -p.x;  P.m02 = -p.y;  P.m03 = -p.z;
        P.m10 = p.x; P.m11 =   0;   P.m12 =  p.z;  P.m13 = -p.y;
        P.m20 = p.y; P.m21 = -p.z;  P.m22 =   0;   P.m23 =  p.x;
        P.m30 = p.z; P.m31 =  p.y;  P.m32 = -p.x;  P.m33 = 0;
        return P;
    }
    
    public static Matrix4f destVecToMat(Vector3f q){
        Matrix4f Q = new Matrix4f();
        /*  0  -q'1  -q'2  -q'3
        *  q'1   0   -q'3   q'2
        *  q'2  q'3    0   -q'1
        *  q'3 -q'2   q'1    0
        */
        Q.m00 =  0;  Q.m01 = -q.x;  Q.m02 = -q.y;  Q.m03 = -q.z;
        Q.m10 = q.x; Q.m11 =   0;   Q.m12 = -q.z;  Q.m13 =  q.y;
        Q.m20 = q.y; Q.m21 =  q.z;  Q.m22 =   0;   Q.m23 = -q.x;
        Q.m30 = q.z; Q.m31 = -q.y;  Q.m32 =  q.x;  Q.m33 = 0;
        return Q;
    }
    
    /*
     * from https://en.wikipedia.org/wiki/Quartic_function#Solution_methods
     */
    //verified ok
    public static float[] solveQuartic(float a4, float a3, float a2, float a1, float a0){
        //reduce to depressed quartic
        float b = a3/a4;
        float c = a2/a4;
        float d = a1/a4;
        float e = a0/a4;
        
        float p = (8*c-3*b*b)/8f;
        float q = (b*b*b -4*b*c+8*d)/8f;
        float r = (-3*b*b*b*b+256*e-64*b*d+16*b*b*c)/256f;
        
        ////////////////////////////////////////////////////////////////////////
        Cf m = solveCubic(8, 8*p, 2*p*p-8*r, -q*q)[0];
        System.out.println();
        System.out.println("Quartic :"+ a4+", "+a3+", "+a2+", "+a1+", "+a0);
        System.out.println("m: "+m);
        Cf imsqrt = new Cf(1).div(m.sqrt()[0]);
        Cf first = imsqrt.mul((float)sqrt(2)*q).add(m.mul(2)).add(2*p).neg().sqrt()[0];//sqrt(-(2*p+2*m+(sqrt(2)*q)/(sqrt(m))));
        Cf second =imsqrt.mul((float)sqrt(2)*q).neg().add(m.mul(2)).add(2*p).neg().sqrt()[0]; //sqrt(-(2*p+2*m-(sqrt(2)*q)/(sqrt(m))));
        Cf y1 = m.mul(2).sqrt()[0].add(first).div(2);//(sqrt(2*m)+first)/2;
        Cf y2 = m.mul(2).sqrt()[0].add(first.neg()).div(2);//(sqrt(2*m)-first)/2;
        Cf y3 = m.mul(2).sqrt()[0].neg().add(second).div(2);//(-sqrt(2*m)+second)/2;
        Cf y4 = m.mul(2).sqrt()[0].neg().add(second.neg()).div(2);//(-sqrt(2*m)-second)/2;
        float diff = (float)b/4;
        float[] x = new float[4];
        x[0] = y1.Re() - diff;
        x[1] = y2.Re() - diff;
        x[2] = y3.Re() - diff;
        x[3] = y4.Re() - diff;
        return x;
    }
    
    public static double[] solveQuarticDouble(double a4, double a3, double a2, double a1, double a0){
        //reduce to depressed quartic
        double b = a3/a4;
        double c = a2/a4;
        double d = a1/a4;
        double e = a0/a4;
        
        double p = (8*c-3*b*b)/8f;
        double q = (b*b*b -4*b*c+8*d)/8f;
        double r = (-3*b*b*b*b+256*e-64*b*d+16*b*b*c)/256f;
        
        ////////////////////////////////////////////////////////////////////////
        Cd m = solveCubicDouble(8, 8*p, 2*p*p-8*r, -q*q)[0];
        System.out.println();
        System.out.println("Quartic :"+ a4+", "+a3+", "+a2+", "+a1+", "+a0);
        System.out.println("m: "+m);
        Cd imsqrt = new Cd(1).div(m.sqrt()[0]);
        Cd first = imsqrt.mul((double)sqrt(2)*q).add(m.mul(2)).add(2*p).neg().sqrt()[0];//sqrt(-(2*p+2*m+(sqrt(2)*q)/(sqrt(m))));
        Cd second =imsqrt.mul((double)sqrt(2)*q).neg().add(m.mul(2)).add(2*p).neg().sqrt()[0]; //sqrt(-(2*p+2*m-(sqrt(2)*q)/(sqrt(m))));
        Cd y1 = m.mul(2).sqrt()[0].add(first).div(2);//(sqrt(2*m)+first)/2;
        Cd y2 = m.mul(2).sqrt()[0].add(first.neg()).div(2);//(sqrt(2*m)-first)/2;
        Cd y3 = m.mul(2).sqrt()[0].neg().add(second).div(2);//(-sqrt(2*m)+second)/2;
        Cd y4 = m.mul(2).sqrt()[0].neg().add(second.neg()).div(2);//(-sqrt(2*m)-second)/2;
        double diff = (double)b/4;
        double[] x = new double[4];
        x[0] = y1.Re() - diff;
        x[1] = y2.Re() - diff;
        x[2] = y3.Re() - diff;
        x[3] = y4.Re() - diff;
        return x;
    }
    
    /*
     * from https://en.wikipedia.org/wiki/Cubic_function
     */
    //verified working ok
    public static Cf[] solveCubic(float a, float b, float c, float d){
        System.out.println();
        System.out.println("Cubic: " + a + ", "+b+", "+c+", "+d);
        float D0 = b*b - 3*a*c;
        float D1 = 2*b*b*b - 9*a*b*c + 27*a*a*d;
        System.out.println(D0 + ", " + D1);
        Cf s = new Cf(D1*D1-4*D0*D0*D0);
        System.out.println(s);
        Cf sr = s.sqrt()[0];
        System.out.println(sr);
        Cf sd = sr.neg().add(D1).mul(1f/2);
        System.out.println(sd);
        Cf[] C;
        if(D1>0){
            C = (new Cf(D1*D1-4*D0*D0*D0).sqrt()[0].add(D1).mul(1f/2)).nthrt(3);
        } else {
            C = (new Cf(D1*D1-4*D0*D0*D0).sqrt()[0].neg().add(D1).mul(1f/2)).nthrt(3);
        }
        Cf[] x = new Cf[3];
        float[] args = new float[3];
        for(int i = 0; i<3 ;i++){
            x[i] = C[i].add(new Cf((float) D0,0).div(C[i])).add((float)b).mul((float)(-1f/(3*a)));
            args[i] = x[i].arg();
            System.out.println(x[i]);
        }
        int min = (args[0]*args[0]>args[1]*args[1])?1:0;
        min = (args[min]*args[min]>args[2]*args[2])?2:min;
        return x;
    }
    
    public static Cd[] solveCubicDouble( double a,  double b,  double c,  double d){
        System.out.println();
        System.out.println("Cubic: " + a + ", "+b+", "+c+", "+d);
         double D0 = b*b - 3*a*c;
         double D1 = 2*b*b*b - 9*a*b*c + 27*a*a*d;
        System.out.println(D0 + ", " + D1);
        Cd s = new Cd(D1*D1-4*D0*D0*D0);
        System.out.println(s);
        Cd sr = s.sqrt()[0];
        System.out.println(sr);
        Cd sd = sr.neg().add(D1).mul(1f/2);
        System.out.println(sd);
        Cd[] C;
        if(D1>0){
            C = (new Cd(D1*D1-4*D0*D0*D0).sqrt()[0].add(D1).mul(1f/2)).nthrt(3);
        } else {
            C = (new Cd(D1*D1-4*D0*D0*D0).sqrt()[0].neg().add(D1).mul(1f/2)).nthrt(3);
        }
        Cd[] x = new Cd[3];
        for(int i = 0; i<3 ;i++){
            x[i] = C[i].add(new Cd(( double) D0,0).div(C[i])).add(( double)b).mul(( double)(-1f/(3*a)));
            System.out.println(x[i]);
        }
        return x;
    }
    
    //working ok
    public static float[] eigenvaluesOfSymmMat4(float a, float b, float c, float d,
            float e, float f, float g, float h, float i, float j){
        float a4 = 1;
        float a3 = -e-a-j-h;
        float a2 = -g*g +e*a +e*j +a*j +a*h +j*h +e*h
                   -f*f -i*i   
                   -b*b -d*d
                   -c*c;
        float a1 = a*g*g +g*g*h -e*a*j -e*a*h -e*j*h -a*j*h
                  +a*f*f +j*f*f +e*i*i +a*i*i -2*f*g*i
                  +b*b*j +b*b*h -2*b*d*g +e*d*d + h*d*d
                  +e*c*c +j*c*c -2*c*b*f-2*c*d*i;
        float a0 = -a*g*g*h +e*a*j*h
                   -a*j*f*f -e*a*i*i +2*a*f*g*i
                   -b*b*h*j +2*b*h*d*g -e*h*d*d
                   -e*j*c*c +2*j*c*b*f +2*e*c*d*i
                   +c*c*g*g -2*b*c*i*g -2*c*d*f*g +b*b*i*i -2*b*d*i*f +d*d*f*f;
        return solveQuartic(a4,a3,a2,a1,a0);
    }
    
    public static double[] eigenvaluesOfSymmMat4Double(double a, double b, double c, double d,
            double e, double f, double g, double h, double i, double j){
        double a4 = 1;
        double a3 = -e-a-j-h;
        double a2 = -g*g +e*a +e*j +a*j +a*h +j*h +e*h
                   -f*f -i*i   
                   -b*b -d*d
                   -c*c;
        double a1 = a*g*g +g*g*h -e*a*j -e*a*h -e*j*h -a*j*h
                  +a*f*f +j*f*f +e*i*i +a*i*i -2*f*g*i
                  +b*b*j +b*b*h -2*b*d*g +e*d*d + h*d*d
                  +e*c*c +j*c*c -2*c*b*f-2*c*d*i;
        double a0 = -a*g*g*h +e*a*j*h
                   -a*j*f*f -e*a*i*i +2*a*f*g*i
                   -b*b*h*j +2*b*h*d*g -e*h*d*d
                   -e*j*c*c +2*j*c*b*f +2*e*c*d*i
                   +c*c*g*g -2*b*c*i*g -2*c*d*f*g +b*b*i*i -2*b*d*i*f +d*d*f*f;
        return solveQuarticDouble(a4,a3,a2,a1,a0);
    }
    
    public static Vector4f eigenVecOfValue(Matrix4f m, float eig){
        Matrix4bd mbd = new Matrix4bd(m);
        BigDecimal[] ebd = eigenVecOfValue(mbd, new BigDecimal(eig));
        return new Vector4f(ebd[0].floatValue(), ebd[1].floatValue(), ebd[2].floatValue(), ebd[3].floatValue());
    }
    
    public static Vector4f eigenVecOfValue(Matrix4f m, double eig){
        Matrix4bd mbd = new Matrix4bd(m);
        BigDecimal[] ebd = eigenVecOfValue(mbd, new BigDecimal(eig));
        return new Vector4f(ebd[0].floatValue(), ebd[1].floatValue(), ebd[2].floatValue(), ebd[3].floatValue());
    }
    
    public static Transformation findMinimumReferenceAlgorithm(PointData src, PointData dest){
        if(dest.points.size() != src.points.size()) {
            System.err.println("Set sizes differ!");
        }
        long time = System.currentTimeMillis();
        int count = src.points.size();
        Matrix4f M = (Matrix4f) new Matrix4f().setZero();
        for(int i = 0; i < count ;i++){
            Matrix4f Pi = sourceVecToMat(src.points.get(i));
            Matrix4f Qi = destVecToMat(dest.points.get(i));
            Matrix4f.add(M, Matrix4f.mul(Pi.transpose(null), Qi, null), M);
        }
        float[] eigen = eigenvaluesOfSymmMat4(M.m00, M.m01, M.m02, M.m03, M.m11,
                M.m12, M.m13, M.m22, M.m23, M.m33);
        float max = eigen[0];
        for(int i = 1; i < eigen.length ;i++){
            max = max > eigen[i] ? eigen[i] : max;
        }
        Vector4f maxEigVec = eigenVecOfValue(M, max);
        Hf quat = new Hf(maxEigVec);
        QuaternionRotation rot = new QuaternionRotation(quat);
        
        Transformation res = new Transformation();
        res.rotation = rot;
        res.translation = Maths.sum(dest.getCentroid(), rot.rotate(src.getCentroid()).negate(null));
        System.out.println(rot);
        System.out.println("translated: " + Prints.printVector(res.translation));
        System.out.println("took " + (System.currentTimeMillis()-time) + "ms" );
        System.out.println();
        return res;
    }
    
    public static Transformation findMinimumReferenceAlgorithmDouble(PointData src, PointData dest){
        if(dest.points.size() != src.points.size()) {
            System.err.println("Set sizes differ!");
        }
        long time = System.currentTimeMillis();
        int count = src.points.size();
        Matrix4f M = (Matrix4f) new Matrix4f().setZero();
        for(int i = 0; i < count ;i++){
            Matrix4f Pi = sourceVecToMat(src.points.get(i));
            Matrix4f Qi = destVecToMat(dest.points.get(i));
            Matrix4f.add(M, Matrix4f.mul(Pi.transpose(null), Qi, null), M);
        }
        double[] eigen = eigenvaluesOfSymmMat4Double(M.m00, M.m01, M.m02, M.m03, M.m11,
                M.m12, M.m13, M.m22, M.m23, M.m33);
        double max = eigen[0];
        for(int i = 1; i < eigen.length ;i++){
            max = max > eigen[i] ? eigen[i] : max;
        }
        Vector4f maxEigVec = eigenVecOfValue(M, max);
        Hf quat = new Hf(maxEigVec);
        QuaternionRotation rot = new QuaternionRotation(quat);
        
        Transformation res = new Transformation();
        res.rotation = rot;
        res.translation = Maths.sum(dest.getCentroid(), rot.rotate(src.getCentroid()).negate(null));
        System.out.println(rot);
        System.out.println("translated: " + Prints.printVector(res.translation));
        System.out.println("took " + (System.currentTimeMillis()-time) + "ms" );
        System.out.println();
        return res;
    }
    
    /**
     * Test main
     * @param args 
     */
    public static void main(String[] args){
//        Prints.printArray(solveQuartic(ONE,ONE,L3.negate(),ONE,ZERO));
//        System.out.println(new Cf(1, -3).debugMethodsOtput());
//        System.out.println(Cf.geom(3.1622777f, (5.0341396f)));
//        System.out.print(new Cf(1,0).nthrt(3)[2]);
        Matrix4f f = new Matrix4f();
//        f.m00 = 1;f.m10 = 1;f.m20 = 1;f.m30 = 1;
//        f.m01 = 2;f.m11 = 2;f.m21 = 3;f.m31 = 2;
//        f.m02 = 3;f.m12 = 1;f.m22 = 3;f.m32 = 0;
//        f.m03 = 4;f.m13 = 1;f.m23 = 3;f.m33 = 5;

        f.m00 = 2;f.m10 = -4;f.m20 = 2;f.m30 = 8;
        f.m01 = -4;f.m11 = 3;f.m21 = 1;f.m31 = -1;
        f.m02 = 2;f.m12 = 1;f.m22 = 20;f.m32 = 3;
        f.m03 = 8;f.m13 = -1;f.m23 = 3;f.m33 = 4;
        
        f.m00 = -233;f.m10 = -340;f.m20 = -48;f.m30 = 397;
        f.m01 = -340;f.m11 = 161;f.m21 = -35;f.m31 = -525;
        f.m02 = -48;f.m12 = -35;f.m22 = -52;f.m32 = -4;
        f.m03 = 397;f.m13 = -525;f.m23 = -4;f.m33 = 124;
//        
//        f.m00 = -23;f.m10 = -34 ;f.m20 = -4;f.m30 = 39;
//        f.m01 = -34;f.m11 = 16  ;f.m21 = -3;f.m31 = -52;
//        f.m02 = -4;f.m12 = -3   ;f.m22 = -5;f.m32 = -0.4f;
//        f.m03 = 39;f.m13 = -52  ;f.m23 = -0.4f;f.m33 = 12;
                
//        f.m00 = 2;f.m10 = 0;f.m20 = 2;f.m30 = 8;
//        f.m01 = 0;f.m11 = 0;f.m21 = 0;f.m31 = 0;
//        f.m02 = 2;f.m12 = 0;f.m22 = 20;f.m32 = 3;
//        f.m03 = 8;f.m13 = 0;f.m23 = 3;f.m33 = 4;
//        Matrix4bd m = new Matrix4bd(f);
//        m.m00 = new BigDecimal("-2333.9448566436767578125");    m.m01 = new BigDecimal("-3404.11792385578155517578125");    m.m02 = new BigDecimal("-489.70150136947631835937500"); m.m03 = new BigDecimal("3976.8704006671905517578125");
//        m.m10 = new BigDecimal("-3404.11792385578155517578125");m.m11 = new BigDecimal("1615.44407664239406585693359375");  m.m12 = new BigDecimal("-359.3015401363372802734375");  m.m13 = new BigDecimal("-5251.42184436321258544921875");
//        m.m20 = new BigDecimal("-489.70150136947631835937500"); m.m21 = new BigDecimal("-359.3015401363372802734375");      m.m22 = new BigDecimal("-526.243221759796142578125");   m.m23 = new BigDecimal("-42.35417270660400390625000");
//        m.m30 = new BigDecimal("3976.8704006671905517578125");  m.m31 = new BigDecimal("-5251.42184436321258544921875");    m.m32 = new BigDecimal("-42.35417270660400390625");     m.m33 = new BigDecimal("1244.7440302371978759765625");
//        
//        System.out.println(m.determinant());
//        BigDecimal[] eigen = eigenvaluesOfSymmMat4(m.m00, m.m01, m.m02, m.m03, m.m11,
//                m.m12, m.m13, m.m22, m.m23, m.m33);
//        Prints.printArray(eigen);
//        
//        System.out.print("\n");
//            print(m);
//        System.out.print("\n");
//        
//        Matrix4bd re = m.clone().rowEchelon();
//        print(re);
//        
//        for(int i = 0;i<4;i++){
//            System.out.println();
//            System.out.println("eigenval: "+eigen[i]);
//            eigenVecOfValue(m, eigen[i]);
//        }

//        System.out.println("det: "+re.determinant());
//        BigDecimal[] sol = re.clone().solveHomogenousSystem();
//            System.out.print("\n");
//        for(int i = 0;i<4;i++){
//            System.out.print(sol[i]+", ");
//        }

        System.out.println(Cbd.arcsin(new BigDecimal(-0.9999)));
    }
    
    public static void print(Matrix4bd pr){
        for(int i = 0;i<4;i++){
            for(int j = 0;j<4;j++){
                System.out.print(pr.getElement(i, j)+"  ");
            }
        System.out.print("\n");
        }
    }
    
    
    ////////////////////////////////////////////////////////////////////////////
    //************************************************************************//
    //BigDecimal implementation of reference algorithm//////////////////////////
    public static BigDecimal ZERO = BigDecimal.ZERO;
    public static BigDecimal HALF = new BigDecimal(0.5);
    public static BigDecimal ONE = new BigDecimal(1);
    public static BigDecimal TWO = new BigDecimal(2);
    public static BigDecimal L3 = new BigDecimal(3);
    public static BigDecimal L4 = new BigDecimal(4);
    public static BigDecimal L8 = new BigDecimal(8);
    public static BigDecimal L9 = new BigDecimal(9);
    public static BigDecimal L16 = new BigDecimal(16);
    public static BigDecimal L27 = new BigDecimal(27);
    public static BigDecimal L64 = new BigDecimal(64);
    public static BigDecimal L256 = new BigDecimal(256);
    
    public static MathContext context = new MathContext(100, RoundingMode.HALF_EVEN);
    public static MathContext roundingContext = new MathContext(5, RoundingMode.HALF_EVEN);
    
    public static BigDecimal[] eigenvaluesOfSymmMat4(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d,
            BigDecimal e, BigDecimal f, BigDecimal g, BigDecimal h, BigDecimal i, BigDecimal j){
        BigDecimal a4 = ONE;
        BigDecimal a3 = e.add(a).add(j).add(h).negate();//-e-a-j-h;
        BigDecimal a2 = sum(new BigDecimal[]{
            mul(g,g.negate()), mul(e,a), mul(e,j), mul(a,j), mul(a,h), mul(j,h), mul(e,h),//-g*g +e*a +e*j +a*j +a*h +j*h +e*h
            mul(f,f.negate()), mul(i,i.negate()),//-f*f -i*i
            mul(b,b.negate()), mul(d,d.negate()),//-b*b -d*d
            mul(c,c.negate())//-c*c
                    });
        BigDecimal a1 = sum(new BigDecimal[]{
            mul(a,g,g), mul(g,g,h), mul(e,a,j.negate()), mul(e,a,h.negate()),
            mul(e,j,h.negate()), mul(a,j,h.negate()),//a*g*g +g*g*h -e*a*j -e*a*h -e*j*h -a*j*h
            mul(a,f,f), mul(j,f,f), mul(e,i,i), mul(a,i,i), mul(TWO,f,g,i.negate()),//+a*f*f +j*f*f +e*i*i +a*i*i -2*f*g*i
            mul(b,b,j), mul(b,b,h), mul(TWO,b,d,g.negate()), mul(e,d,d), mul(h,d,d),//+b*b*j +b*b*h -2*b*d*g +e*d*d + h*d*d
            mul(e,c,c), mul(j,c,c), mul(TWO,c,b,f.negate()), mul(TWO,c,d,i.negate())//+e*c*c +j*c*c -2*c*b*f-2*c*d*i
                    });
        BigDecimal a0 = sum(new BigDecimal[]{
            mul(a,g,g,h.negate()), mul(e,a,j,h),//-a*g*g*h +e*a*j*h
            mul(a,j,f,f.negate()), mul(e,a,i,i.negate()), mul(TWO,a,f,g,i),//-a*j*f*f -e*a*i*i +2*a*f*g*i
            mul(b,b,h,j.negate()), mul(TWO,b,h,d,g), mul(e,h,d,d.negate()),//-b*b*h*j +2*b*h*d*g -e*h*d*d
            mul(e,j,c,c.negate()), mul(TWO,j,c,b,f), mul(TWO,e,c,d,i),//-e*j*c*c +2*j*c*b*f +2*e*c*d*i
            mul(c,c,g,g), mul(TWO,b,c,i,g.negate()), mul(TWO,c,d,f,g.negate()),
            mul(b,b,i,i), mul(TWO,b,d,i,f.negate()), mul(d,d,f,f)//+c*c*g*g -2*b*c*i*g -2*c*d*f*g +b*b*i*i -2*b*d*i*f +d*d*f*f
                    });
        return solveQuartic(a4,a3,a2,a1,a0);
    }
    
    public static BigDecimal[] solveQuartic(BigDecimal a4, BigDecimal a3, BigDecimal a2, BigDecimal a1, BigDecimal a0){
        //reduce to depressed quartic
        BigDecimal b = a3.divide(a4, context);
        BigDecimal c = a2.divide(a4, context);
        BigDecimal d = a1.divide(a4, context);
        BigDecimal e = a0.divide(a4, context);
        
        BigDecimal p = L8.multiply(c).subtract(L3.multiply(b.pow(2))).divide(L8, context);//(8*c-3*b*b)/8f;
        BigDecimal q = b.pow(3).subtract(L4.multiply(b).multiply(c))
                .add(L8.multiply(d)).divide(L8, context);//(b*b*b -4*b*c+8*d)/8f;
        BigDecimal r = L3.negate().multiply(b.pow(4)).add(L256.multiply(e))
                .subtract(L64.multiply(b).multiply(d)).add(L16.multiply(b.pow(2)
                .multiply(c))).divide(L256, context);//(-3*b*b*b*b+256*e-64*b*d+16*b*b*c)/256f;
        
        ////////////////////////////////////////////////////////////////////////
        Cbd m, first, second;
        BigDecimal y1 = null, y2 = null, y3 = null, y4 = null;
        Cbd y10 = null, y20 = null, y30 = null, y40 = null;
        try{
        m = solveCubic(L8, L8.multiply(p), TWO.multiply(p.pow(2)).subtract(L8.multiply(r)), q.pow(2).negate());
        System.out.println();
        System.out.println("Quartic :"+ a4+", "+a3+", "+a2+", "+a1+", "+a0);
        System.out.println("m: "+m);
        first = (new Cbd(mul(TWO,p)).add(m.mul(TWO)).add(new Cbd(mul(Cbd.sqrt(TWO),q))
                .div(m.sqrt()[0]))).neg().sqrt()[0];//sqrt(-(2*p+2*m+(sqrt(2)*q)/(sqrt(m))));
        second = (new Cbd(2).mul(p).add(m.mul(TWO)).add(new Cbd(Cbd.sqrt(TWO)
                .multiply(q)).div(m.sqrt()[0]).neg())).neg().sqrt()[0];//sqrt(-(2*p+2*m-(sqrt(2)*q)/(sqrt(m))));
        Cbd m2sqrt = m.mul(TWO).sqrt()[0];
        y10 =  m2sqrt.add(first).div(TWO);//(sqrt(2*m)+first)/2;
        y20 =  m2sqrt.add(first.neg()).div(TWO);//(sqrt(2*m)-first)/2;
        y30 =  m2sqrt.neg().add(second).div(TWO);//(-sqrt(2*m)+second)/2;
        y40 =  m2sqrt.neg().add(second.neg()).div(TWO);//(-sqrt(2*m)-second)/2;
        y1 = y10.real();
        y2 = y20.real();
        y3 = y30.real();
        y4 = y40.real();
            } catch(ArithmeticException ex){
                System.out.println("Caught \"not real\" exception and defaulted to real part.");
                y1 = y10.Re();
                y2 = y20.Re();
                y3 = y30.Re();
                y4 = y40.Re();                
            }
        BigDecimal diff = b.divide(L4, context);
        BigDecimal[] x = new BigDecimal[4];
        x[0] = y1.subtract(diff);
        x[1] = y2.subtract(diff);
        x[2] = y3.subtract(diff);
        x[3] = y4.subtract(diff);
        for(BigDecimal u : x){
            System.out.println("root: "+u + " "+(testRoot(a4,a3,a2,a1,a0,u)?" passed.":" not passed"));
        }
        System.out.println();
        return x;
    }
    
    public static Cbd solveCubic(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d){
        System.out.println();
        System.out.println("Cubic: " + a + ", "+b+", "+c+", "+d);
        BigDecimal D0 = b.pow(2).subtract(L3.multiply(a).multiply(c));//b*b - 3*a*c;
        BigDecimal D1 = TWO.multiply(b.pow(3)).
                subtract(L9.multiply(a).multiply(b).multiply(c))
                .add(L27.multiply(a.pow(2).multiply(d)));//2*b*b*b - 9*a*b*c + 27*a*a*d;
        System.out.println(D0 + ", " + D1);
        Cbd s = new Cbd(D1.pow(2).subtract(L4.multiply(D0.pow(3))));
        System.out.println(s);
        Cbd sr = s.sqrt()[0];
        System.out.println(sr);
        Cbd[] C;
        if(D1.compareTo(BigDecimal.ZERO)>0){
            C = (sr.add(D1).mul(HALF)).nthrt(3);
        } else {
            C = (sr.neg().add(D1).mul(HALF)).nthrt(3);
        }
        Cbd[] x = new Cbd[3];
        double[] args = new double[3];
        for(int i = 0; i<3 ;i++){
            x[i] = C[i].add(new Cbd(D0).div(C[i])).add(b).mul(BigDecimal.ONE.negate().divide(L3.multiply(a), context));
            args[i] = x[i].arg();
            System.out.println(x[i]);
        }
        int min = (args[0]*args[0]>args[1]*args[1])?1:0;
        min = (args[min]*args[min]>args[2]*args[2])?2:min;
        return x[min];
    }
    
    public static BigDecimal[] eigenVecOfValue(Matrix4bd m, BigDecimal eig){
        m = m.clone();
        m.m00 = m.m00.subtract(eig);
        m.m11 = m.m11.subtract(eig);
        m.m22 = m.m22.subtract(eig);
        m.m33 = m.m33.subtract(eig);
        System.out.println();
        print(m);
        System.out.println();
        System.out.println("determinant: " + m.determinant().round(roundingContext));
        m = m.rowEchelon();
        BigDecimal[] vec = m.solveHomogenousSystem();
        return vec;
    }
    
    public static BigDecimal[] normalize(BigDecimal[] vec){
        BigDecimal normSq = BigDecimal.ZERO;
        for(int i = 0; i < vec.length;i++){
            normSq.add(vec[i].pow(2));
        }
        BigDecimal norm = Cbd.sqrt(normSq);
        if(norm.compareTo(BigDecimal.ZERO)==0){
            return vec;
        }
        BigDecimal[] nev = new BigDecimal[vec.length];
        for(int i = 0; i < vec.length;i++){
            nev[i] = vec[i].divide(norm, context);
        }
        return nev;
    }
    
    public static Transformation findMinimumReferenceAlgorithmBigD(PointData src, PointData dest){
        if(dest.points.size() != src.points.size()) {
            System.err.println("Set sizes differ!");
        }
        long time = System.currentTimeMillis();
        int count = src.points.size();
        Matrix4bd M = new Matrix4bd().setZero();
        for(int i = 0; i < count ;i++){
            Matrix4f Pi = sourceVecToMat(src.points.get(i));
            Matrix4f Qi = destVecToMat(dest.points.get(i));
            M.add(new Matrix4bd(Matrix4f.mul(Pi.transpose(null), Qi, null)));
        }
        System.out.println();
        print(M);
        System.out.println();
        checkSymmetric(M);
        BigDecimal[] eigen = eigenvaluesOfSymmMat4(M.m00, M.m01, M.m02, M.m03, M.m11,
                M.m12, M.m13, M.m22, M.m23, M.m33);
        BigDecimal max = eigen[0];
        for(int i = 1; i < eigen.length ;i++){
            max = max.compareTo(eigen[i]) < 0 ? eigen[i] : max;
        }
        /**/
        for(int i = 0;i<4;i++){
            System.out.println();
            System.out.println("eigenval: "+eigen[i]);
            eigenVecOfValue(M, eigen[i]);
        }
        /**/
        System.out.println("largest eigenvalue: "+max);
        BigDecimal[] maxEigVec = eigenVecOfValue(M, max);
        BigDecimal[] eVec = normalize(maxEigVec);
        float[] vec = new float[eVec.length];
        for(int i = 0; i < vec.length ;i++){
            vec[i] = eVec[i].floatValue();
            System.out.print(vec[i]+", ");
        }
        System.out.println();
        Hf quat = new Hf(vec[0], vec[1], vec[2], vec[3]);
        QuaternionRotation rot = new QuaternionRotation(quat.normalised());
        
        Transformation res = new Transformation();
        res.rotation = rot;
        res.translation = Maths.sum(dest.getCentroid(), rot.rotate(src.getCentroid()).negate(null));
        System.out.println(rot);
        System.out.println("translated: " + Prints.printVector(res.translation));
        System.out.println("took " + (System.currentTimeMillis()-time) + "ms" );
        return res;
    }
    
    public static BigDecimal mul(BigDecimal a, BigDecimal b){
        return a.multiply(b);
    }
    
    public static BigDecimal mul(BigDecimal a, BigDecimal b, BigDecimal c){
        return a.multiply(b).multiply(c);
    }
    
    public static BigDecimal mul(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d){
        return a.multiply(b).multiply(c).multiply(d);
    }
    
    public static BigDecimal mul(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d, BigDecimal e){
        return a.multiply(b).multiply(c).multiply(d).multiply(e);
    }
    
    public static BigDecimal mul(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d, BigDecimal e, BigDecimal f){
        return a.multiply(b).multiply(c).multiply(d).multiply(e).multiply(f);
    }
    
    public static BigDecimal sum(BigDecimal[] summands){
        BigDecimal res = ZERO;
        for(BigDecimal s : summands){
            res = res.add(s);
        }
        return res;
    }
    
    public static void checkSymmetric(Matrix4bd m){
        for(int j = 0; j < 4 ;j++){
            for(int i = j; i < 4 ;i++){
                BigDecimal a = m.getElement(i, j);
                BigDecimal b = m.getElement(j, i);
                if(a.compareTo(b) != 0){
                    System.out.println("Found difference: " + a + " vs. "+b);
                }
            }
        }
    }
    
    public static boolean testRoot(BigDecimal a, BigDecimal b, BigDecimal c, BigDecimal d, BigDecimal e, BigDecimal x){
        BigDecimal r = sum(new BigDecimal[]{
            a.multiply(x.pow(4)),
            b.multiply(x.pow(3)),
            c.multiply(x.pow(2)),
            d.multiply(x.pow(1)),
            e
        });
        System.out.println("eval poly:"+r);
        return r.abs().compareTo(Matrix4bd.NEAR_ZERO) < 0;
    }
}
