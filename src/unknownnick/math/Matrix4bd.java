package unknownnick.math;

import java.math.BigDecimal;
import org.lwjgl.util.vector.Matrix4f;

public class Matrix4bd {
    public BigDecimal m00;
    public BigDecimal m01;
    public BigDecimal m02;
    public BigDecimal m03;
    public BigDecimal m10;
    public BigDecimal m11;
    public BigDecimal m12;
    public BigDecimal m13;
    public BigDecimal m20;
    public BigDecimal m21;
    public BigDecimal m22;
    public BigDecimal m23;
    public BigDecimal m30;
    public BigDecimal m31;
    public BigDecimal m32;
    public BigDecimal m33;
    
    public Matrix4bd(){
        m00 = new BigDecimal(1);
        m01 = new BigDecimal(0);
        m02 = new BigDecimal(0);
        m03 = new BigDecimal(0);
        m10 = new BigDecimal(0);
        m11 = new BigDecimal(1);
        m12 = new BigDecimal(0);
        m13 = new BigDecimal(0);
        m20 = new BigDecimal(0);
        m21 = new BigDecimal(0);
        m22 = new BigDecimal(1);
        m23 = new BigDecimal(0);
        m30 = new BigDecimal(0);
        m31 = new BigDecimal(0);
        m32 = new BigDecimal(0);
        m33 = new BigDecimal(1);
    }
    
    public Matrix4bd(Matrix4bd read){
        m00 = read.m00;
        m01 = read.m01;
        m02 = read.m02;
        m03 = read.m03;
        m10 = read.m10;
        m11 = read.m11;
        m12 = read.m12;
        m13 = read.m13;
        m20 = read.m20;
        m21 = read.m21;
        m22 = read.m22;
        m23 = read.m23;
        m30 = read.m30;
        m31 = read.m31;
        m32 = read.m32;
        m33 = read.m33;
    }
    
    public Matrix4bd(Matrix4f read){
        m00 = new BigDecimal(read.m00);
        m01 = new BigDecimal(read.m01);
        m02 = new BigDecimal(read.m02);
        m03 = new BigDecimal(read.m03);
        m10 = new BigDecimal(read.m10);
        m11 = new BigDecimal(read.m11);
        m12 = new BigDecimal(read.m12);
        m13 = new BigDecimal(read.m13);
        m20 = new BigDecimal(read.m20);
        m21 = new BigDecimal(read.m21);
        m22 = new BigDecimal(read.m22);
        m23 = new BigDecimal(read.m23);
        m30 = new BigDecimal(read.m30);
        m31 = new BigDecimal(read.m31);
        m32 = new BigDecimal(read.m32);
        m33 = new BigDecimal(read.m33);
    }
    
    public Matrix4bd setIdentity(){
        m00 = new BigDecimal(1);
        m01 = new BigDecimal(0);
        m02 = new BigDecimal(0);
        m03 = new BigDecimal(0);
        m10 = new BigDecimal(0);
        m11 = new BigDecimal(1);
        m12 = new BigDecimal(0);
        m13 = new BigDecimal(0);
        m20 = new BigDecimal(0);
        m21 = new BigDecimal(0);
        m22 = new BigDecimal(1);
        m23 = new BigDecimal(0);
        m30 = new BigDecimal(0);
        m31 = new BigDecimal(0);
        m32 = new BigDecimal(0);
        m33 = new BigDecimal(1);
        return this;
    }
    
    public Matrix4bd setZero(){
        m00 = new BigDecimal(0);
        m01 = new BigDecimal(0);
        m02 = new BigDecimal(0);
        m03 = new BigDecimal(0);
        m10 = new BigDecimal(0);
        m11 = new BigDecimal(0);
        m12 = new BigDecimal(0);
        m13 = new BigDecimal(0);
        m20 = new BigDecimal(0);
        m21 = new BigDecimal(0);
        m22 = new BigDecimal(0);
        m23 = new BigDecimal(0);
        m30 = new BigDecimal(0);
        m31 = new BigDecimal(0);
        m32 = new BigDecimal(0);
        m33 = new BigDecimal(0);
        return this;
    }
    
    /**
     * @param i 0..3 row index
     * @return row i in BigDecimal array
     */
    public BigDecimal[] row(int i){
        if(i == 0)
            return new BigDecimal[]{m00, m01, m02, m03};
        if(i == 1)
            return new BigDecimal[]{m10, m11, m12, m13};
        if(i == 2)
            return new BigDecimal[]{m20, m21, m22, m23};
        if(i == 3)
            return new BigDecimal[]{m30, m31, m32, m33};
        else return null;
    }
    
    /**
     * @param j 0..3 column index
     * @return column j in BigDecimal array
     */
    public BigDecimal[] col(int j){
        if(j == 0)
            return new BigDecimal[]{m00, m10, m20, m30};
        if(j == 1)
            return new BigDecimal[]{m01, m11, m21, m31};
        if(j == 2)
            return new BigDecimal[]{m02, m12, m22, m32};
        if(j == 3)
            return new BigDecimal[]{m03, m13, m23, m33};
        else return null;
    }
    
    public static BigDecimal dot(BigDecimal[] a, BigDecimal[] b){
        BigDecimal ret = BigDecimal.ZERO;
        if(a.length != b.length){
            throw new ArithmeticException("Vector sizes differ!");
        } else {
            for(int i = 0; i < a.length;i++){
                ret.add(a[i].multiply(b[i]));
            }
            return ret;
        }
    }
    
    public Matrix4bd setElement(int i, int j, BigDecimal val){
        switch (i) {
            case 0:
        switch (j) {
            case 0:
                m00 = val;
                break;
            case 1:
                m01 = val;
                break;
            case 2:
                m02 = val;
                break;
            case 3:
                m03 = val;
                break;
            default:
                break;
        }
                break;
            case 1:
        switch (j) {
            case 0:
                m10 = val;
                break;
            case 1:
                m11 = val;
                break;
            case 2:
                m12 = val;
                break;
            case 3:
                m13 = val;
                break;
            default:
                break;
        }
                break;
            case 2:
        switch (j) {
            case 0:
                m20 = val;
                break;
            case 1:
                m21 = val;
                break;
            case 2:
                m22 = val;
                break;
            case 3:
                m23 = val;
                break;
            default:
                break;
        }
                break;
            case 3:
        switch (j) {
            case 0:
                m30 = val;
                break;
            case 1:
                m31 = val;
                break;
            case 2:
                m32 = val;
                break;
            case 3:
                m33 = val;
                break;
            default:
                break;
        }
                break;
            default:
                break;
        }
        return this;
    }
    
    public static Matrix4bd mul(Matrix4bd left, Matrix4bd right, Matrix4bd dest){
        Matrix4bd ret = new Matrix4bd();
        for(int i = 0; i<4 ;i++){
            for(int j = 0; j<4 ;j++){
                ret.setElement(i, j, dot(left.row(i), right.col(j)));
            }
        }
        if(dest != null)
            return dest.load(ret);
        else
            return ret;
    }
    
    public BigDecimal getElement(int i, int j){
        BigDecimal val = null;
        switch (i) {
            case 0:
        switch (j) {
            case 0:
                val = m00;
                break;
            case 1:
                val = m01;
                break;
            case 2:
                val = m02;
                break;
            case 3:
                val = m03;
                break;
            default:
                break;
        }
                break;
            case 1:
        switch (j) {
            case 0:
                val = m10;
                break;
            case 1:
                val = m11;
                break;
            case 2:
                val = m12;
                break;
            case 3:
                val = m13;
                break;
            default:
                break;
        }
                break;
            case 2:
        switch (j) {
            case 0:
                val = m20;
                break;
            case 1:
                val = m21;
                break;
            case 2:
                val = m22;
                break;
            case 3:
                val = m23;
                break;
            default:
                break;
        }
                break;
            case 3:
        switch (j) {
            case 0:
                val = m30;
                break;
            case 1:
                val = m31;
                break;
            case 2:
                val = m32;
                break;
            case 3:
                val = m33;
                break;
            default:
                break;
        }
                break;
            default:
                break;
        }
        return val;
    }
    
    public Matrix4bd add(Matrix4bd a){
        for(int i = 0; i<4 ;i++){
            for(int j = 0; j<4 ;j++){
                this.setElement(i, j, this.getElement(i, j).add(a.getElement(i, j)));
            }
        }
        return this;
    }
    
    public Matrix4bd transpose(){
        Matrix4bd copy = new Matrix4bd(this);
        for(int i = 0; i<4 ;i++){
            for(int j = 0; j<4 ;j++){
                this.setElement(j, i, copy.getElement(i, j));
            }
        }
        return this;
    }
    
    public Matrix4bd storeRow(int i, BigDecimal[] row){
        for(int j = 0; j<4 ;j++){
            this.setElement(i, j, row[j]);
        }
        return this;
    }
    
    public Matrix4bd storeColumn(int j, BigDecimal[] column){
        for(int i = 0; i<4 ;i++){
            this.setElement(i, j, column[i]);
        }
        return this;
    }
    
    public BigDecimal determinant(){
        BigDecimal
      b00 = m00.multiply(m11).subtract(m01.multiply(m10)),
      b01 = m00.multiply(m12).subtract(m02.multiply(m10)),
      b02 = m00.multiply(m13).subtract(m03.multiply(m10)),
      b03 = m01.multiply(m12).subtract(m02.multiply(m11)),
      b04 = m01.multiply(m13).subtract(m03.multiply(m11)),
      b05 = m02.multiply(m13).subtract(m03.multiply(m12)),
      b06 = m20.multiply(m31).subtract(m21.multiply(m30)),
      b07 = m20.multiply(m32).subtract(m22.multiply(m30)),
      b08 = m20.multiply(m33).subtract(m23.multiply(m30)),
      b09 = m21.multiply(m32).subtract(m22.multiply(m31)),
      b10 = m21.multiply(m33).subtract(m23.multiply(m31)),
      b11 = m22.multiply(m33).subtract(m23.multiply(m32));
    return b00.multiply(b11).subtract(b01.multiply(b10)).add(b02.multiply(b09))
            .add(b03.multiply(b08)).subtract(b04.multiply(b07)).add(b05.multiply(b06));
    }
    
    public Matrix4bd rowEchelon(){
        int h = 0;
        int k = 0;
        while(h< 4 && k<4){
            int imax = 0;
            BigDecimal max = BigDecimal.ZERO;
            for(int a = h; a < 4;a++){
                if(max.compareTo(this.getElement(a, k).abs())<0){
                    max = this.getElement(a, k);
                    imax = a;
                }
            }
            if(max.compareTo(BigDecimal.ZERO)==0){
                k++;
            } else {
                BigDecimal[] temp = this.row(imax);
                this.storeRow(imax, row(h));
                this.storeRow(h, temp);
                BigDecimal Ahk = this.getElement(h, k);
                for(int i = h+1; i < 4 ;i++){
                    BigDecimal f = this.getElement(i, k).divide(Ahk, Cbd.context);
                    this.setElement(i, k, BigDecimal.ZERO);
                    for(int j = k+1; j < 4 ;j++){
                        this.setElement(i, j, this.getElement(i, j).subtract(this.getElement(h, j).multiply(f)));
                    }
                }
            k++;
            h++;
            }
        }
        return this;
    }
    
    public Matrix4bd mul(BigDecimal b){
        m00 = m00.multiply(b);
        m01 = m01.multiply(b);
        m02 = m02.multiply(b);
        m03 = m03.multiply(b);
        m10 = m10.multiply(b);
        m11 = m11.multiply(b);
        m12 = m12.multiply(b);
        m13 = m13.multiply(b);
        m20 = m20.multiply(b);
        m21 = m21.multiply(b);
        m22 = m22.multiply(b);
        m23 = m23.multiply(b);
        m30 = m30.multiply(b);
        m31 = m31.multiply(b);
        m32 = m32.multiply(b);
        m33 = m33.multiply(b);
        return this;
    }
    
    public Matrix4bd load(Matrix4bd source){
        m00 = source.m00;
        m01 = source.m01;
        m02 = source.m02;
        m03 = source.m03;
        m10 = source.m10;
        m11 = source.m11;
        m12 = source.m12;
        m13 = source.m13;
        m20 = source.m20;
        m21 = source.m21;
        m22 = source.m22;
        m23 = source.m23;
        m30 = source.m30;
        m31 = source.m31;
        m32 = source.m32;
        m33 = source.m33;
        return this;
    }
    
    @Override
    public Matrix4bd clone(){
        return new Matrix4bd(this);
    }
    
    /**
    * Warning: Destroys the matrix
    */
    public BigDecimal[] solveHomogenousSystem(){
        int parameters = 0;
        BigDecimal[] vals = new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO};
        BigDecimal[] res =new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO};
        
        BigDecimal det = checkRoundZero(this.determinant());
        System.out.println("det in lsys: "+det);
        if(det.compareTo(BigDecimal.ZERO) != 0){
            System.out.println("returned on "+(4-parameters));
            return res;
        } else {
            parameters++;
            if(this.getElement(3-parameters, 3-parameters).compareTo(BigDecimal.ZERO) == 0){
                res[4-parameters] = BigDecimal.ZERO;
            } else {
                res[4-parameters] = BigDecimal.ONE;
            }
            for(int h = 0; h <(4-parameters);h++){
                vals[h] = vals[h].subtract(res[4-parameters].multiply(this.getElement(h, 4-parameters)));
            }                    
            
            det = checkRoundZero(det3least());
            if(det.compareTo(BigDecimal.ZERO) != 0){
                for(int u = 0; u < 4-parameters;u++){
                    Matrix4bd clone = this.clone();
                    res[u] = clone.storeColumn(u, vals).det3least().divide(det, Cbd.context);
                }
            System.out.println("returned on "+(4-parameters));
                return res;
            } else {
                parameters++;
                BigDecimal el = this.getElement(4-parameters, 4-parameters);
                if(el.compareTo(BigDecimal.ZERO) != 0){
                    res[4-parameters] = vals[4-parameters].divide(el, Cbd.context);
                } else {
                    if(vals[4-parameters].compareTo(BigDecimal.ZERO) != 0){
                        System.err.println("Invalid system on " + (4-parameters)+"!");
                        return null;
                    } else {
                        
                        if(this.getElement(3-parameters, 3-parameters).compareTo(BigDecimal.ZERO) == 0){
                            res[4-parameters] = BigDecimal.ZERO;
                        } else {
                            res[4-parameters] = BigDecimal.ONE;
                        }
                    }
                }
                for(int h = 0; h <(4-parameters);h++){
                    vals[h] = vals[h].subtract(res[4-parameters].multiply(this.getElement(h, 4-parameters)));
                }
                
                det = checkRoundZero(det2least());
                if(det.compareTo(BigDecimal.ZERO) != 0){
                    for(int u = 0; u < 4-parameters;u++){
                        Matrix4bd clone = this.clone();
                        res[u] = clone.storeColumn(u, vals).det2least().divide(det, Cbd.context);
                    }
            System.out.println("returned on "+(4-parameters));
                    return res;
                } else {
                    parameters++;
                    el = this.getElement(4-parameters, 4-parameters);
                    if(el.compareTo(BigDecimal.ZERO) != 0){
                        res[4-parameters] = vals[4-parameters].divide(el, Cbd.context);
                    } else {
                        if(vals[4-parameters].compareTo(BigDecimal.ZERO) != 0){
                            System.err.println("Invalid system on " + (4-parameters)+"!");
                            return null;
                        } else {
                            if(this.getElement(3-parameters, 3-parameters).compareTo(BigDecimal.ZERO) == 0){
                                res[4-parameters] = BigDecimal.ZERO;
                            } else {
                                res[4-parameters] = BigDecimal.ONE;
                            }
                        }
                    }
                    for(int h = 0; h <(4-parameters);h++){
                        vals[h] = vals[h].subtract(res[4-parameters].multiply(this.getElement(h, 4-parameters)));
                    }
                    
                    parameters++;
                    el = m00;
                    if(checkRoundZero(el).compareTo(BigDecimal.ZERO) != 0){
                        res[4-parameters] = vals[4-parameters].divide(el, Cbd.context);
                    } else {
                        if(vals[4-parameters].compareTo(BigDecimal.ZERO) != 0){
                            System.err.println("Invalid system on " + (4-parameters)+"!");
                            return null;
                        } else {
                            res[4-parameters] = BigDecimal.ONE;
                        }
                    }
                    return res;
                }
            }
        }
    }
    
    public BigDecimal det3least(){
        BigDecimal det = BigDecimal.ZERO;
        for(int k = 0; k < 3;k++){
            BigDecimal prod = BigDecimal.ONE;
            for(int l = 0; l < 3 ;l++){
                prod.multiply(this.getElement((k+l)%3,l));
            }
            det.add(prod);
        }
        for(int k = 0; k < 3;k++){
            BigDecimal prod = BigDecimal.ONE;
            for(int l = 0; l < 3 ;l++){
                prod.multiply(this.getElement(2-(k+l)%3,l));
            }
            det.add(prod);
        }
        return det;
    }
    
    public BigDecimal det2least(){
        return m00.multiply(m11).subtract(m10.multiply(m01));
    }
    
    public static BigDecimal NEAR_ZERO = new BigDecimal("0.00001");
    
    public static BigDecimal checkRoundZero(BigDecimal b){
        if(b.abs().compareTo(NEAR_ZERO) < 0){
            return BigDecimal.ZERO;
        } else 
            return b;
    }
}
